import { Component } from '@angular/core';

@Component({
  selector: 'cx-carousal-molecule',
  templateUrl: './carousal-molecule.component.html',
  styleUrls: ['./carousal-molecule.component.css']
})

export class CarousalMoleculeComponent {
  images = [62, 83, 466, 965, 982, 1043, 738].map((n) => `https://picsum.photos/id/${n}/900/500`);
}
