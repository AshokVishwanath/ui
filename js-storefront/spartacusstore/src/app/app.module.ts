import { registerLocaleData } from '@angular/common';
import localeDe from '@angular/common/locales/de';
import localeJa from '@angular/common/locales/ja';
import localeZh from '@angular/common/locales/zh';
import { NgModule } from '@angular/core';
import {
  BrowserModule,
  BrowserTransferStateModule,
} from '@angular/platform-browser';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { ConfigModule, TestConfigModule } from '@spartacus/core';
import { environment } from '../environments/environment';
import { AppRoutingModule } from './app-routing.module';
import { SharedModule } from './shared/shared.module';
import { ServicesModule } from './services/services.module';
import { AppComponent } from './app.component';
import { TestOutletModule } from '../test-outlets/test-outlet.module';
registerLocaleData(localeDe);
registerLocaleData(localeJa);
registerLocaleData(localeZh);

const devImports = [];

if (!environment.production) {
  devImports.push(StoreDevtoolsModule.instrument());
}

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'spartacus-app' }),
    BrowserTransferStateModule,
    NgbModule,
    // B2cStorefrontModule.withConfig({
    //   backend: {
    //     occ: {
    //       baseUrl: environment.occBaseUrl,
    //       legacy: false,
    //     },
    //   },
    //   context: {
    //     urlParameters: ['baseSite', 'language', 'currency'],
    //     baseSite: [
    //       'electronics-spa',
    //       'electronics',
    //       'apparel-de',
    //       'apparel-uk',
    //       'apparel-uk-spa',
    //     ],
    //   },

    //   // custom routing configuration for e2e testing
    //   routing: {
    //     routes: {
    //       product: {
    //         paths: ['product/:productCode/:name', 'product/:productCode'],
    //       },
    //     },
    //   },
    //   // we bring in static translations to be up and running soon right away
    //   i18n: {
    //     resources: translations,
    //     chunks: translationChunksConfig,
    //     fallbackLang: 'en',
    //   },
    //   features: {
    //     level: '1.5',
    //     anonymousConsents: true,
    //   },
    // }),
    // JsonLdBuilderModule,

    TestOutletModule, // custom usages of cxOutletRef only for e2e testing
    TestConfigModule.forRoot({ cookie: 'cxConfigE2E' }), // Injects config dynamically from e2e tests. Should be imported after other config modules.

    ...devImports,
    ConfigModule,
    SharedModule,
    ServicesModule,
    AppRoutingModule
  ],

  bootstrap: [AppComponent],
})
export class AppModule {}
