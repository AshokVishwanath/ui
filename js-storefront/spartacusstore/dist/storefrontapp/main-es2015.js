(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./$$_lazy_route_resource lazy recursive":
/*!******************************************************!*\
  !*** ./$$_lazy_route_resource lazy namespace object ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/app.component.html":
/*!**************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/app.component.html ***!
  \**************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (" <!-- Header Section Start -->\n <app-header-organism></app-header-organism>\n  <!-- Header Section End -->\n\n  <!-- Body Content start -->\n<router-outlet></router-outlet>\n <!-- Body Content end -->\n\n <!-- Footer Section Start -->\n <app-footer-organism></app-footer-organism>\n <!-- Footer Section End -->\n\n\n\n\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/molecules/carousal-molecule/carousal-molecule.component.html":
/*!***************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/shared/molecules/carousal-molecule/carousal-molecule.component.html ***!
  \***************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div id=\"carouselExampleCaptions\" class=\"carousel slide\" data-ride=\"carousel\">\n    <!-- <ol class=\"carousel-indicators\">\n      <li data-target=\"#carouselExampleCaptions\" data-slide-to=\"0\" class=\"active\"></li>\n      <li data-target=\"#carouselExampleCaptions\" data-slide-to=\"1\"></li>\n      <li data-target=\"#carouselExampleCaptions\" data-slide-to=\"2\"></li>\n    </ol> -->\n    <ol class=\"carousel-indicators\">\n      <li data-target=\"#carouselExampleCaptions\" *ngFor=\"let x of homeContent;let i = index\" [attr.data-slide-to]=\"i\" ngClass=\"i == 0 ? 'active' : ''\"></li>\n    </ol>\n    <div class=\"carousel-inner\">\n      <div *ngFor=\"let content of homeContent; let i = index\" class=\"carousel-item\" [ngClass]=\"{'active': i === 0 }\" >\n        <img [src]=\"hostName + content.media.url\" class=\"d-block w-100 img-fluid\" [alt]=\"content.media.code\">\n      </div>\n    </div>\n    <a class=\"carousel-control-prev\" href=\"#carouselExampleCaptions\" role=\"button\" data-slide=\"prev\">\n      <span class=\"carousel-control-prev-icon\" aria-hidden=\"true\"></span>\n      <span class=\"sr-only\">Previous</span>\n    </a>\n    <a class=\"carousel-control-next\" href=\"#carouselExampleCaptions\" role=\"button\" data-slide=\"next\">\n      <span class=\"carousel-control-next-icon\" aria-hidden=\"true\"></span>\n      <span class=\"sr-only\">Next</span>\n    </a>\n  </div>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/molecules/cart-molecule/cart-molecule.component.html":
/*!*******************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/shared/molecules/cart-molecule/cart-molecule.component.html ***!
  \*******************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"cart-wrapper\">\n    <a aria-label=\"0 items currently in your cart\" href=\"\">\n        <span class=\"cart-icon\">\n            <i class=\"fa fa-shopping-cart\"></i>\n        </span>\n        <span class=\"cart-total\"></span><span class=\"cart-count\">0</span>\n    </a>\n</div>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/molecules/client-logo-molecule/client-logo.component.html":
/*!************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/shared/molecules/client-logo-molecule/client-logo.component.html ***!
  \************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"container\">\n    <div class=\"logo-section mb-30 clearfix\">\n      <h2 class=\"title text-center mt-30\">OUR VALUABLE CLIENTS</h2>\n      <div class=\"row clearfix client-logos col-12 mt-50\">\n        <div *ngFor=\"let logo of clientLogos\" class=\"col-xs-4 col-md-2 mx-auto\">\n          <img  class=\"img-fluid mx-auto img-grayscale\" src= \"{{hostName +logo.media.url}}\" alt=\"{{logo.name}}\" />\n      </div>\n      </div>\n    </div>\n  </div>\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/molecules/contact-us-molecule/contact-us-molecule.component.html":
/*!*******************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/shared/molecules/contact-us-molecule/contact-us-molecule.component.html ***!
  \*******************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<mat-card class=\"container\">\n  <form class=\"contact-us-form\" [formGroup]=\"userDetailsForm\" (ngSubmit)=\"onSubmitUserDetails(userDetailsForm.value)\">\n    <mat-card-title class=\"title\">CONTACT US </mat-card-title>\n    <mat-form-field class=\"name-field float-md-left\" appearance=\"standard\">\n      <mat-label>Name</mat-label>\n      <input matInput placeholder=\"\" value=\"\" formControlName=\"name\" required autocomplete=\"off\" />\n      <mat-error *ngFor=\"let validation of validationMessages.name\">\n        <mat-error class=\"error-message\" *ngIf=\"\n            userDetailsForm.get('name').hasError(validation.type) &&\n            (userDetailsForm.get('name').dirty ||\n              userDetailsForm.get('name').touched)\n          \">{{ validation.message }}</mat-error>\n      </mat-error>\n    </mat-form-field>\n    <mat-form-field class=\"email-field float-md-right\" appearance=\"standard\">      \n      <mat-label>Email</mat-label>\n      <input matInput placeholder=\"\" value=\"\" formControlName=\"email\" required autocomplete=\"off\" />\n      <mat-error *ngFor=\"let validation of validationMessages.email\">\n        <mat-error class=\"error-message\" *ngIf=\"\n            userDetailsForm.get('email').hasError(validation.type) &&\n            (userDetailsForm.get('email').dirty ||\n              userDetailsForm.get('email').touched)\n          \">{{ validation.message }}</mat-error>\n      </mat-error>\n    </mat-form-field>\n    <mat-form-field class=\"comment-field\" appearance=\"standard\">\n      <mat-label>Message</mat-label>\n      <textarea matInput   placeholder=\"\" formControlName=\"message\"\n        required></textarea>\n      <mat-error *ngFor=\"let validation of validationMessages.message\">\n        <mat-error class=\"error-message\" *ngIf=\"\n              userDetailsForm.get('message').hasError(validation.type) &&\n              (userDetailsForm.get('message').dirty ||\n                userDetailsForm.get('message').touched)\n            \">{{ validation.message }}</mat-error>\n      </mat-error>\n    </mat-form-field>\n    <button mat-raised-button [disabled]=\"!userDetailsForm.valid\" class=\"submit-btn\">\n      Submit\n    </button>\n  </form>\n</mat-card>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/molecules/footer-molecule/footer.component.html":
/*!**************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/shared/molecules/footer-molecule/footer.component.html ***!
  \**************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<footer class=\"secondary-bg footer-container\">\n  <div class=\"container main-footer pt-50 pb-50\">\n    <aside class=\"col-xs-12 footer-sidebar footer-widget-area\" role=\"complementary\">\n      <div class=\"clearfix visible-lg-block\"></div>\n      <div class=\"footer-column footer-column-2 col-md-3 col-sm-6\" *ngFor=\"let columns of footerColumnSlot\">\n        <div id=\"text-18\" class=\"footer-widget  footer-widget-collapse widget_text\">\n          <h5 class=\"widget-title primary-color\">{{columns.name}}</h5>\n          <div class=\"textwidget\">\n            <ul class=\"menu list-unstyled no-margin\" *ngFor=\"let linkData of columns.components.component\">\n              <li><a href=\"{{linkData.url}}\">{{linkData.linkName}}</a></li>\n            </ul>\n          </div>\n        </div>\n      </div>\n    </aside>\n  </div>\n  <div class=\"copyrights-wrapper text-center\">\n    <div class=\"container\">\n      <div class=\"min-footer\">\n        <div class=\"copyright-text text-center\">{{copyRightSlot.content}} </div>\n      </div>\n    </div>\n  </div>\n</footer>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/molecules/header-links-molecule/header-links-molecule.component.html":
/*!***********************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/shared/molecules/header-links-molecule/header-links-molecule.component.html ***!
  \***********************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<section class=\"container-fluid\">\n  <div class=\"top_header\">\n    <ul class=\"top_section_left list-unstyled d-none d-md-block no-margin\">\n      <li class=\"text-white pr-20\">\n        <span class=\"top-icon primary-color\">\n          <i class=\"fa fa-mobile-phone\"></i>\n        </span> {{headerLinks !== undefined ? headerLinks[0].content : ''}}</li>\n      <li class=\"text-white\">\n        <span class=\"top-icon primary-color\">\n          <svg class=\"bi bi-envelope-fill\" width=\"1em\" height=\"1em\" viewBox=\"0 0 20 20\" fill=\"currentColor\"\n            xmlns=\"http://www.w3.org/2000/svg\">\n            <path\n              d=\"M2.05 5.555L10 10.414l7.95-4.859A2 2 0 0016 4H4a2 2 0 00-1.95 1.555zM18 6.697l-5.875 3.59L18 13.743V6.697zm-.168 8.108l-6.675-3.926-1.157.707-1.157-.707-6.675 3.926A2 2 0 004 16h12a2 2 0 001.832-1.195zM2 13.743l5.875-3.456L2 6.697v7.046z\">\n            </path>\n          </svg>\n        </span>\n        {{headerLinks !== undefined ? headerLinks[1].content : ''}}</li>\n    </ul>\n    <ul class=\"top_section_right list-unstyled no-margin no-padding\">\n      <li *ngIf=\"!isLoggedIn\"><a [routerLink]=\"'/login'\" class=\"text-uppercase pr-20\">Login</a></li>\n      <li *ngIf=\"isLoggedIn\"><a [routerLink]=\"'/'\" class=\"text-uppercase pr-20\">{{displayName}}</a></li>\n      <li *ngIf=\"!isLoggedIn\"><a class=\"text-uppercase\" routerLink=\"/register\">Register</a></li>\n      <li *ngIf=\"isLoggedIn\"><a class=\"text-uppercase\" (click)=\"onLogout($event)\">Logout</a></li>\n    </ul>\n  </div>\n</section>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/molecules/header-nav-molecule/header-nav-molecule.component.html":
/*!*******************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/shared/molecules/header-nav-molecule/header-nav-molecule.component.html ***!
  \*******************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<section class=\"container-fluid nav-container\">\n  <app-logo-molecule [logoUrl]=\"logoUrl\" displayType=\"logo-container\"></app-logo-molecule>\n  <nav>\n    <ul>\n      <li *ngFor=\"let link of headerNavLinks\"><a [href]=\"link.url\" alt=\"\">{{link.linkName}}</a></li>\n    </ul>\n  </nav>\n  <app-cart-molecule class=\"cart-container\"></app-cart-molecule>\n</section>\n<section class=\"mobile-nav-container\">\n    <div id=\"mySidenav\" class=\"mobile-sidenav\">\n      <a href=\"javascript:void(0)\" class=\"closebtn\"  (click)=\"closeNav()\">&times;</a>\n      <ul>\n        <li  *ngFor=\"let link of headerNavLinks\"><a [href]=\"link.url\" alt=\"\">{{link.linkName}}</a></li>\n      </ul>\n    </div>\n    <span class=\"hamburger\" style=\"font-size:30px;cursor:pointer\"  (click)=\"openNav()\">&#9776;</span>\n    <app-logo-molecule [logoUrl]=\"logoUrl\" displayType=\"mobile-logo-container\"></app-logo-molecule>\n</section>\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/molecules/login-form-molecule/login-form-molecule.component.html":
/*!*******************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/shared/molecules/login-form-molecule/login-form-molecule.component.html ***!
  \*******************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<mat-card class=\"login-container\">\n  <mat-card-title class=\"title\"><h2>Login </h2></mat-card-title>\n  <mat-error class=\"error-msg\" *ngIf=\"loginError\">Login Failed. Please try again</mat-error>\n  <form class=\"login-us-form\" [formGroup]=\"userLoginForm\" (ngSubmit)=\"onSubmitLoginDetails(userLoginForm.value)\">\n    <mat-form-field class=\"email-field\" appearance=\"standard\">\n      <mat-label>Email</mat-label>\n      <input matInput placeholder=\"\" value=\"\" formControlName=\"email\" required autocomplete=\"off\" />\n      <mat-error *ngFor=\"let validation of validationMessages.email\">\n          <mat-error class=\"error-message\" *ngIf=\"\n          userLoginForm.get('email').hasError(validation.type) &&\n              (userLoginForm.get('email').dirty ||\n              userLoginForm.get('email').touched)\">{{ validation.message }}</mat-error>\n        </mat-error>\n    </mat-form-field>\n  \n    <mat-form-field class=\"password-field \" appearance=\"standard\">      \n      <mat-label>Password</mat-label>\n      <input type=\"password\" matInput placeholder=\"\" value=\"\" formControlName=\"password\" required autocomplete=\"off\" />\n      <mat-error *ngFor=\"let validation of validationMessages.password\">\n        <mat-error class=\"error-message\" *ngIf=\"\n        userLoginForm.get('password').hasError(validation.type) &&\n            (userLoginForm.get('password').dirty ||\n            userLoginForm.get('password').touched)\">{{ validation.message }}</mat-error>\n      </mat-error>\n    </mat-form-field>\n    <div class=\"forgot-pwd\"><a href=\"/\">Forgot Password?</a></div>\n    <button mat-raised-button [disabled]=\"!userLoginForm.valid\" class=\"submit-btn\">\n      Sign In\n    </button>\n    <div class=\"register-link\"><p>Don't have an account</p></div>\n    <button mat-raised-button class=\"register-btn\" routerLink = '/register'>\n      Register\n    </button>\n  </form>\n  </mat-card>\n  ");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/molecules/logo-molecule/logo-molecule.component.html":
/*!*******************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/shared/molecules/logo-molecule/logo-molecule.component.html ***!
  \*******************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class={{displayType}}>\n    <a class=\"logo-img\" routerLink=\"/\" title=\"\" *ngIf=\"logoUrl !== undefined\">\n        <img class=\"img-fluid\" [src]=\"hostName + logoUrl\" alt=\"logo\">\n    </a>\n</div>\n\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/molecules/register-molecule/register-molecule.component.html":
/*!***************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/shared/molecules/register-molecule/register-molecule.component.html ***!
  \***************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n<div class=\"col-lg-12 register-title\">\n    <nav>\n        <span><a routerLink='/'>Home</a></span>\n    </nav>\n    <h1>Create an Account</h1>\n</div>\n\n\n<div class=\"col-md-6 reg-form-wrapper\">\n    <form [formGroup]=\"registerForm\" (ngSubmit)=\"registerUser(registerForm.value)\" autocomplete=\"off\">\n        <p>\n            <mat-form-field appearance=\"standard\">\n                <mat-label>Name</mat-label>\n                <input matInput placeholder=\"Name\" formControlName=\"name\" required >\n                <mat-error *ngFor=\"let validation of validationMessages.name\">\n                    <mat-error class=\"error-message\" *ngIf=\"\n                    registerForm.get('name').hasError(validation.type) &&\n                        (registerForm.get('name').dirty ||\n                        registerForm.get('name').touched)\n                      \">{{ validation.message }}</mat-error>\n                  </mat-error>\n            </mat-form-field>\n        </p>\n        <p>\n            <mat-form-field appearance=\"standard\">\n                <mat-label>Email</mat-label>\n                <input matInput placeholder=\"Email\" formControlName=\"email\" required >\n                <mat-error *ngFor=\"let validation of validationMessages.email\">\n                    <mat-error class=\"error-message\" *ngIf=\"\n                    registerForm.get('email').hasError(validation.type) &&\n                        (registerForm.get('email').dirty ||\n                        registerForm.get('email').touched)\n                      \">{{ validation.message }}</mat-error>\n                  </mat-error>\n            </mat-form-field>\n        </p>\n        <p>\n            <mat-form-field appearance=\"standard\">\n                <mat-label>Mobile</mat-label>\n                <input matInput placeholder=\"Mobile\" formControlName=\"mobile\" required >\n                <mat-error *ngFor=\"let validation of validationMessages.mobile\">\n                    <mat-error class=\"error-message\" *ngIf=\"\n                    registerForm.get('mobile').hasError(validation.type) &&\n                        (registerForm.get('mobile').dirty ||\n                        registerForm.get('mobile').touched)\n                      \">{{ validation.message }}</mat-error>\n                  </mat-error>\n            </mat-form-field>\n        </p>\n        <p>\n            <mat-form-field appearance=\"standard\">\n                <mat-label>Company</mat-label>\n                <input matInput placeholder=\"Company\" formControlName=\"company\" required >\n                <mat-error *ngFor=\"let validation of validationMessages.company\">\n                    <mat-error class=\"error-message\" *ngIf=\"\n                    registerForm.get('company').hasError(validation.type) &&\n                        (registerForm.get('company').dirty ||\n                        registerForm.get('company').touched)\n                      \">{{ validation.message }}</mat-error>\n                  </mat-error>\n            </mat-form-field>\n        </p>\n        <p>\n            <mat-form-field appearance=\"standard\">\n                <mat-label>Select a size range</mat-label>\n                <mat-select disableRipple formControlName=\"size\" required>\n                  <mat-option value=\"1-25\">1-25</mat-option>\n                  <mat-option value=\"25-50\">25-50</mat-option>\n                  <mat-option value=\"50-100\">50-100</mat-option>\n                  <mat-option value=\"100-150\">100-150</mat-option>\n                  <mat-option value=\"150-200\">150-200</mat-option>\n                  <mat-option value=\"200-300\">200-300</mat-option>\n                </mat-select>\n            </mat-form-field>\n        </p>\n        <p>\n            <mat-form-field appearance=\"standard\">\n                <mat-label>Commercial Registration</mat-label>\n                <input matInput placeholder=\"Commercial Registration\" formControlName=\"commercial\" required>\n                <mat-error *ngFor=\"let validation of validationMessages.commercial\">\n                    <mat-error class=\"error-message\" *ngIf=\"\n                    registerForm.get('commercial').hasError(validation.type) &&\n                        (registerForm.get('commercial').dirty ||\n                        registerForm.get('commercial').touched)\n                      \">{{ validation.message }}</mat-error>\n                  </mat-error>\n            </mat-form-field>\n        </p>\n        <p>\n            <button  mat-raised-button [disabled]=\"!registerForm.valid\"  class=\"reg-btn\" >Register</button>\n        </p>\n        <a class=\"btn-link\" routerLink=\"/login\">I already have an account. Sign In</a>\n    </form>\n\n\n</div>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/molecules/saned-section-molecule/saned-section-molecule.component.html":
/*!*************************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/shared/molecules/saned-section-molecule/saned-section-molecule.component.html ***!
  \*************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<mat-card class=\"col-12 saned-section-wrapper\">\n    <mat-card-title class=\"title text-center\">SANED SOLUTIONS </mat-card-title>\n    <ul class=\"domains col-12 mt-50\">\n        <li *ngFor=\"let img of images\" class=\"col-sm-6 col-md-3 text-center\">\n            <a href=\"/\">\n                <img src=\"{{hostName +img.media.url}}\">\n                <p>{{img.headline}}</p>\n            </a>\n        </li>\n    </ul>\n\n    <div *ngFor=\"let logo of clientLogos\" class=\"col-xs-4 col-md-2 mx-auto\">\n        <img  class=\"img-fluid mx-auto img-grayscale\" src= \"{{hostName +logo.media.url}}\" alt=\"{{logo.name}}\" />\n    </div>\n</mat-card>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/molecules/sign-up-molecule/sign-up.component.html":
/*!****************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/shared/molecules/sign-up-molecule/sign-up.component.html ***!
  \****************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<section class=\"news-letter-section\">\n  <form class=\"example-form\">\n    <mat-card-title class=\"title\">SINGUP FOR NEWS LETTER </mat-card-title>\n    <div class=\"row\">\n      <div class=\"sign-up-input-field col-12 mx-auto mt-50\">\n        <input id=\"email\" type=\"email\" class=\"validate\">\n        <button class=\"signup-btn\" type=\"submit\" name=\"action\">Submit\n        </button>\n      </div>\n    </div>\n  </form>\n  </section>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/organisms/footer-organism/footer-organism.component.html":
/*!***********************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/shared/organisms/footer-organism/footer-organism.component.html ***!
  \***********************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("  <app-cx-footer [footerColumnSlot]=\"footerColumnSlot\" [copyRightSlot]=\"this.copyRightSlot\"></app-cx-footer>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/organisms/header-organism/header-organism.component.html":
/*!***********************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/shared/organisms/header-organism/header-organism.component.html ***!
  \***********************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<header class=\"container-fluid\">\n  <app-cx-header-links-molecule [headerLinks]=\"headerLinks\"></app-cx-header-links-molecule>\n  <cx-header-nav-molecule [headerNavLinks]=\"headerNavLinks\" [logoUrl]=\"logoUrl\"></cx-header-nav-molecule>\n</header>\n\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/organisms/home-organism/home-organism.component.html":
/*!*******************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/shared/organisms/home-organism/home-organism.component.html ***!
  \*******************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<cx-carousal-molecule [homeContent]=\"homeContent\"></cx-carousal-molecule>\n<app-saned-section-molecule></app-saned-section-molecule>\n<app-cx-client-logo></app-cx-client-logo>\n<app-cx-sign-up></app-cx-sign-up>\n<app-contact-us-molecule></app-contact-us-molecule>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/organisms/login-organism/login-organism.component.html":
/*!*********************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/shared/organisms/login-organism/login-organism.component.html ***!
  \*********************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<app-login-form-molecule></app-login-form-molecule>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/organisms/not-found-organism/not-found-organism.component.html":
/*!*****************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/shared/organisms/not-found-organism/not-found-organism.component.html ***!
  \*****************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<p>\n  not-found-organism works!\n</p>\n");

/***/ }),

/***/ "./node_modules/tslib/tslib.es6.js":
/*!*****************************************!*\
  !*** ./node_modules/tslib/tslib.es6.js ***!
  \*****************************************/
/*! exports provided: __extends, __assign, __rest, __decorate, __param, __metadata, __awaiter, __generator, __exportStar, __values, __read, __spread, __spreadArrays, __await, __asyncGenerator, __asyncDelegator, __asyncValues, __makeTemplateObject, __importStar, __importDefault */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__extends", function() { return __extends; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__assign", function() { return __assign; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__rest", function() { return __rest; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__decorate", function() { return __decorate; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__param", function() { return __param; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__metadata", function() { return __metadata; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__awaiter", function() { return __awaiter; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__generator", function() { return __generator; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__exportStar", function() { return __exportStar; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__values", function() { return __values; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__read", function() { return __read; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__spread", function() { return __spread; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__spreadArrays", function() { return __spreadArrays; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__await", function() { return __await; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__asyncGenerator", function() { return __asyncGenerator; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__asyncDelegator", function() { return __asyncDelegator; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__asyncValues", function() { return __asyncValues; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__makeTemplateObject", function() { return __makeTemplateObject; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__importStar", function() { return __importStar; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__importDefault", function() { return __importDefault; });
/*! *****************************************************************************
Copyright (c) Microsoft Corporation. All rights reserved.
Licensed under the Apache License, Version 2.0 (the "License"); you may not use
this file except in compliance with the License. You may obtain a copy of the
License at http://www.apache.org/licenses/LICENSE-2.0

THIS CODE IS PROVIDED ON AN *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED
WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE,
MERCHANTABLITY OR NON-INFRINGEMENT.

See the Apache Version 2.0 License for specific language governing permissions
and limitations under the License.
***************************************************************************** */
/* global Reflect, Promise */

var extendStatics = function(d, b) {
    extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return extendStatics(d, b);
};

function __extends(d, b) {
    extendStatics(d, b);
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
}

var __assign = function() {
    __assign = Object.assign || function __assign(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p)) t[p] = s[p];
        }
        return t;
    }
    return __assign.apply(this, arguments);
}

function __rest(s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
            if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                t[p[i]] = s[p[i]];
        }
    return t;
}

function __decorate(decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
}

function __param(paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
}

function __metadata(metadataKey, metadataValue) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(metadataKey, metadataValue);
}

function __awaiter(thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
}

function __generator(thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
}

function __exportStar(m, exports) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}

function __values(o) {
    var m = typeof Symbol === "function" && o[Symbol.iterator], i = 0;
    if (m) return m.call(o);
    return {
        next: function () {
            if (o && i >= o.length) o = void 0;
            return { value: o && o[i++], done: !o };
        }
    };
}

function __read(o, n) {
    var m = typeof Symbol === "function" && o[Symbol.iterator];
    if (!m) return o;
    var i = m.call(o), r, ar = [], e;
    try {
        while ((n === void 0 || n-- > 0) && !(r = i.next()).done) ar.push(r.value);
    }
    catch (error) { e = { error: error }; }
    finally {
        try {
            if (r && !r.done && (m = i["return"])) m.call(i);
        }
        finally { if (e) throw e.error; }
    }
    return ar;
}

function __spread() {
    for (var ar = [], i = 0; i < arguments.length; i++)
        ar = ar.concat(__read(arguments[i]));
    return ar;
}

function __spreadArrays() {
    for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
    for (var r = Array(s), k = 0, i = 0; i < il; i++)
        for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
            r[k] = a[j];
    return r;
};

function __await(v) {
    return this instanceof __await ? (this.v = v, this) : new __await(v);
}

function __asyncGenerator(thisArg, _arguments, generator) {
    if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
    var g = generator.apply(thisArg, _arguments || []), i, q = [];
    return i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () { return this; }, i;
    function verb(n) { if (g[n]) i[n] = function (v) { return new Promise(function (a, b) { q.push([n, v, a, b]) > 1 || resume(n, v); }); }; }
    function resume(n, v) { try { step(g[n](v)); } catch (e) { settle(q[0][3], e); } }
    function step(r) { r.value instanceof __await ? Promise.resolve(r.value.v).then(fulfill, reject) : settle(q[0][2], r); }
    function fulfill(value) { resume("next", value); }
    function reject(value) { resume("throw", value); }
    function settle(f, v) { if (f(v), q.shift(), q.length) resume(q[0][0], q[0][1]); }
}

function __asyncDelegator(o) {
    var i, p;
    return i = {}, verb("next"), verb("throw", function (e) { throw e; }), verb("return"), i[Symbol.iterator] = function () { return this; }, i;
    function verb(n, f) { i[n] = o[n] ? function (v) { return (p = !p) ? { value: __await(o[n](v)), done: n === "return" } : f ? f(v) : v; } : f; }
}

function __asyncValues(o) {
    if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
    var m = o[Symbol.asyncIterator], i;
    return m ? m.call(o) : (o = typeof __values === "function" ? __values(o) : o[Symbol.iterator](), i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () { return this; }, i);
    function verb(n) { i[n] = o[n] && function (v) { return new Promise(function (resolve, reject) { v = o[n](v), settle(resolve, reject, v.done, v.value); }); }; }
    function settle(resolve, reject, d, v) { Promise.resolve(v).then(function(v) { resolve({ value: v, done: d }); }, reject); }
}

function __makeTemplateObject(cooked, raw) {
    if (Object.defineProperty) { Object.defineProperty(cooked, "raw", { value: raw }); } else { cooked.raw = raw; }
    return cooked;
};

function __importStar(mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result.default = mod;
    return result;
}

function __importDefault(mod) {
    return (mod && mod.__esModule) ? mod : { default: mod };
}


/***/ }),

/***/ "./src/app/app-routing.module.ts":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _app_routes__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./app.routes */ "./src/app/app.routes.ts");




let AppRoutingModule = class AppRoutingModule {
};
AppRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forRoot(_app_routes__WEBPACK_IMPORTED_MODULE_3__["routes"])],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })
], AppRoutingModule);



/***/ }),

/***/ "./src/app/app.component.scss":
/*!************************************!*\
  !*** ./src/app/app.component.scss ***!
  \************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FwcC5jb21wb25lbnQuc2NzcyJ9 */");

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _services_common_utility_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./services/common-utility-service */ "./src/app/services/common-utility-service.ts");
/* harmony import */ var _services_login_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./services/login.service */ "./src/app/services/login.service.ts");




let AppComponent = class AppComponent {
    constructor(utilityService, loginService) {
        this.utilityService = utilityService;
        this.loginService = loginService;
        this.title = 'ng-Freelance';
        this.loginService.userContext.subscribe(context => {
            this.userContext = context;
        });
    }
    ngOnInit() {
        if (this.utilityService.getCookie('isAuthenticated')) {
            this.userContext.displayUID = this.utilityService.getCookie('userName');
            this.userContext.displayName = this.utilityService.getCookie('displayUid');
            this.userContext.isAuthenticated = this.utilityService.getCookie('isAuthenticated');
            this.loginService.changeUserContext(this.userContext);
        }
    }
};
AppComponent.ctorParameters = () => [
    { type: _services_common_utility_service__WEBPACK_IMPORTED_MODULE_2__["CommonUtilityService"] },
    { type: _services_login_service__WEBPACK_IMPORTED_MODULE_3__["LoginService"] }
];
AppComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-root',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./app.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/app.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./app.component.scss */ "./src/app/app.component.scss")).default]
    })
], AppComponent);



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm2015/platform-browser.js");
/* harmony import */ var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/platform-browser/animations */ "./node_modules/@angular/platform-browser/fesm2015/animations.js");
/* harmony import */ var ngx_cookie__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ngx-cookie */ "./node_modules/ngx-cookie/fesm2015/ngx-cookie.js");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./app-routing.module */ "./src/app/app-routing.module.ts");
/* harmony import */ var _shared_shared_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./shared/shared.module */ "./src/app/shared/shared.module.ts");
/* harmony import */ var _services_services_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./services/services.module */ "./src/app/services/services.module.ts");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _spartacus_storefront__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @spartacus/storefront */ "./node_modules/@spartacus/storefront/fesm2015/spartacus-storefront.js");
/* harmony import */ var _spartacus_assets__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @spartacus/assets */ "./node_modules/@spartacus/assets/fesm2015/spartacus-assets.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ../environments/environment */ "./src/environments/environment.ts");













let AppModule = class AppModule {
};
AppModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        declarations: [
            _app_component__WEBPACK_IMPORTED_MODULE_9__["AppComponent"]
        ],
        imports: [
            _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__["BrowserModule"].withServerTransition({ appId: 'serverApp' }),
            _spartacus_storefront__WEBPACK_IMPORTED_MODULE_10__["B2cStorefrontModule"].withConfig({
                backend: {
                    occ: {
                        baseUrl: _environments_environment__WEBPACK_IMPORTED_MODULE_12__["environment"].hostName,
                        legacy: false,
                    },
                },
                context: {
                    urlParameters: ['baseSite', 'language', 'currency'],
                    baseSite: [
                        'electronics-spa',
                        'electronics',
                        'apparel-de',
                        'apparel-uk',
                        'apparel-uk-spa',
                    ],
                },
                // custom routing configuration for e2e testing
                routing: {
                    routes: {
                        product: {
                            paths: ['product/:productCode/:name', 'product/:productCode'],
                        },
                    },
                },
                // we bring in static translations to be up and running soon right away
                i18n: {
                    resources: _spartacus_assets__WEBPACK_IMPORTED_MODULE_11__["translations"],
                    chunks: _spartacus_assets__WEBPACK_IMPORTED_MODULE_11__["translationChunksConfig"],
                    fallbackLang: 'en',
                },
                features: {
                    level: '1.5',
                    anonymousConsents: true,
                },
            }),
            _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClientModule"],
            _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__["BrowserTransferStateModule"],
            _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_4__["BrowserAnimationsModule"],
            _shared_shared_module__WEBPACK_IMPORTED_MODULE_7__["SharedModule"],
            _services_services_module__WEBPACK_IMPORTED_MODULE_8__["ServicesModule"],
            _app_routing_module__WEBPACK_IMPORTED_MODULE_6__["AppRoutingModule"],
            ngx_cookie__WEBPACK_IMPORTED_MODULE_5__["CookieModule"].forRoot()
        ],
        providers: [],
        bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_9__["AppComponent"]]
    })
], AppModule);



/***/ }),

/***/ "./src/app/app.routes.ts":
/*!*******************************!*\
  !*** ./src/app/app.routes.ts ***!
  \*******************************/
/*! exports provided: routes */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "routes", function() { return routes; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _shared_organisms_home_organism_home_organism_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./shared/organisms/home-organism/home-organism.component */ "./src/app/shared/organisms/home-organism/home-organism.component.ts");
/* harmony import */ var _shared_organisms_not_found_organism_not_found_organism_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./shared/organisms/not-found-organism/not-found-organism.component */ "./src/app/shared/organisms/not-found-organism/not-found-organism.component.ts");
/* harmony import */ var _services_home_resolver_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./services/home-resolver.service */ "./src/app/services/home-resolver.service.ts");
/* harmony import */ var _shared_molecules_register_molecule_register_molecule_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./shared/molecules/register-molecule/register-molecule.component */ "./src/app/shared/molecules/register-molecule/register-molecule.component.ts");
/* harmony import */ var _shared_organisms_login_organism_login_organism_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./shared/organisms/login-organism/login-organism.component */ "./src/app/shared/organisms/login-organism/login-organism.component.ts");






const routes = [{
        path: '',
        component: _shared_organisms_home_organism_home_organism_component__WEBPACK_IMPORTED_MODULE_1__["HomeOrganismComponent"],
        data: [{
                pageName: 'Home Page',
            }],
        resolve: {
            homeData: _services_home_resolver_service__WEBPACK_IMPORTED_MODULE_3__["HomeResolverService"]
        }
    },
    {
        path: 'register',
        component: _shared_molecules_register_molecule_register_molecule_component__WEBPACK_IMPORTED_MODULE_4__["RegisterMoleculeComponent"]
    },
    {
        path: 'login',
        component: _shared_organisms_login_organism_login_organism_component__WEBPACK_IMPORTED_MODULE_5__["LoginOrganismComponent"],
        data: [{
                pageName: 'Login Page',
            }],
    },
    {
        path: 'not-found',
        component: _shared_organisms_not_found_organism_not_found_organism_component__WEBPACK_IMPORTED_MODULE_2__["NotFoundOrganismComponent"],
        pathMatch: 'full',
        data: [{
                pageName: 'Not Found',
            }]
    },
    {
        path: '**',
        redirectTo: 'not-found',
        data: [{
                pageName: 'Not Found',
            }]
    },
    {
        path: 'products',
        component: _shared_organisms_login_organism_login_organism_component__WEBPACK_IMPORTED_MODULE_5__["LoginOrganismComponent"],
        pathMatch: 'full',
        data: [{
                pageName: 'Products Listing',
            }]
    }];


/***/ }),

/***/ "./src/app/material.module.ts":
/*!************************************!*\
  !*** ./src/app/material.module.ts ***!
  \************************************/
/*! exports provided: MaterialModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MaterialModule", function() { return MaterialModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm2015/material.js");



const modules = [
    _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatButtonModule"],
    _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatInputModule"],
    _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatRippleModule"],
    _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatIconModule"],
    _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatDividerModule"],
    _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatCardModule"],
    _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSelectModule"],
    _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatFormFieldModule"],
    _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatDatepickerModule"],
    _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatNativeDateModule"],
    _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatCheckboxModule"],
    _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSnackBarModule"]
];
let MaterialModule = class MaterialModule {
};
MaterialModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [...modules],
        exports: [...modules]
    })
], MaterialModule);



/***/ }),

/***/ "./src/app/services/common-utility-service.ts":
/*!****************************************************!*\
  !*** ./src/app/services/common-utility-service.ts ***!
  \****************************************************/
/*! exports provided: CommonUtilityService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CommonUtilityService", function() { return CommonUtilityService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var ngx_cookie__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ngx-cookie */ "./node_modules/ngx-cookie/fesm2015/ngx-cookie.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm2015/platform-browser.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../environments/environment */ "./src/environments/environment.ts");







/**
 * Utility service for the application
 * Common utility functions that can be used throughout the application
 * @author Mohamed Omar Farook
 */
let CommonUtilityService = class CommonUtilityService {
    constructor(plateformId, http, state, cookieService) {
        this.plateformId = plateformId;
        this.http = http;
        this.state = state;
        this.cookieService = cookieService;
    }
    /**
     * @param input object to be validated
     * @returns true if object is undefined or empty, otherwise false
     */
    isUndefinedOrEmpty(input) {
        if (undefined !== input && '' !== input) {
            return false;
        }
        else {
            return true;
        }
    }
    /**
     * @param input object to be validated
     * @returns true if object is undefined or null, otherwise false
     */
    isNullOrUndefined(input) {
        if (undefined !== input && null !== input) {
            return false;
        }
        else {
            return true;
        }
    }
    /**
     * Checks if application is running in browser
     */
    isBrowser() {
        if (Object(_angular_common__WEBPACK_IMPORTED_MODULE_2__["isPlatformBrowser"])(this.plateformId)) {
            return true;
        }
        else {
            return false;
        }
    }
    /**
     * @param Endpoint url
     * @returns Get request response from server
     */
    getRequest(url, header) {
        let api = this.getApi(url);
        return this.http.get(api, header);
    }
    /**
     * @param Endpoint url
     * @returns Post request response from server
     */
    postRequest(url, request, options) {
        let api = this.getApi(url);
        return this.http.post(api, request, options);
    }
    /**
     * @param url
     * @returns generated endpoint url
     */
    getApi(url) {
        if (url === '../../assets/mockApi/home.json') {
            return url;
        }
        return _environments_environment__WEBPACK_IMPORTED_MODULE_6__["environment"].hostName + url;
    }
    getAuth() {
        const httpHeaders = new _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpHeaders"]({ 'Content-Type': 'application/x-www-form-urlencoded;' });
        const url = _environments_environment__WEBPACK_IMPORTED_MODULE_6__["environment"].hostName + _environments_environment__WEBPACK_IMPORTED_MODULE_6__["environment"].oAuthAPI;
        const request = new _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpParams"]()
            .set('client_id', 'trusted_client')
            .set('client_secret', 'secret')
            .set('grant_type', 'client_credentials')
            .set('scope', 'extended');
        return this.http.post(url, request.toString(), { headers: httpHeaders });
    }
    setCookie(key, value) {
        this.cookieService.put(key, value, {
            path: '/',
            domain: window.location.hostname
        });
    }
    getCookie(key) {
        return this.cookieService.get(key);
    }
    removeCookie(key) {
        this.cookieService.remove(key, {
            path: '/',
            domain: window.location.hostname
        });
    }
};
CommonUtilityService.ctorParameters = () => [
    { type: Object, decorators: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"], args: [_angular_core__WEBPACK_IMPORTED_MODULE_1__["PLATFORM_ID"],] }] },
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"] },
    { type: _angular_platform_browser__WEBPACK_IMPORTED_MODULE_5__["TransferState"] },
    { type: ngx_cookie__WEBPACK_IMPORTED_MODULE_4__["CookieService"] }
];
CommonUtilityService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__param"](0, Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"])(_angular_core__WEBPACK_IMPORTED_MODULE_1__["PLATFORM_ID"]))
], CommonUtilityService);



/***/ }),

/***/ "./src/app/services/home-page.service.ts":
/*!***********************************************!*\
  !*** ./src/app/services/home-page.service.ts ***!
  \***********************************************/
/*! exports provided: HomePageService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomePageService", function() { return HomePageService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var _services_common_utility_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../services/common-utility-service */ "./src/app/services/common-utility-service.ts");




let HomePageService = class HomePageService {
    constructor(http, utilityService) {
        this.http = http;
        this.utilityService = utilityService;
    }
    postUserMessage(req) {
        const url = '';
        return this.http.post(url, req);
    }
};
HomePageService.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] },
    { type: _services_common_utility_service__WEBPACK_IMPORTED_MODULE_3__["CommonUtilityService"] }
];
HomePageService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root',
    })
], HomePageService);



/***/ }),

/***/ "./src/app/services/home-resolver.service.ts":
/*!***************************************************!*\
  !*** ./src/app/services/home-resolver.service.ts ***!
  \***************************************************/
/*! exports provided: HomeResolverService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeResolverService", function() { return HomeResolverService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _common_utility_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./common-utility-service */ "./src/app/services/common-utility-service.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../environments/environment */ "./src/environments/environment.ts");




let HomeResolverService = class HomeResolverService {
    constructor(utilityService) {
        this.utilityService = utilityService;
    }
    resolve() {
        return this.utilityService.getRequest(_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].homePageEndPoint, '');
    }
};
HomeResolverService.ctorParameters = () => [
    { type: _common_utility_service__WEBPACK_IMPORTED_MODULE_2__["CommonUtilityService"] }
];
HomeResolverService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])()
], HomeResolverService);



/***/ }),

/***/ "./src/app/services/login.service.ts":
/*!*******************************************!*\
  !*** ./src/app/services/login.service.ts ***!
  \*******************************************/
/*! exports provided: LoginService, UserContext */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginService", function() { return LoginService; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserContext", function() { return UserContext; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");
/* harmony import */ var _services_common_utility_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../services/common-utility-service */ "./src/app/services/common-utility-service.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../environments/environment */ "./src/environments/environment.ts");






let LoginService = class LoginService {
    constructor(http, utilityService) {
        this.http = http;
        this.utilityService = utilityService;
        this.userContextData = new rxjs__WEBPACK_IMPORTED_MODULE_3__["BehaviorSubject"](new UserContext('', '', false));
        this.userContext = this.userContextData.asObservable();
        this.fetchUserDetails = (token, username) => {
            let httpHeaders = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]()
                .set('Content-Type', 'application/json')
                .set('Authorization', `Bearer${token}`);
            let options = {
                headers: httpHeaders
            };
            let loginUrl = _environments_environment__WEBPACK_IMPORTED_MODULE_5__["environment"].loginEndpoint + username + '?';
            return this.utilityService.getRequest(loginUrl, options);
        };
    }
    changeUserContext(data) {
        this.userContextData.next(data);
    }
    userLogin(req) {
        const request = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpParams"]()
            .set('client_id', 'trusted_client')
            .set('client_secret', 'secret')
            .set('grant_type', 'client_credentials')
            .set('scope', 'extended')
            .set('username', req.username)
            .set('password', req.password);
        let httpHeaders = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]()
            .set('Content-Type', 'application/x-www-form-urlencoded')
            .set('Accept-Charset', 'UTF-8');
        let options = {
            headers: httpHeaders
        };
        let tokenUrl = _environments_environment__WEBPACK_IMPORTED_MODULE_5__["environment"].tokenEndpoint;
        return this.utilityService.postRequest(tokenUrl, request, options);
    }
};
LoginService.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] },
    { type: _services_common_utility_service__WEBPACK_IMPORTED_MODULE_4__["CommonUtilityService"] }
];
LoginService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root',
    }),
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])()
], LoginService);

class UserContext {
    constructor(displayUID, displayName, isAuthenticated) {
        this.displayUID = displayUID;
        this.displayName = displayName;
        this.isAuthenticated = isAuthenticated;
        this.displayUID = displayUID;
        this.displayName = displayName;
        this.isAuthenticated = isAuthenticated;
    }
}


/***/ }),

/***/ "./src/app/services/register-user.service.ts":
/*!***************************************************!*\
  !*** ./src/app/services/register-user.service.ts ***!
  \***************************************************/
/*! exports provided: RegisterUserService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RegisterUserService", function() { return RegisterUserService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../environments/environment */ "./src/environments/environment.ts");




let RegisterUserService = class RegisterUserService {
    constructor(http) {
        this.http = http;
    }
    registerUserDetails(token, req) {
        const authToken = 'Bearer ' + token;
        const httpHeaders = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
            'Content-Type': 'application/x-www-form-urlencoded;',
            'Authorization': authToken
        });
        const request = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpParams"]()
            .set('name', req.name)
            .set('email', req.email)
            .set('contact', req.mobile)
            .set('company', req.company)
            .set('sizeRange', req.size)
            .set('commercialRegistration', req.commercial);
        const url = _environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].hostName + _environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].registerUserAPI;
        return this.http.post(url, request.toString(), { headers: httpHeaders });
    }
};
RegisterUserService.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] }
];
RegisterUserService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    })
], RegisterUserService);



/***/ }),

/***/ "./src/app/services/services.module.ts":
/*!*********************************************!*\
  !*** ./src/app/services/services.module.ts ***!
  \*********************************************/
/*! exports provided: ServicesModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ServicesModule", function() { return ServicesModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var _common_utility_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./common-utility-service */ "./src/app/services/common-utility-service.ts");
/* harmony import */ var _home_page_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./home-page.service */ "./src/app/services/home-page.service.ts");
/* harmony import */ var _home_resolver_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./home-resolver.service */ "./src/app/services/home-resolver.service.ts");
/* harmony import */ var _register_user_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./register-user.service */ "./src/app/services/register-user.service.ts");
/* harmony import */ var _login_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./login.service */ "./src/app/services/login.service.ts");









let ServicesModule = class ServicesModule {
};
ServicesModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClientModule"]
        ],
        exports: [],
        declarations: [],
        providers: [
            _common_utility_service__WEBPACK_IMPORTED_MODULE_4__["CommonUtilityService"],
            _home_page_service__WEBPACK_IMPORTED_MODULE_5__["HomePageService"],
            _home_resolver_service__WEBPACK_IMPORTED_MODULE_6__["HomeResolverService"],
            _register_user_service__WEBPACK_IMPORTED_MODULE_7__["RegisterUserService"],
            _login_service__WEBPACK_IMPORTED_MODULE_8__["LoginService"]
        ],
    })
], ServicesModule);



/***/ }),

/***/ "./src/app/shared/directives/directives.module.ts":
/*!********************************************************!*\
  !*** ./src/app/shared/directives/directives.module.ts ***!
  \********************************************************/
/*! exports provided: DirectivesModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DirectivesModule", function() { return DirectivesModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");



let DirectivesModule = class DirectivesModule {
};
DirectivesModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"]
        ],
        exports: [],
        declarations: [],
        providers: [],
    })
], DirectivesModule);



/***/ }),

/***/ "./src/app/shared/molecules/carousal-molecule/carousal-molecule.component.scss":
/*!*************************************************************************************!*\
  !*** ./src/app/shared/molecules/carousal-molecule/carousal-molecule.component.scss ***!
  \*************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("/********************Custom Variables **************************/\n.text-white {\n  color: #fff;\n}\n.secondary-bg {\n  background: #000;\n}\n.primary-color {\n  color: #a7f108;\n}\n.no-margin {\n  margin: 0;\n}\n.no-padding {\n  padding: 0;\n}\n.margin-auto {\n  margin: 0 auto;\n}\n.font-bold {\n  font-weight: 800;\n}\n.font-semi-bold {\n  font-weight: 600;\n}\n.mb-30 {\n  margin-bottom: 30px;\n}\n.mb-50 {\n  margin-bottom: 50px;\n}\n.pr-20 {\n  padding-right: 20px;\n}\n.bg-white {\n  background: #fff;\n}\nimg {\n  width: 100% !important;\n}\n.carousel-inner {\n  margin: 0;\n}\n.carousel-item {\n  height: 500px;\n}\n.carousel-indicators > .active {\n  background-color: #a7f108;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi92YXIvd3d3L2h0bWwvanMtc3RvcmVmcm9udC9zcGFydGFjdXNzdG9yZS9zcmMvYXNzZXRzL2Nzcy92YXJpYWJsZS5zY3NzIiwic3JjL2FwcC9zaGFyZWQvbW9sZWN1bGVzL2Nhcm91c2FsLW1vbGVjdWxlL2Nhcm91c2FsLW1vbGVjdWxlLmNvbXBvbmVudC5zY3NzIiwiL3Zhci93d3cvaHRtbC9qcy1zdG9yZWZyb250L3NwYXJ0YWN1c3N0b3JlL3NyYy9hcHAvc2hhcmVkL21vbGVjdWxlcy9jYXJvdXNhbC1tb2xlY3VsZS9jYXJvdXNhbC1tb2xlY3VsZS5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxnRUFBQTtBQU1BO0VBQ0ksV0FBQTtBQ0pKO0FETUU7RUFDRSxnQkFBQTtBQ0hKO0FES0U7RUFDRSxjQUFBO0FDRko7QURJRTtFQUNFLFNBQUE7QUNESjtBREdFO0VBQ0UsVUFBQTtBQ0FKO0FERUU7RUFDRSxjQUFBO0FDQ0o7QURDRTtFQUNFLGdCQUFBO0FDRUo7QURBRTtFQUNFLGdCQUFBO0FDR0o7QURERTtFQUNFLG1CQUFBO0FDSUo7QURGRTtFQUNFLG1CQUFBO0FDS0o7QURIRTtFQUNFLG1CQUFBO0FDTUo7QURKRTtFQUNFLGdCQUFBO0FDT0o7QUM5Q0E7RUFDSSxzQkFBQTtBRGlESjtBQy9DQTtFQUNJLFNBQUE7QURrREo7QUNoREE7RUFDSSxhQUFBO0FEbURKO0FDakRBO0VBQ0kseUJGUlk7QUM0RGhCIiwiZmlsZSI6InNyYy9hcHAvc2hhcmVkL21vbGVjdWxlcy9jYXJvdXNhbC1tb2xlY3VsZS9jYXJvdXNhbC1tb2xlY3VsZS5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi8qKioqKioqKioqKioqKioqKioqKkN1c3RvbSBWYXJpYWJsZXMgKioqKioqKioqKioqKioqKioqKioqKioqKiovXG4kd2hpdGU6ICNmZmZmZmY7XG4kYmxhY2s6ICMwMDAwMDA7XG4kcHJpbWFyeS1jb2xvcjogI2E3ZjEwODtcbiRib3JkZXItY29sb3I6ICByZ2JhKDI1NSwgMjU1LCAyNTUsIDAuNik7XG4kZm9ybS1iZzogI2Y3ZjdmNztcbi50ZXh0LXdoaXRlIHtcbiAgICBjb2xvcjogI2ZmZjtcbiAgfVxuICAuc2Vjb25kYXJ5LWJnIHtcbiAgICBiYWNrZ3JvdW5kOiAjMDAwO1xuICB9XG4gIC5wcmltYXJ5LWNvbG9yIHtcbiAgICBjb2xvcjogI2E3ZjEwODtcbiAgfVxuICAubm8tbWFyZ2luIHtcbiAgICBtYXJnaW46IDA7XG4gIH1cbiAgLm5vLXBhZGRpbmcge1xuICAgIHBhZGRpbmc6IDA7XG4gIH1cbiAgLm1hcmdpbi1hdXRvIHtcbiAgICBtYXJnaW46IDAgYXV0bztcbiAgfVxuICAuZm9udC1ib2xkIHtcbiAgICBmb250LXdlaWdodDogODAwO1xuICB9XG4gIC5mb250LXNlbWktYm9sZCB7XG4gICAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgfVxuICAubWItMzAge1xuICAgIG1hcmdpbi1ib3R0b206IDMwcHg7XG4gIH1cbiAgLm1iLTUwIHtcbiAgICBtYXJnaW4tYm90dG9tOiA1MHB4O1xuICB9XG4gIC5wci0yMCB7XG4gICAgcGFkZGluZy1yaWdodDogMjBweDtcbiAgfVxuICAuYmctd2hpdGUge1xuICAgIGJhY2tncm91bmQ6ICNmZmY7XG4gIH0iLCIvKioqKioqKioqKioqKioqKioqKipDdXN0b20gVmFyaWFibGVzICoqKioqKioqKioqKioqKioqKioqKioqKioqL1xuLnRleHQtd2hpdGUge1xuICBjb2xvcjogI2ZmZjtcbn1cblxuLnNlY29uZGFyeS1iZyB7XG4gIGJhY2tncm91bmQ6ICMwMDA7XG59XG5cbi5wcmltYXJ5LWNvbG9yIHtcbiAgY29sb3I6ICNhN2YxMDg7XG59XG5cbi5uby1tYXJnaW4ge1xuICBtYXJnaW46IDA7XG59XG5cbi5uby1wYWRkaW5nIHtcbiAgcGFkZGluZzogMDtcbn1cblxuLm1hcmdpbi1hdXRvIHtcbiAgbWFyZ2luOiAwIGF1dG87XG59XG5cbi5mb250LWJvbGQge1xuICBmb250LXdlaWdodDogODAwO1xufVxuXG4uZm9udC1zZW1pLWJvbGQge1xuICBmb250LXdlaWdodDogNjAwO1xufVxuXG4ubWItMzAge1xuICBtYXJnaW4tYm90dG9tOiAzMHB4O1xufVxuXG4ubWItNTAge1xuICBtYXJnaW4tYm90dG9tOiA1MHB4O1xufVxuXG4ucHItMjAge1xuICBwYWRkaW5nLXJpZ2h0OiAyMHB4O1xufVxuXG4uYmctd2hpdGUge1xuICBiYWNrZ3JvdW5kOiAjZmZmO1xufVxuXG5pbWcge1xuICB3aWR0aDogMTAwJSAhaW1wb3J0YW50O1xufVxuXG4uY2Fyb3VzZWwtaW5uZXIge1xuICBtYXJnaW46IDA7XG59XG5cbi5jYXJvdXNlbC1pdGVtIHtcbiAgaGVpZ2h0OiA1MDBweDtcbn1cblxuLmNhcm91c2VsLWluZGljYXRvcnMgPiAuYWN0aXZlIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2E3ZjEwODtcbn0iLCJAaW1wb3J0IFwiLi4vLi4vLi4vLi4vYXNzZXRzL2Nzcy9jb25zdGFudHMuc2Nzc1wiO1xuaW1nIHtcbiAgICB3aWR0aDogMTAwJSAhaW1wb3J0YW50O1xufVxuLmNhcm91c2VsLWlubmVyIHtcbiAgICBtYXJnaW46IDA7XG59XG4uY2Fyb3VzZWwtaXRlbSB7XG4gICAgaGVpZ2h0OiA1MDBweDtcbn1cbi5jYXJvdXNlbC1pbmRpY2F0b3JzID4gLmFjdGl2ZSB7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogJHByaW1hcnktY29sb3I7XG59Il19 */");

/***/ }),

/***/ "./src/app/shared/molecules/carousal-molecule/carousal-molecule.component.ts":
/*!***********************************************************************************!*\
  !*** ./src/app/shared/molecules/carousal-molecule/carousal-molecule.component.ts ***!
  \***********************************************************************************/
/*! exports provided: CarousalMoleculeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CarousalMoleculeComponent", function() { return CarousalMoleculeComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../environments/environment */ "./src/environments/environment.ts");



let CarousalMoleculeComponent = class CarousalMoleculeComponent {
    constructor() {
        this.hostName = _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].hostName;
    }
};
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])()
], CarousalMoleculeComponent.prototype, "homeContent", void 0);
CarousalMoleculeComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'cx-carousal-molecule',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./carousal-molecule.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/molecules/carousal-molecule/carousal-molecule.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./carousal-molecule.component.scss */ "./src/app/shared/molecules/carousal-molecule/carousal-molecule.component.scss")).default]
    })
], CarousalMoleculeComponent);



/***/ }),

/***/ "./src/app/shared/molecules/cart-molecule/cart-molecule.component.scss":
/*!*****************************************************************************!*\
  !*** ./src/app/shared/molecules/cart-molecule/cart-molecule.component.scss ***!
  \*****************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("/********************Custom Variables **************************/\n.text-white {\n  color: #fff;\n}\n.secondary-bg {\n  background: #000;\n}\n.primary-color {\n  color: #a7f108;\n}\n.no-margin {\n  margin: 0;\n}\n.no-padding {\n  padding: 0;\n}\n.margin-auto {\n  margin: 0 auto;\n}\n.font-bold {\n  font-weight: 800;\n}\n.font-semi-bold {\n  font-weight: 600;\n}\n.mb-30 {\n  margin-bottom: 30px;\n}\n.mb-50 {\n  margin-bottom: 50px;\n}\n.pr-20 {\n  padding-right: 20px;\n}\n.bg-white {\n  background: #fff;\n}\n.cart-wrapper a {\n  position: relative;\n}\n.cart-wrapper a .cart-icon i {\n  font-size: 2em;\n  color: #a7f108;\n}\n.cart-wrapper a .cart-count {\n  position: absolute;\n  width: 22px;\n  height: 22px;\n  font-size: 12px;\n  line-height: 22px;\n  left: 15px;\n  top: -22px;\n  background: #ffffff;\n  border-radius: 50%;\n  text-align: center;\n  color: #000000;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi92YXIvd3d3L2h0bWwvanMtc3RvcmVmcm9udC9zcGFydGFjdXNzdG9yZS9zcmMvYXNzZXRzL2Nzcy92YXJpYWJsZS5zY3NzIiwic3JjL2FwcC9zaGFyZWQvbW9sZWN1bGVzL2NhcnQtbW9sZWN1bGUvY2FydC1tb2xlY3VsZS5jb21wb25lbnQuc2NzcyIsIi92YXIvd3d3L2h0bWwvanMtc3RvcmVmcm9udC9zcGFydGFjdXNzdG9yZS9zcmMvYXBwL3NoYXJlZC9tb2xlY3VsZXMvY2FydC1tb2xlY3VsZS9jYXJ0LW1vbGVjdWxlLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLGdFQUFBO0FBTUE7RUFDSSxXQUFBO0FDSko7QURNRTtFQUNFLGdCQUFBO0FDSEo7QURLRTtFQUNFLGNBQUE7QUNGSjtBRElFO0VBQ0UsU0FBQTtBQ0RKO0FER0U7RUFDRSxVQUFBO0FDQUo7QURFRTtFQUNFLGNBQUE7QUNDSjtBRENFO0VBQ0UsZ0JBQUE7QUNFSjtBREFFO0VBQ0UsZ0JBQUE7QUNHSjtBRERFO0VBQ0UsbUJBQUE7QUNJSjtBREZFO0VBQ0UsbUJBQUE7QUNLSjtBREhFO0VBQ0UsbUJBQUE7QUNNSjtBREpFO0VBQ0UsZ0JBQUE7QUNPSjtBQzdDSTtFQUNJLGtCQUFBO0FEZ0RSO0FDOUNZO0VBQ0ksY0FBQTtFQUNBLGNGSkE7QUNvRGhCO0FDN0NRO0VBQ0ksa0JBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtFQUNBLGVBQUE7RUFDQSxpQkFBQTtFQUNBLFVBQUE7RUFDQSxVQUFBO0VBQ0EsbUJGakJKO0VFa0JJLGtCQUFBO0VBQ0Esa0JBQUE7RUFDQSxjRm5CSjtBQ2tFUiIsImZpbGUiOiJzcmMvYXBwL3NoYXJlZC9tb2xlY3VsZXMvY2FydC1tb2xlY3VsZS9jYXJ0LW1vbGVjdWxlLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLyoqKioqKioqKioqKioqKioqKioqQ3VzdG9tIFZhcmlhYmxlcyAqKioqKioqKioqKioqKioqKioqKioqKioqKi9cbiR3aGl0ZTogI2ZmZmZmZjtcbiRibGFjazogIzAwMDAwMDtcbiRwcmltYXJ5LWNvbG9yOiAjYTdmMTA4O1xuJGJvcmRlci1jb2xvcjogIHJnYmEoMjU1LCAyNTUsIDI1NSwgMC42KTtcbiRmb3JtLWJnOiAjZjdmN2Y3O1xuLnRleHQtd2hpdGUge1xuICAgIGNvbG9yOiAjZmZmO1xuICB9XG4gIC5zZWNvbmRhcnktYmcge1xuICAgIGJhY2tncm91bmQ6ICMwMDA7XG4gIH1cbiAgLnByaW1hcnktY29sb3Ige1xuICAgIGNvbG9yOiAjYTdmMTA4O1xuICB9XG4gIC5uby1tYXJnaW4ge1xuICAgIG1hcmdpbjogMDtcbiAgfVxuICAubm8tcGFkZGluZyB7XG4gICAgcGFkZGluZzogMDtcbiAgfVxuICAubWFyZ2luLWF1dG8ge1xuICAgIG1hcmdpbjogMCBhdXRvO1xuICB9XG4gIC5mb250LWJvbGQge1xuICAgIGZvbnQtd2VpZ2h0OiA4MDA7XG4gIH1cbiAgLmZvbnQtc2VtaS1ib2xkIHtcbiAgICBmb250LXdlaWdodDogNjAwO1xuICB9XG4gIC5tYi0zMCB7XG4gICAgbWFyZ2luLWJvdHRvbTogMzBweDtcbiAgfVxuICAubWItNTAge1xuICAgIG1hcmdpbi1ib3R0b206IDUwcHg7XG4gIH1cbiAgLnByLTIwIHtcbiAgICBwYWRkaW5nLXJpZ2h0OiAyMHB4O1xuICB9XG4gIC5iZy13aGl0ZSB7XG4gICAgYmFja2dyb3VuZDogI2ZmZjtcbiAgfSIsIi8qKioqKioqKioqKioqKioqKioqKkN1c3RvbSBWYXJpYWJsZXMgKioqKioqKioqKioqKioqKioqKioqKioqKiovXG4udGV4dC13aGl0ZSB7XG4gIGNvbG9yOiAjZmZmO1xufVxuXG4uc2Vjb25kYXJ5LWJnIHtcbiAgYmFja2dyb3VuZDogIzAwMDtcbn1cblxuLnByaW1hcnktY29sb3Ige1xuICBjb2xvcjogI2E3ZjEwODtcbn1cblxuLm5vLW1hcmdpbiB7XG4gIG1hcmdpbjogMDtcbn1cblxuLm5vLXBhZGRpbmcge1xuICBwYWRkaW5nOiAwO1xufVxuXG4ubWFyZ2luLWF1dG8ge1xuICBtYXJnaW46IDAgYXV0bztcbn1cblxuLmZvbnQtYm9sZCB7XG4gIGZvbnQtd2VpZ2h0OiA4MDA7XG59XG5cbi5mb250LXNlbWktYm9sZCB7XG4gIGZvbnQtd2VpZ2h0OiA2MDA7XG59XG5cbi5tYi0zMCB7XG4gIG1hcmdpbi1ib3R0b206IDMwcHg7XG59XG5cbi5tYi01MCB7XG4gIG1hcmdpbi1ib3R0b206IDUwcHg7XG59XG5cbi5wci0yMCB7XG4gIHBhZGRpbmctcmlnaHQ6IDIwcHg7XG59XG5cbi5iZy13aGl0ZSB7XG4gIGJhY2tncm91bmQ6ICNmZmY7XG59XG5cbi5jYXJ0LXdyYXBwZXIgYSB7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbn1cbi5jYXJ0LXdyYXBwZXIgYSAuY2FydC1pY29uIGkge1xuICBmb250LXNpemU6IDJlbTtcbiAgY29sb3I6ICNhN2YxMDg7XG59XG4uY2FydC13cmFwcGVyIGEgLmNhcnQtY291bnQge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHdpZHRoOiAyMnB4O1xuICBoZWlnaHQ6IDIycHg7XG4gIGZvbnQtc2l6ZTogMTJweDtcbiAgbGluZS1oZWlnaHQ6IDIycHg7XG4gIGxlZnQ6IDE1cHg7XG4gIHRvcDogLTIycHg7XG4gIGJhY2tncm91bmQ6ICNmZmZmZmY7XG4gIGJvcmRlci1yYWRpdXM6IDUwJTtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBjb2xvcjogIzAwMDAwMDtcbn0iLCJAaW1wb3J0IFwiLi4vLi4vLi4vLi4vYXNzZXRzL2Nzcy9jb25zdGFudHMuc2Nzc1wiO1xuLmNhcnQtd3JhcHBlcntcbiAgICBhe1xuICAgICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgICAgIC5jYXJ0LWljb257XG4gICAgICAgICAgICBpe1xuICAgICAgICAgICAgICAgIGZvbnQtc2l6ZTogMmVtO1xuICAgICAgICAgICAgICAgIGNvbG9yOiAkcHJpbWFyeS1jb2xvcjtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgICAuY2FydC1jb3VudHtcbiAgICAgICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICAgICAgICAgIHdpZHRoOiAyMnB4O1xuICAgICAgICAgICAgaGVpZ2h0OiAyMnB4O1xuICAgICAgICAgICAgZm9udC1zaXplOiAxMnB4O1xuICAgICAgICAgICAgbGluZS1oZWlnaHQ6IDIycHg7XG4gICAgICAgICAgICBsZWZ0OiAxNXB4O1xuICAgICAgICAgICAgdG9wOiAtMjJweDtcbiAgICAgICAgICAgIGJhY2tncm91bmQ6ICR3aGl0ZTtcbiAgICAgICAgICAgIGJvcmRlci1yYWRpdXM6IDUwJTtcbiAgICAgICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICAgICAgICAgIGNvbG9yOiAkYmxhY2s7XG4gICAgICAgIH1cbiAgICB9XG59Il19 */");

/***/ }),

/***/ "./src/app/shared/molecules/cart-molecule/cart-molecule.component.ts":
/*!***************************************************************************!*\
  !*** ./src/app/shared/molecules/cart-molecule/cart-molecule.component.ts ***!
  \***************************************************************************/
/*! exports provided: CartMoleculeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CartMoleculeComponent", function() { return CartMoleculeComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let CartMoleculeComponent = class CartMoleculeComponent {
    constructor() { }
    ngOnInit() {
    }
};
CartMoleculeComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-cart-molecule',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./cart-molecule.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/molecules/cart-molecule/cart-molecule.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./cart-molecule.component.scss */ "./src/app/shared/molecules/cart-molecule/cart-molecule.component.scss")).default]
    })
], CartMoleculeComponent);



/***/ }),

/***/ "./src/app/shared/molecules/client-logo-molecule/client-logo.component.scss":
/*!**********************************************************************************!*\
  !*** ./src/app/shared/molecules/client-logo-molecule/client-logo.component.scss ***!
  \**********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("/********************Custom Variables **************************/\n.text-white {\n  color: #fff;\n}\n.secondary-bg {\n  background: #000;\n}\n.primary-color {\n  color: #a7f108;\n}\n.no-margin {\n  margin: 0;\n}\n.no-padding {\n  padding: 0;\n}\n.margin-auto {\n  margin: 0 auto;\n}\n.font-bold {\n  font-weight: 800;\n}\n.font-semi-bold {\n  font-weight: 600;\n}\n.mb-30 {\n  margin-bottom: 30px;\n}\n.mb-50 {\n  margin-bottom: 50px;\n}\n.pr-20 {\n  padding-right: 20px;\n}\n.bg-white {\n  background: #fff;\n}\n.logo-section .title {\n  font-size: 34px;\n  font-family: \"Roboto-Medium\";\n}\n.logo-section .title::after {\n  content: \"\";\n  display: block;\n  margin: 0 auto;\n  width: 8%;\n  padding-top: 15px;\n  border-bottom: 4px solid #a7f108;\n}\n.logo-section .client-logos .img-grayscale {\n  filter: gray;\n  /* IE6-9 */\n  -webkit-filter: grayscale(1);\n  /* Google Chrome, Safari 6+ & Opera 15+ */\n  filter: grayscale(1);\n  /* Microsoft Edge and Firefox 35+ */\n  cursor: pointer;\n}\n.logo-section .client-logos .img-grayscale:hover {\n  opacity: 1;\n  -webkit-filter: grayscale(0);\n          filter: grayscale(0);\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi92YXIvd3d3L2h0bWwvanMtc3RvcmVmcm9udC9zcGFydGFjdXNzdG9yZS9zcmMvYXNzZXRzL2Nzcy92YXJpYWJsZS5zY3NzIiwic3JjL2FwcC9zaGFyZWQvbW9sZWN1bGVzL2NsaWVudC1sb2dvLW1vbGVjdWxlL2NsaWVudC1sb2dvLmNvbXBvbmVudC5zY3NzIiwiL3Zhci93d3cvaHRtbC9qcy1zdG9yZWZyb250L3NwYXJ0YWN1c3N0b3JlL3NyYy9hcHAvc2hhcmVkL21vbGVjdWxlcy9jbGllbnQtbG9nby1tb2xlY3VsZS9jbGllbnQtbG9nby5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxnRUFBQTtBQU1BO0VBQ0ksV0FBQTtBQ0pKO0FETUU7RUFDRSxnQkFBQTtBQ0hKO0FES0U7RUFDRSxjQUFBO0FDRko7QURJRTtFQUNFLFNBQUE7QUNESjtBREdFO0VBQ0UsVUFBQTtBQ0FKO0FERUU7RUFDRSxjQUFBO0FDQ0o7QURDRTtFQUNFLGdCQUFBO0FDRUo7QURBRTtFQUNFLGdCQUFBO0FDR0o7QURERTtFQUNFLG1CQUFBO0FDSUo7QURGRTtFQUNFLG1CQUFBO0FDS0o7QURIRTtFQUNFLG1CQUFBO0FDTUo7QURKRTtFQUNFLGdCQUFBO0FDT0o7QUM3Q0k7RUFDSSxlQUFBO0VBQ0EsNEJBQUE7QURnRFI7QUMvQ1E7RUFDSSxXQUFBO0VBQ0EsY0FBQTtFQUNBLGNBQUE7RUFDQSxTQUFBO0VBQ0EsaUJBQUE7RUFDQSxnQ0FBQTtBRGlEWjtBQzdDSTtFQUNJLFlBQUE7RUFDQSxVQUFBO0VBQ0EsNEJBQUE7RUFDQSx5Q0FBQTtFQUNBLG9CQUFBO0VBQ0EsbUNBQUE7RUFDQSxlQUFBO0FEK0NSO0FDOUNRO0VBQ1EsVUFBQTtFQUNBLDRCQUFBO1VBQUEsb0JBQUE7QURnRGhCIiwiZmlsZSI6InNyYy9hcHAvc2hhcmVkL21vbGVjdWxlcy9jbGllbnQtbG9nby1tb2xlY3VsZS9jbGllbnQtbG9nby5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi8qKioqKioqKioqKioqKioqKioqKkN1c3RvbSBWYXJpYWJsZXMgKioqKioqKioqKioqKioqKioqKioqKioqKiovXG4kd2hpdGU6ICNmZmZmZmY7XG4kYmxhY2s6ICMwMDAwMDA7XG4kcHJpbWFyeS1jb2xvcjogI2E3ZjEwODtcbiRib3JkZXItY29sb3I6ICByZ2JhKDI1NSwgMjU1LCAyNTUsIDAuNik7XG4kZm9ybS1iZzogI2Y3ZjdmNztcbi50ZXh0LXdoaXRlIHtcbiAgICBjb2xvcjogI2ZmZjtcbiAgfVxuICAuc2Vjb25kYXJ5LWJnIHtcbiAgICBiYWNrZ3JvdW5kOiAjMDAwO1xuICB9XG4gIC5wcmltYXJ5LWNvbG9yIHtcbiAgICBjb2xvcjogI2E3ZjEwODtcbiAgfVxuICAubm8tbWFyZ2luIHtcbiAgICBtYXJnaW46IDA7XG4gIH1cbiAgLm5vLXBhZGRpbmcge1xuICAgIHBhZGRpbmc6IDA7XG4gIH1cbiAgLm1hcmdpbi1hdXRvIHtcbiAgICBtYXJnaW46IDAgYXV0bztcbiAgfVxuICAuZm9udC1ib2xkIHtcbiAgICBmb250LXdlaWdodDogODAwO1xuICB9XG4gIC5mb250LXNlbWktYm9sZCB7XG4gICAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgfVxuICAubWItMzAge1xuICAgIG1hcmdpbi1ib3R0b206IDMwcHg7XG4gIH1cbiAgLm1iLTUwIHtcbiAgICBtYXJnaW4tYm90dG9tOiA1MHB4O1xuICB9XG4gIC5wci0yMCB7XG4gICAgcGFkZGluZy1yaWdodDogMjBweDtcbiAgfVxuICAuYmctd2hpdGUge1xuICAgIGJhY2tncm91bmQ6ICNmZmY7XG4gIH0iLCIvKioqKioqKioqKioqKioqKioqKipDdXN0b20gVmFyaWFibGVzICoqKioqKioqKioqKioqKioqKioqKioqKioqL1xuLnRleHQtd2hpdGUge1xuICBjb2xvcjogI2ZmZjtcbn1cblxuLnNlY29uZGFyeS1iZyB7XG4gIGJhY2tncm91bmQ6ICMwMDA7XG59XG5cbi5wcmltYXJ5LWNvbG9yIHtcbiAgY29sb3I6ICNhN2YxMDg7XG59XG5cbi5uby1tYXJnaW4ge1xuICBtYXJnaW46IDA7XG59XG5cbi5uby1wYWRkaW5nIHtcbiAgcGFkZGluZzogMDtcbn1cblxuLm1hcmdpbi1hdXRvIHtcbiAgbWFyZ2luOiAwIGF1dG87XG59XG5cbi5mb250LWJvbGQge1xuICBmb250LXdlaWdodDogODAwO1xufVxuXG4uZm9udC1zZW1pLWJvbGQge1xuICBmb250LXdlaWdodDogNjAwO1xufVxuXG4ubWItMzAge1xuICBtYXJnaW4tYm90dG9tOiAzMHB4O1xufVxuXG4ubWItNTAge1xuICBtYXJnaW4tYm90dG9tOiA1MHB4O1xufVxuXG4ucHItMjAge1xuICBwYWRkaW5nLXJpZ2h0OiAyMHB4O1xufVxuXG4uYmctd2hpdGUge1xuICBiYWNrZ3JvdW5kOiAjZmZmO1xufVxuXG4ubG9nby1zZWN0aW9uIC50aXRsZSB7XG4gIGZvbnQtc2l6ZTogMzRweDtcbiAgZm9udC1mYW1pbHk6IFwiUm9ib3RvLU1lZGl1bVwiO1xufVxuLmxvZ28tc2VjdGlvbiAudGl0bGU6OmFmdGVyIHtcbiAgY29udGVudDogXCJcIjtcbiAgZGlzcGxheTogYmxvY2s7XG4gIG1hcmdpbjogMCBhdXRvO1xuICB3aWR0aDogOCU7XG4gIHBhZGRpbmctdG9wOiAxNXB4O1xuICBib3JkZXItYm90dG9tOiA0cHggc29saWQgI2E3ZjEwODtcbn1cbi5sb2dvLXNlY3Rpb24gLmNsaWVudC1sb2dvcyAuaW1nLWdyYXlzY2FsZSB7XG4gIGZpbHRlcjogZ3JheTtcbiAgLyogSUU2LTkgKi9cbiAgLXdlYmtpdC1maWx0ZXI6IGdyYXlzY2FsZSgxKTtcbiAgLyogR29vZ2xlIENocm9tZSwgU2FmYXJpIDYrICYgT3BlcmEgMTUrICovXG4gIGZpbHRlcjogZ3JheXNjYWxlKDEpO1xuICAvKiBNaWNyb3NvZnQgRWRnZSBhbmQgRmlyZWZveCAzNSsgKi9cbiAgY3Vyc29yOiBwb2ludGVyO1xufVxuLmxvZ28tc2VjdGlvbiAuY2xpZW50LWxvZ29zIC5pbWctZ3JheXNjYWxlOmhvdmVyIHtcbiAgb3BhY2l0eTogMTtcbiAgZmlsdGVyOiBncmF5c2NhbGUoMCk7XG59IiwiQGltcG9ydCBcIi4uLy4uLy4uLy4uL2Fzc2V0cy9jc3MvY29uc3RhbnRzLnNjc3NcIjtcbi5sb2dvLXNlY3Rpb257XG4gICAgLnRpdGxlIHtcbiAgICAgICAgZm9udC1zaXplOiAzNHB4O1xuICAgICAgICBmb250LWZhbWlseTogJ1JvYm90by1NZWRpdW0nO1xuICAgICAgICAmOjphZnRlcntcbiAgICAgICAgICAgIGNvbnRlbnQ6IFwiXCI7XG4gICAgICAgICAgICBkaXNwbGF5OiBibG9jaztcbiAgICAgICAgICAgIG1hcmdpbjogMCBhdXRvO1xuICAgICAgICAgICAgd2lkdGg6IDglO1xuICAgICAgICAgICAgcGFkZGluZy10b3A6IDE1cHg7XG4gICAgICAgICAgICBib3JkZXItYm90dG9tOiA0cHggc29saWQgI2E3ZjEwODtcbiAgICAgICAgfVxuICAgICAgfVxuLmNsaWVudC1sb2dvcyB7XG4gICAgLmltZy1ncmF5c2NhbGUge1xuICAgICAgICBmaWx0ZXI6IGdyYXk7XG4gICAgICAgIC8qIElFNi05ICovXG4gICAgICAgIC13ZWJraXQtZmlsdGVyOiBncmF5c2NhbGUoMSk7XG4gICAgICAgIC8qIEdvb2dsZSBDaHJvbWUsIFNhZmFyaSA2KyAmIE9wZXJhIDE1KyAqL1xuICAgICAgICBmaWx0ZXI6IGdyYXlzY2FsZSgxKTtcbiAgICAgICAgLyogTWljcm9zb2Z0IEVkZ2UgYW5kIEZpcmVmb3ggMzUrICovXG4gICAgICAgIGN1cnNvcjogcG9pbnRlcjtcbiAgICAgICAgJjpob3ZlcntcbiAgICAgICAgICAgICAgICBvcGFjaXR5OiAxO1xuICAgICAgICAgICAgICAgIGZpbHRlcjogZ3JheXNjYWxlKDApO1xuICAgICAgICB9XG4gICAgICB9XG59XG59XG4iXX0= */");

/***/ }),

/***/ "./src/app/shared/molecules/client-logo-molecule/client-logo.component.ts":
/*!********************************************************************************!*\
  !*** ./src/app/shared/molecules/client-logo-molecule/client-logo.component.ts ***!
  \********************************************************************************/
/*! exports provided: ClientLogoComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ClientLogoComponent", function() { return ClientLogoComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../environments/environment */ "./src/environments/environment.ts");




let ClientLogoComponent = class ClientLogoComponent {
    constructor(route) {
        this.route = route;
        this.hostName = _environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].hostName;
    }
    ngOnInit() {
        this.route.data.subscribe((data) => {
            const response = data.homeData;
            if (response &&
                response.contentSlots &&
                response.contentSlots.contentSlot) {
                for (const content of response.contentSlots.contentSlot) {
                    if (content.slotId === 'ValuableClientSlot') {
                        this.clientLogos = content.components.component;
                    }
                }
            }
        });
    }
};
ClientLogoComponent.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"] }
];
ClientLogoComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-cx-client-logo',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./client-logo.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/molecules/client-logo-molecule/client-logo.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./client-logo.component.scss */ "./src/app/shared/molecules/client-logo-molecule/client-logo.component.scss")).default]
    })
], ClientLogoComponent);



/***/ }),

/***/ "./src/app/shared/molecules/contact-us-molecule/contact-us-molecule.component.scss":
/*!*****************************************************************************************!*\
  !*** ./src/app/shared/molecules/contact-us-molecule/contact-us-molecule.component.scss ***!
  \*****************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("/********************Custom Variables **************************/\n.text-white {\n  color: #fff;\n}\n.secondary-bg {\n  background: #000;\n}\n.primary-color {\n  color: #a7f108;\n}\n.no-margin {\n  margin: 0;\n}\n.no-padding {\n  padding: 0;\n}\n.margin-auto {\n  margin: 0 auto;\n}\n.font-bold {\n  font-weight: 800;\n}\n.font-semi-bold {\n  font-weight: 600;\n}\n.mb-30 {\n  margin-bottom: 30px;\n}\n.mb-50 {\n  margin-bottom: 50px;\n}\n.pr-20 {\n  padding-right: 20px;\n}\n.bg-white {\n  background: #fff;\n}\n.contact-us-form {\n  width: 100%;\n  padding: 5% 10%;\n  text-align: center;\n}\n.contact-us-form .title {\n  font-size: 34px;\n  font-family: \"Roboto-Medium\";\n  margin: 0 auto;\n}\n.contact-us-form .title::after {\n  content: \"\";\n  display: block;\n  margin: 0 auto;\n  width: 8%;\n  padding-top: 15px;\n  border-bottom: 4px solid #a7f108;\n}\n.contact-us-form .name-field,\n.contact-us-form .email-field {\n  width: 45%;\n}\n.contact-us-form .comment-field {\n  width: 100%;\n}\n.contact-us-form .error-message {\n  font-size: 12px;\n}\n.contact-us-form .submit-btn {\n  background-color: #a7f108;\n  border-radius: 24px;\n  width: 150px;\n  font-size: 17px;\n  padding: 8px;\n  font-weight: bold;\n  box-shadow: none;\n}\n::ng-deep .mat-form-field {\n  padding: 10px 0;\n}\n@media only screen and (max-width: 768px) {\n  .contact-us-form .name-field,\n.contact-us-form .email-field {\n    width: 100%;\n  }\n}\n.mat-card:not([class*=mat-elevation-z]) {\n  box-shadow: none;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi92YXIvd3d3L2h0bWwvanMtc3RvcmVmcm9udC9zcGFydGFjdXNzdG9yZS9zcmMvYXNzZXRzL2Nzcy92YXJpYWJsZS5zY3NzIiwic3JjL2FwcC9zaGFyZWQvbW9sZWN1bGVzL2NvbnRhY3QtdXMtbW9sZWN1bGUvY29udGFjdC11cy1tb2xlY3VsZS5jb21wb25lbnQuc2NzcyIsIi92YXIvd3d3L2h0bWwvanMtc3RvcmVmcm9udC9zcGFydGFjdXNzdG9yZS9zcmMvYXBwL3NoYXJlZC9tb2xlY3VsZXMvY29udGFjdC11cy1tb2xlY3VsZS9jb250YWN0LXVzLW1vbGVjdWxlLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLGdFQUFBO0FBTUE7RUFDSSxXQUFBO0FDSko7QURNRTtFQUNFLGdCQUFBO0FDSEo7QURLRTtFQUNFLGNBQUE7QUNGSjtBRElFO0VBQ0UsU0FBQTtBQ0RKO0FER0U7RUFDRSxVQUFBO0FDQUo7QURFRTtFQUNFLGNBQUE7QUNDSjtBRENFO0VBQ0UsZ0JBQUE7QUNFSjtBREFFO0VBQ0UsZ0JBQUE7QUNHSjtBRERFO0VBQ0UsbUJBQUE7QUNJSjtBREZFO0VBQ0UsbUJBQUE7QUNLSjtBREhFO0VBQ0UsbUJBQUE7QUNNSjtBREpFO0VBQ0UsZ0JBQUE7QUNPSjtBQzlDQTtFQUNFLFdBQUE7RUFDQSxlQUFBO0VBQ0Esa0JBQUE7QURpREY7QUNoREU7RUFDRSxlQUFBO0VBQ0EsNEJBQUE7RUFDQSxjQUFBO0FEa0RKO0FDakRJO0VBQ0UsV0FBQTtFQUNBLGNBQUE7RUFDQSxjQUFBO0VBQ0EsU0FBQTtFQUNBLGlCQUFBO0VBQ0EsZ0NBQUE7QURtRE47QUNoREU7O0VBRUUsVUFBQTtBRGtESjtBQ2hERTtFQUNFLFdBQUE7QURrREo7QUNoREU7RUFDRSxlQUFBO0FEa0RKO0FDaERFO0VBQ0UseUJGMUJZO0VFMkJaLG1CQUFBO0VBQ0EsWUFBQTtFQUNBLGVBQUE7RUFDQSxZQUFBO0VBQ0EsaUJBQUE7RUFDQSxnQkFBQTtBRGtESjtBQy9DQTtFQUNFLGVBQUE7QURrREY7QUM5Q0E7RUFFSTs7SUFFRSxXQUFBO0VEZ0RKO0FBQ0Y7QUM3Q0E7RUFDRSxnQkFBQTtBRCtDRiIsImZpbGUiOiJzcmMvYXBwL3NoYXJlZC9tb2xlY3VsZXMvY29udGFjdC11cy1tb2xlY3VsZS9jb250YWN0LXVzLW1vbGVjdWxlLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLyoqKioqKioqKioqKioqKioqKioqQ3VzdG9tIFZhcmlhYmxlcyAqKioqKioqKioqKioqKioqKioqKioqKioqKi9cbiR3aGl0ZTogI2ZmZmZmZjtcbiRibGFjazogIzAwMDAwMDtcbiRwcmltYXJ5LWNvbG9yOiAjYTdmMTA4O1xuJGJvcmRlci1jb2xvcjogIHJnYmEoMjU1LCAyNTUsIDI1NSwgMC42KTtcbiRmb3JtLWJnOiAjZjdmN2Y3O1xuLnRleHQtd2hpdGUge1xuICAgIGNvbG9yOiAjZmZmO1xuICB9XG4gIC5zZWNvbmRhcnktYmcge1xuICAgIGJhY2tncm91bmQ6ICMwMDA7XG4gIH1cbiAgLnByaW1hcnktY29sb3Ige1xuICAgIGNvbG9yOiAjYTdmMTA4O1xuICB9XG4gIC5uby1tYXJnaW4ge1xuICAgIG1hcmdpbjogMDtcbiAgfVxuICAubm8tcGFkZGluZyB7XG4gICAgcGFkZGluZzogMDtcbiAgfVxuICAubWFyZ2luLWF1dG8ge1xuICAgIG1hcmdpbjogMCBhdXRvO1xuICB9XG4gIC5mb250LWJvbGQge1xuICAgIGZvbnQtd2VpZ2h0OiA4MDA7XG4gIH1cbiAgLmZvbnQtc2VtaS1ib2xkIHtcbiAgICBmb250LXdlaWdodDogNjAwO1xuICB9XG4gIC5tYi0zMCB7XG4gICAgbWFyZ2luLWJvdHRvbTogMzBweDtcbiAgfVxuICAubWItNTAge1xuICAgIG1hcmdpbi1ib3R0b206IDUwcHg7XG4gIH1cbiAgLnByLTIwIHtcbiAgICBwYWRkaW5nLXJpZ2h0OiAyMHB4O1xuICB9XG4gIC5iZy13aGl0ZSB7XG4gICAgYmFja2dyb3VuZDogI2ZmZjtcbiAgfSIsIi8qKioqKioqKioqKioqKioqKioqKkN1c3RvbSBWYXJpYWJsZXMgKioqKioqKioqKioqKioqKioqKioqKioqKiovXG4udGV4dC13aGl0ZSB7XG4gIGNvbG9yOiAjZmZmO1xufVxuXG4uc2Vjb25kYXJ5LWJnIHtcbiAgYmFja2dyb3VuZDogIzAwMDtcbn1cblxuLnByaW1hcnktY29sb3Ige1xuICBjb2xvcjogI2E3ZjEwODtcbn1cblxuLm5vLW1hcmdpbiB7XG4gIG1hcmdpbjogMDtcbn1cblxuLm5vLXBhZGRpbmcge1xuICBwYWRkaW5nOiAwO1xufVxuXG4ubWFyZ2luLWF1dG8ge1xuICBtYXJnaW46IDAgYXV0bztcbn1cblxuLmZvbnQtYm9sZCB7XG4gIGZvbnQtd2VpZ2h0OiA4MDA7XG59XG5cbi5mb250LXNlbWktYm9sZCB7XG4gIGZvbnQtd2VpZ2h0OiA2MDA7XG59XG5cbi5tYi0zMCB7XG4gIG1hcmdpbi1ib3R0b206IDMwcHg7XG59XG5cbi5tYi01MCB7XG4gIG1hcmdpbi1ib3R0b206IDUwcHg7XG59XG5cbi5wci0yMCB7XG4gIHBhZGRpbmctcmlnaHQ6IDIwcHg7XG59XG5cbi5iZy13aGl0ZSB7XG4gIGJhY2tncm91bmQ6ICNmZmY7XG59XG5cbi5jb250YWN0LXVzLWZvcm0ge1xuICB3aWR0aDogMTAwJTtcbiAgcGFkZGluZzogNSUgMTAlO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG4uY29udGFjdC11cy1mb3JtIC50aXRsZSB7XG4gIGZvbnQtc2l6ZTogMzRweDtcbiAgZm9udC1mYW1pbHk6IFwiUm9ib3RvLU1lZGl1bVwiO1xuICBtYXJnaW46IDAgYXV0bztcbn1cbi5jb250YWN0LXVzLWZvcm0gLnRpdGxlOjphZnRlciB7XG4gIGNvbnRlbnQ6IFwiXCI7XG4gIGRpc3BsYXk6IGJsb2NrO1xuICBtYXJnaW46IDAgYXV0bztcbiAgd2lkdGg6IDglO1xuICBwYWRkaW5nLXRvcDogMTVweDtcbiAgYm9yZGVyLWJvdHRvbTogNHB4IHNvbGlkICNhN2YxMDg7XG59XG4uY29udGFjdC11cy1mb3JtIC5uYW1lLWZpZWxkLFxuLmNvbnRhY3QtdXMtZm9ybSAuZW1haWwtZmllbGQge1xuICB3aWR0aDogNDUlO1xufVxuLmNvbnRhY3QtdXMtZm9ybSAuY29tbWVudC1maWVsZCB7XG4gIHdpZHRoOiAxMDAlO1xufVxuLmNvbnRhY3QtdXMtZm9ybSAuZXJyb3ItbWVzc2FnZSB7XG4gIGZvbnQtc2l6ZTogMTJweDtcbn1cbi5jb250YWN0LXVzLWZvcm0gLnN1Ym1pdC1idG4ge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjYTdmMTA4O1xuICBib3JkZXItcmFkaXVzOiAyNHB4O1xuICB3aWR0aDogMTUwcHg7XG4gIGZvbnQtc2l6ZTogMTdweDtcbiAgcGFkZGluZzogOHB4O1xuICBmb250LXdlaWdodDogYm9sZDtcbiAgYm94LXNoYWRvdzogbm9uZTtcbn1cblxuOjpuZy1kZWVwIC5tYXQtZm9ybS1maWVsZCB7XG4gIHBhZGRpbmc6IDEwcHggMDtcbn1cblxuQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiA3NjhweCkge1xuICAuY29udGFjdC11cy1mb3JtIC5uYW1lLWZpZWxkLFxuLmNvbnRhY3QtdXMtZm9ybSAuZW1haWwtZmllbGQge1xuICAgIHdpZHRoOiAxMDAlO1xuICB9XG59XG4ubWF0LWNhcmQ6bm90KFtjbGFzcyo9bWF0LWVsZXZhdGlvbi16XSkge1xuICBib3gtc2hhZG93OiBub25lO1xufSIsIkBpbXBvcnQgXCIuLi8uLi8uLi8uLi9hc3NldHMvY3NzL2NvbnN0YW50cy5zY3NzXCI7XG4uY29udGFjdC11cy1mb3JtIHtcbiAgd2lkdGg6IDEwMCU7XG4gIHBhZGRpbmc6IDUlIDEwJTtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAudGl0bGUge1xuICAgIGZvbnQtc2l6ZTogMzRweDtcbiAgICBmb250LWZhbWlseTogJ1JvYm90by1NZWRpdW0nOyAgXG4gICAgbWFyZ2luOiAwIGF1dG87XG4gICAgJjo6YWZ0ZXJ7XG4gICAgICBjb250ZW50OiBcIlwiO1xuICAgICAgZGlzcGxheTogYmxvY2s7XG4gICAgICBtYXJnaW46IDAgYXV0bztcbiAgICAgIHdpZHRoOiA4JTtcbiAgICAgIHBhZGRpbmctdG9wOiAxNXB4O1xuICAgICAgYm9yZGVyLWJvdHRvbTogNHB4IHNvbGlkICRwcmltYXJ5LWNvbG9yO1xuICAgIH1cbiAgfVxuICAubmFtZS1maWVsZCxcbiAgLmVtYWlsLWZpZWxkIHtcbiAgICB3aWR0aDogNDUlO1xuICB9XG4gIC5jb21tZW50LWZpZWxkIHtcbiAgICB3aWR0aDogMTAwJTtcbiAgfVxuICAuZXJyb3ItbWVzc2FnZSB7XG4gICAgZm9udC1zaXplOiAxMnB4O1xuICB9XG4gIC5zdWJtaXQtYnRuIHtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAkcHJpbWFyeS1jb2xvcjtcbiAgICBib3JkZXItcmFkaXVzOiAyNHB4O1xuICAgIHdpZHRoOiAxNTBweDtcbiAgICBmb250LXNpemU6IDE3cHg7XG4gICAgcGFkZGluZzogOHB4O1xuICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xuICAgIGJveC1zaGFkb3c6IG5vbmU7XG4gIH1cbn1cbjo6bmctZGVlcCAubWF0LWZvcm0tZmllbGR7XG4gIHBhZGRpbmc6IDEwcHggMDtcbn1cblxuXG5AbWVkaWEgb25seSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDc2OHB4KSB7XG4gIC5jb250YWN0LXVzLWZvcm0ge1xuICAgIC5uYW1lLWZpZWxkLFxuICAgIC5lbWFpbC1maWVsZCB7XG4gICAgICB3aWR0aDogMTAwJTtcbiAgICB9XG4gIH1cbn1cbi5tYXQtY2FyZDpub3QoW2NsYXNzKj1tYXQtZWxldmF0aW9uLXpdKSB7XG4gIGJveC1zaGFkb3c6IG5vbmU7XG59Il19 */");

/***/ }),

/***/ "./src/app/shared/molecules/contact-us-molecule/contact-us-molecule.component.ts":
/*!***************************************************************************************!*\
  !*** ./src/app/shared/molecules/contact-us-molecule/contact-us-molecule.component.ts ***!
  \***************************************************************************************/
/*! exports provided: ContactUsMoleculeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ContactUsMoleculeComponent", function() { return ContactUsMoleculeComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _services_home_page_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../services/home-page.service */ "./src/app/services/home-page.service.ts");




let ContactUsMoleculeComponent = class ContactUsMoleculeComponent {
    constructor(fb, homePageService) {
        this.fb = fb;
        this.homePageService = homePageService;
        this.validationMessages = {
            name: [{ type: 'required', message: 'Full name is required' }],
            email: [
                { type: 'required', message: 'Email is required' },
                { type: 'email', message: 'Enter a valid email' }
            ],
            message: [{ type: 'required', message: 'Please enter some message' }],
        };
    }
    ngOnInit() {
        // user details form validations
        this.userDetailsForm = this.fb.group({
            name: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
            email: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([
                    _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required,
                    _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].email
                ])],
            message: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]
        });
    }
    onSubmitUserDetails(userMsg) {
        console.log('userValue', userMsg);
        if (userMsg) {
            this.homePageService.postUserMessage(userMsg).subscribe(() => { });
        }
    }
};
ContactUsMoleculeComponent.ctorParameters = () => [
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"] },
    { type: _services_home_page_service__WEBPACK_IMPORTED_MODULE_3__["HomePageService"] }
];
ContactUsMoleculeComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-contact-us-molecule',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./contact-us-molecule.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/molecules/contact-us-molecule/contact-us-molecule.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./contact-us-molecule.component.scss */ "./src/app/shared/molecules/contact-us-molecule/contact-us-molecule.component.scss")).default]
    })
], ContactUsMoleculeComponent);



/***/ }),

/***/ "./src/app/shared/molecules/footer-molecule/footer.component.scss":
/*!************************************************************************!*\
  !*** ./src/app/shared/molecules/footer-molecule/footer.component.scss ***!
  \************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("/********************Custom Variables **************************/\n.text-white {\n  color: #fff;\n}\n.secondary-bg {\n  background: #000;\n}\n.primary-color {\n  color: #a7f108;\n}\n.no-margin {\n  margin: 0;\n}\n.no-padding {\n  padding: 0;\n}\n.margin-auto {\n  margin: 0 auto;\n}\n.font-bold {\n  font-weight: 800;\n}\n.font-semi-bold {\n  font-weight: 600;\n}\n.mb-30 {\n  margin-bottom: 30px;\n}\n.mb-50 {\n  margin-bottom: 50px;\n}\n.pr-20 {\n  padding-right: 20px;\n}\n.bg-white {\n  background: #fff;\n}\n.copyrights-wrapper .min-footer {\n  border-top: 1px solid rgba(255, 255, 255, 0.6);\n}\n.copyrights-wrapper .copyright-text {\n  color: rgba(255, 255, 255, 0.6);\n  font-size: 0.9em;\n  padding: 30px 0;\n}\n/********************Footer **************************/\n.footer-column .footer-sidebar {\n  padding: 50px 10px;\n}\n.footer-column h5 {\n  font-size: 1.1em;\n  font-weight: bold;\n}\n.footer-column ul li a {\n  color: #ffffff;\n  display: block;\n  padding: 5px 0;\n  text-decoration: none;\n}\n.footer-column .footer-column-5 ul li a {\n  display: block;\n  padding: 10px 0;\n}\n.footer-widget-area {\n  display: flex;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi92YXIvd3d3L2h0bWwvanMtc3RvcmVmcm9udC9zcGFydGFjdXNzdG9yZS9zcmMvYXNzZXRzL2Nzcy92YXJpYWJsZS5zY3NzIiwic3JjL2FwcC9zaGFyZWQvbW9sZWN1bGVzL2Zvb3Rlci1tb2xlY3VsZS9mb290ZXIuY29tcG9uZW50LnNjc3MiLCIvdmFyL3d3dy9odG1sL2pzLXN0b3JlZnJvbnQvc3BhcnRhY3Vzc3RvcmUvc3JjL2FwcC9zaGFyZWQvbW9sZWN1bGVzL2Zvb3Rlci1tb2xlY3VsZS9mb290ZXIuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsZ0VBQUE7QUFNQTtFQUNJLFdBQUE7QUNKSjtBRE1FO0VBQ0UsZ0JBQUE7QUNISjtBREtFO0VBQ0UsY0FBQTtBQ0ZKO0FESUU7RUFDRSxTQUFBO0FDREo7QURHRTtFQUNFLFVBQUE7QUNBSjtBREVFO0VBQ0UsY0FBQTtBQ0NKO0FEQ0U7RUFDRSxnQkFBQTtBQ0VKO0FEQUU7RUFDRSxnQkFBQTtBQ0dKO0FEREU7RUFDRSxtQkFBQTtBQ0lKO0FERkU7RUFDRSxtQkFBQTtBQ0tKO0FESEU7RUFDRSxtQkFBQTtBQ01KO0FESkU7RUFDRSxnQkFBQTtBQ09KO0FDNUNJO0VBQ0UsOENBQUE7QUQrQ047QUM3Q0k7RUFDRSwrQkFBQTtFQUNBLGdCQUFBO0VBQ0EsZUFBQTtBRCtDTjtBQzVDRSxzREFBQTtBQUVFO0VBQ0Usa0JBQUE7QUQ4Q047QUM1Q0k7RUFDRSxnQkFBQTtFQUNBLGlCQUFBO0FEOENOO0FDMUNRO0VBQ0UsY0Z2QkY7RUV3QkUsY0FBQTtFQUNBLGNBQUE7RUFDQSxxQkFBQTtBRDRDVjtBQ3JDVTtFQUNFLGNBQUE7RUFDQSxlQUFBO0FEdUNaO0FDakNFO0VBQ0ksYUFBQTtBRG9DTiIsImZpbGUiOiJzcmMvYXBwL3NoYXJlZC9tb2xlY3VsZXMvZm9vdGVyLW1vbGVjdWxlL2Zvb3Rlci5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi8qKioqKioqKioqKioqKioqKioqKkN1c3RvbSBWYXJpYWJsZXMgKioqKioqKioqKioqKioqKioqKioqKioqKiovXG4kd2hpdGU6ICNmZmZmZmY7XG4kYmxhY2s6ICMwMDAwMDA7XG4kcHJpbWFyeS1jb2xvcjogI2E3ZjEwODtcbiRib3JkZXItY29sb3I6ICByZ2JhKDI1NSwgMjU1LCAyNTUsIDAuNik7XG4kZm9ybS1iZzogI2Y3ZjdmNztcbi50ZXh0LXdoaXRlIHtcbiAgICBjb2xvcjogI2ZmZjtcbiAgfVxuICAuc2Vjb25kYXJ5LWJnIHtcbiAgICBiYWNrZ3JvdW5kOiAjMDAwO1xuICB9XG4gIC5wcmltYXJ5LWNvbG9yIHtcbiAgICBjb2xvcjogI2E3ZjEwODtcbiAgfVxuICAubm8tbWFyZ2luIHtcbiAgICBtYXJnaW46IDA7XG4gIH1cbiAgLm5vLXBhZGRpbmcge1xuICAgIHBhZGRpbmc6IDA7XG4gIH1cbiAgLm1hcmdpbi1hdXRvIHtcbiAgICBtYXJnaW46IDAgYXV0bztcbiAgfVxuICAuZm9udC1ib2xkIHtcbiAgICBmb250LXdlaWdodDogODAwO1xuICB9XG4gIC5mb250LXNlbWktYm9sZCB7XG4gICAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgfVxuICAubWItMzAge1xuICAgIG1hcmdpbi1ib3R0b206IDMwcHg7XG4gIH1cbiAgLm1iLTUwIHtcbiAgICBtYXJnaW4tYm90dG9tOiA1MHB4O1xuICB9XG4gIC5wci0yMCB7XG4gICAgcGFkZGluZy1yaWdodDogMjBweDtcbiAgfVxuICAuYmctd2hpdGUge1xuICAgIGJhY2tncm91bmQ6ICNmZmY7XG4gIH0iLCIvKioqKioqKioqKioqKioqKioqKipDdXN0b20gVmFyaWFibGVzICoqKioqKioqKioqKioqKioqKioqKioqKioqL1xuLnRleHQtd2hpdGUge1xuICBjb2xvcjogI2ZmZjtcbn1cblxuLnNlY29uZGFyeS1iZyB7XG4gIGJhY2tncm91bmQ6ICMwMDA7XG59XG5cbi5wcmltYXJ5LWNvbG9yIHtcbiAgY29sb3I6ICNhN2YxMDg7XG59XG5cbi5uby1tYXJnaW4ge1xuICBtYXJnaW46IDA7XG59XG5cbi5uby1wYWRkaW5nIHtcbiAgcGFkZGluZzogMDtcbn1cblxuLm1hcmdpbi1hdXRvIHtcbiAgbWFyZ2luOiAwIGF1dG87XG59XG5cbi5mb250LWJvbGQge1xuICBmb250LXdlaWdodDogODAwO1xufVxuXG4uZm9udC1zZW1pLWJvbGQge1xuICBmb250LXdlaWdodDogNjAwO1xufVxuXG4ubWItMzAge1xuICBtYXJnaW4tYm90dG9tOiAzMHB4O1xufVxuXG4ubWItNTAge1xuICBtYXJnaW4tYm90dG9tOiA1MHB4O1xufVxuXG4ucHItMjAge1xuICBwYWRkaW5nLXJpZ2h0OiAyMHB4O1xufVxuXG4uYmctd2hpdGUge1xuICBiYWNrZ3JvdW5kOiAjZmZmO1xufVxuXG4uY29weXJpZ2h0cy13cmFwcGVyIC5taW4tZm9vdGVyIHtcbiAgYm9yZGVyLXRvcDogMXB4IHNvbGlkIHJnYmEoMjU1LCAyNTUsIDI1NSwgMC42KTtcbn1cbi5jb3B5cmlnaHRzLXdyYXBwZXIgLmNvcHlyaWdodC10ZXh0IHtcbiAgY29sb3I6IHJnYmEoMjU1LCAyNTUsIDI1NSwgMC42KTtcbiAgZm9udC1zaXplOiAwLjllbTtcbiAgcGFkZGluZzogMzBweCAwO1xufVxuXG4vKioqKioqKioqKioqKioqKioqKipGb290ZXIgKioqKioqKioqKioqKioqKioqKioqKioqKiovXG4uZm9vdGVyLWNvbHVtbiAuZm9vdGVyLXNpZGViYXIge1xuICBwYWRkaW5nOiA1MHB4IDEwcHg7XG59XG4uZm9vdGVyLWNvbHVtbiBoNSB7XG4gIGZvbnQtc2l6ZTogMS4xZW07XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xufVxuLmZvb3Rlci1jb2x1bW4gdWwgbGkgYSB7XG4gIGNvbG9yOiAjZmZmZmZmO1xuICBkaXNwbGF5OiBibG9jaztcbiAgcGFkZGluZzogNXB4IDA7XG4gIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcbn1cbi5mb290ZXItY29sdW1uIC5mb290ZXItY29sdW1uLTUgdWwgbGkgYSB7XG4gIGRpc3BsYXk6IGJsb2NrO1xuICBwYWRkaW5nOiAxMHB4IDA7XG59XG5cbi5mb290ZXItd2lkZ2V0LWFyZWEge1xuICBkaXNwbGF5OiBmbGV4O1xufSIsIiAgQGltcG9ydCBcIi4uLy4uLy4uLy4uL2Fzc2V0cy9jc3MvY29uc3RhbnRzLnNjc3NcIjtcbiAgXG4gIC5jb3B5cmlnaHRzLXdyYXBwZXIge1xuICAgIC5taW4tZm9vdGVyIHtcbiAgICAgIGJvcmRlci10b3A6IDFweCBzb2xpZCByZ2JhKDI1NSwgMjU1LCAyNTUsIDAuNik7XG4gICAgfVxuICAgIC5jb3B5cmlnaHQtdGV4dCB7XG4gICAgICBjb2xvcjogcmdiYSgyNTUsIDI1NSwgMjU1LCAwLjYpO1xuICAgICAgZm9udC1zaXplOiAwLjllbTtcbiAgICAgIHBhZGRpbmc6IDMwcHggMDtcbiAgICB9XG4gIH1cbiAgLyoqKioqKioqKioqKioqKioqKioqRm9vdGVyICoqKioqKioqKioqKioqKioqKioqKioqKioqL1xuICAuZm9vdGVyLWNvbHVtbiB7XG4gICAgLmZvb3Rlci1zaWRlYmFyIHtcbiAgICAgIHBhZGRpbmc6IDUwcHggMTBweDtcbiAgICB9XG4gICAgaDUge1xuICAgICAgZm9udC1zaXplOiAxLjFlbTtcbiAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xuICAgIH1cbiAgICB1bCB7XG4gICAgICBsaSB7XG4gICAgICAgIGEge1xuICAgICAgICAgIGNvbG9yOiAkd2hpdGU7XG4gICAgICAgICAgZGlzcGxheTogYmxvY2s7XG4gICAgICAgICAgcGFkZGluZzogNXB4IDA7XG4gICAgICAgICAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xuICAgICAgICB9XG4gICAgICB9XG4gICAgfVxuICAgIC5mb290ZXItY29sdW1uLTUge1xuICAgICAgdWwge1xuICAgICAgICBsaSB7XG4gICAgICAgICAgYSB7XG4gICAgICAgICAgICBkaXNwbGF5OiBibG9jaztcbiAgICAgICAgICAgIHBhZGRpbmc6IDEwcHggMDtcbiAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9XG4gIH1cbiAgLmZvb3Rlci13aWRnZXQtYXJlYSB7XG4gICAgICBkaXNwbGF5OiBmbGV4O1xuICB9XG4gICJdfQ== */");

/***/ }),

/***/ "./src/app/shared/molecules/footer-molecule/footer.component.ts":
/*!**********************************************************************!*\
  !*** ./src/app/shared/molecules/footer-molecule/footer.component.ts ***!
  \**********************************************************************/
/*! exports provided: FooterComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FooterComponent", function() { return FooterComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");



let FooterComponent = class FooterComponent {
    constructor(route) {
        this.route = route;
    }
    ngOnInit() {
    }
};
FooterComponent.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"] }
];
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])()
], FooterComponent.prototype, "footerColumnSlot", void 0);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])()
], FooterComponent.prototype, "copyRightSlot", void 0);
FooterComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-cx-footer',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./footer.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/molecules/footer-molecule/footer.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./footer.component.scss */ "./src/app/shared/molecules/footer-molecule/footer.component.scss")).default]
    })
], FooterComponent);



/***/ }),

/***/ "./src/app/shared/molecules/header-links-molecule/header-links-molecule.component.scss":
/*!*********************************************************************************************!*\
  !*** ./src/app/shared/molecules/header-links-molecule/header-links-molecule.component.scss ***!
  \*********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("/********************Custom Variables **************************/\n.text-white {\n  color: #fff;\n}\n.secondary-bg {\n  background: #000;\n}\n.primary-color {\n  color: #a7f108;\n}\n.no-margin {\n  margin: 0;\n}\n.no-padding {\n  padding: 0;\n}\n.margin-auto {\n  margin: 0 auto;\n}\n.font-bold {\n  font-weight: 800;\n}\n.font-semi-bold {\n  font-weight: 600;\n}\n.mb-30 {\n  margin-bottom: 30px;\n}\n.mb-50 {\n  margin-bottom: 50px;\n}\n.pr-20 {\n  padding-right: 20px;\n}\n.bg-white {\n  background: #fff;\n}\n.top_header {\n  padding: 20px;\n  display: flex;\n  justify-content: space-between;\n  vertical-align: middle;\n  align-items: center;\n  font-family: \"Roboto-Regular\";\n  font-size: 0.9em;\n}\n@media only screen and (min-width: 768px) {\n  .top_header .top_section_left {\n    display: flex !important;\n  }\n}\n.top_header .top_section_right {\n  justify-content: flex-end;\n  display: flex;\n}\n.top_header .top_section_right li a {\n  font-size: 0.9em;\n  color: #a7f108;\n  cursor: pointer;\n}\n@media only screen and (max-width: 480px) {\n  .top_header {\n    flex-direction: column;\n    padding: 0 !important;\n  }\n\n  .top_section_left, .top_section_right {\n    display: flex;\n    justify-content: space-between;\n  }\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi92YXIvd3d3L2h0bWwvanMtc3RvcmVmcm9udC9zcGFydGFjdXNzdG9yZS9zcmMvYXNzZXRzL2Nzcy92YXJpYWJsZS5zY3NzIiwic3JjL2FwcC9zaGFyZWQvbW9sZWN1bGVzL2hlYWRlci1saW5rcy1tb2xlY3VsZS9oZWFkZXItbGlua3MtbW9sZWN1bGUuY29tcG9uZW50LnNjc3MiLCIvdmFyL3d3dy9odG1sL2pzLXN0b3JlZnJvbnQvc3BhcnRhY3Vzc3RvcmUvc3JjL2FwcC9zaGFyZWQvbW9sZWN1bGVzL2hlYWRlci1saW5rcy1tb2xlY3VsZS9oZWFkZXItbGlua3MtbW9sZWN1bGUuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsZ0VBQUE7QUFNQTtFQUNJLFdBQUE7QUNKSjtBRE1FO0VBQ0UsZ0JBQUE7QUNISjtBREtFO0VBQ0UsY0FBQTtBQ0ZKO0FESUU7RUFDRSxTQUFBO0FDREo7QURHRTtFQUNFLFVBQUE7QUNBSjtBREVFO0VBQ0UsY0FBQTtBQ0NKO0FEQ0U7RUFDRSxnQkFBQTtBQ0VKO0FEQUU7RUFDRSxnQkFBQTtBQ0dKO0FEREU7RUFDRSxtQkFBQTtBQ0lKO0FERkU7RUFDRSxtQkFBQTtBQ0tKO0FESEU7RUFDRSxtQkFBQTtBQ01KO0FESkU7RUFDRSxnQkFBQTtBQ09KO0FDOUNBO0VBQ0UsYUFBQTtFQUNBLGFBQUE7RUFDQSw4QkFBQTtFQUNBLHNCQUFBO0VBQ0EsbUJBQUE7RUFDQSw2QkFBQTtFQUNBLGdCQUFBO0FEaURGO0FDL0NJO0VBREY7SUFFRSx3QkFBQTtFRGtERjtBQUNGO0FDaERFO0VBQ0UseUJBQUE7RUFDQSxhQUFBO0FEa0RKO0FDaERNO0VBQ0UsZ0JBQUE7RUFDQSxjRmpCUTtFRWtCUixlQUFBO0FEa0RSO0FDNUNBO0VBQ0U7SUFDRSxzQkFBQTtJQUNBLHFCQUFBO0VEK0NGOztFQzdDQTtJQUNFLGFBQUE7SUFDQSw4QkFBQTtFRGdERjtBQUNGIiwiZmlsZSI6InNyYy9hcHAvc2hhcmVkL21vbGVjdWxlcy9oZWFkZXItbGlua3MtbW9sZWN1bGUvaGVhZGVyLWxpbmtzLW1vbGVjdWxlLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLyoqKioqKioqKioqKioqKioqKioqQ3VzdG9tIFZhcmlhYmxlcyAqKioqKioqKioqKioqKioqKioqKioqKioqKi9cbiR3aGl0ZTogI2ZmZmZmZjtcbiRibGFjazogIzAwMDAwMDtcbiRwcmltYXJ5LWNvbG9yOiAjYTdmMTA4O1xuJGJvcmRlci1jb2xvcjogIHJnYmEoMjU1LCAyNTUsIDI1NSwgMC42KTtcbiRmb3JtLWJnOiAjZjdmN2Y3O1xuLnRleHQtd2hpdGUge1xuICAgIGNvbG9yOiAjZmZmO1xuICB9XG4gIC5zZWNvbmRhcnktYmcge1xuICAgIGJhY2tncm91bmQ6ICMwMDA7XG4gIH1cbiAgLnByaW1hcnktY29sb3Ige1xuICAgIGNvbG9yOiAjYTdmMTA4O1xuICB9XG4gIC5uby1tYXJnaW4ge1xuICAgIG1hcmdpbjogMDtcbiAgfVxuICAubm8tcGFkZGluZyB7XG4gICAgcGFkZGluZzogMDtcbiAgfVxuICAubWFyZ2luLWF1dG8ge1xuICAgIG1hcmdpbjogMCBhdXRvO1xuICB9XG4gIC5mb250LWJvbGQge1xuICAgIGZvbnQtd2VpZ2h0OiA4MDA7XG4gIH1cbiAgLmZvbnQtc2VtaS1ib2xkIHtcbiAgICBmb250LXdlaWdodDogNjAwO1xuICB9XG4gIC5tYi0zMCB7XG4gICAgbWFyZ2luLWJvdHRvbTogMzBweDtcbiAgfVxuICAubWItNTAge1xuICAgIG1hcmdpbi1ib3R0b206IDUwcHg7XG4gIH1cbiAgLnByLTIwIHtcbiAgICBwYWRkaW5nLXJpZ2h0OiAyMHB4O1xuICB9XG4gIC5iZy13aGl0ZSB7XG4gICAgYmFja2dyb3VuZDogI2ZmZjtcbiAgfSIsIi8qKioqKioqKioqKioqKioqKioqKkN1c3RvbSBWYXJpYWJsZXMgKioqKioqKioqKioqKioqKioqKioqKioqKiovXG4udGV4dC13aGl0ZSB7XG4gIGNvbG9yOiAjZmZmO1xufVxuXG4uc2Vjb25kYXJ5LWJnIHtcbiAgYmFja2dyb3VuZDogIzAwMDtcbn1cblxuLnByaW1hcnktY29sb3Ige1xuICBjb2xvcjogI2E3ZjEwODtcbn1cblxuLm5vLW1hcmdpbiB7XG4gIG1hcmdpbjogMDtcbn1cblxuLm5vLXBhZGRpbmcge1xuICBwYWRkaW5nOiAwO1xufVxuXG4ubWFyZ2luLWF1dG8ge1xuICBtYXJnaW46IDAgYXV0bztcbn1cblxuLmZvbnQtYm9sZCB7XG4gIGZvbnQtd2VpZ2h0OiA4MDA7XG59XG5cbi5mb250LXNlbWktYm9sZCB7XG4gIGZvbnQtd2VpZ2h0OiA2MDA7XG59XG5cbi5tYi0zMCB7XG4gIG1hcmdpbi1ib3R0b206IDMwcHg7XG59XG5cbi5tYi01MCB7XG4gIG1hcmdpbi1ib3R0b206IDUwcHg7XG59XG5cbi5wci0yMCB7XG4gIHBhZGRpbmctcmlnaHQ6IDIwcHg7XG59XG5cbi5iZy13aGl0ZSB7XG4gIGJhY2tncm91bmQ6ICNmZmY7XG59XG5cbi50b3BfaGVhZGVyIHtcbiAgcGFkZGluZzogMjBweDtcbiAgZGlzcGxheTogZmxleDtcbiAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xuICB2ZXJ0aWNhbC1hbGlnbjogbWlkZGxlO1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICBmb250LWZhbWlseTogXCJSb2JvdG8tUmVndWxhclwiO1xuICBmb250LXNpemU6IDAuOWVtO1xufVxuQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWluLXdpZHRoOiA3NjhweCkge1xuICAudG9wX2hlYWRlciAudG9wX3NlY3Rpb25fbGVmdCB7XG4gICAgZGlzcGxheTogZmxleCAhaW1wb3J0YW50O1xuICB9XG59XG4udG9wX2hlYWRlciAudG9wX3NlY3Rpb25fcmlnaHQge1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtZW5kO1xuICBkaXNwbGF5OiBmbGV4O1xufVxuLnRvcF9oZWFkZXIgLnRvcF9zZWN0aW9uX3JpZ2h0IGxpIGEge1xuICBmb250LXNpemU6IDAuOWVtO1xuICBjb2xvcjogI2E3ZjEwODtcbiAgY3Vyc29yOiBwb2ludGVyO1xufVxuXG5AbWVkaWEgb25seSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDQ4MHB4KSB7XG4gIC50b3BfaGVhZGVyIHtcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICAgIHBhZGRpbmc6IDAgIWltcG9ydGFudDtcbiAgfVxuXG4gIC50b3Bfc2VjdGlvbl9sZWZ0LCAudG9wX3NlY3Rpb25fcmlnaHQge1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xuICB9XG59IiwiQGltcG9ydCBcIi4uLy4uLy4uLy4uL2Fzc2V0cy9jc3MvY29uc3RhbnRzLnNjc3NcIjtcbi50b3BfaGVhZGVyIHtcbiAgcGFkZGluZzogMjBweDtcbiAgZGlzcGxheTogZmxleDtcbiAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xuICB2ZXJ0aWNhbC1hbGlnbjogbWlkZGxlO1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICBmb250LWZhbWlseTogJ1JvYm90by1SZWd1bGFyJztcbiAgZm9udC1zaXplOiAwLjllbTtcbiAgLnRvcF9zZWN0aW9uX2xlZnR7XG4gICAgQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWluLXdpZHRoOiA3NjhweCkge1xuICAgIGRpc3BsYXk6IGZsZXggIWltcG9ydGFudDtcbiAgfVxufVxuICAudG9wX3NlY3Rpb25fcmlnaHQge1xuICAgIGp1c3RpZnktY29udGVudDogZmxleC1lbmQ7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBsaXtcbiAgICAgIGF7XG4gICAgICAgIGZvbnQtc2l6ZTogMC45ZW07XG4gICAgICAgIGNvbG9yOiAkcHJpbWFyeS1jb2xvcjtcbiAgICAgICAgY3Vyc29yOiBwb2ludGVyO1xuICAgICAgfVxuICAgIH1cbiAgfVxufVxuXG5AbWVkaWEgb25seSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDQ4MHB4KSB7XG4gIC50b3BfaGVhZGVyIHtcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICAgIHBhZGRpbmc6IDAgIWltcG9ydGFudDtcbiAgfVxuICAudG9wX3NlY3Rpb25fbGVmdCwgLnRvcF9zZWN0aW9uX3JpZ2h0IHtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcbiAgfVxufVxuXG5cbiAgIl19 */");

/***/ }),

/***/ "./src/app/shared/molecules/header-links-molecule/header-links-molecule.component.ts":
/*!*******************************************************************************************!*\
  !*** ./src/app/shared/molecules/header-links-molecule/header-links-molecule.component.ts ***!
  \*******************************************************************************************/
/*! exports provided: HeaderLinksMoleculeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HeaderLinksMoleculeComponent", function() { return HeaderLinksMoleculeComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _services_login_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../services/login.service */ "./src/app/services/login.service.ts");
/* harmony import */ var _services_common_utility_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../services/common-utility-service */ "./src/app/services/common-utility-service.ts");




let HeaderLinksMoleculeComponent = class HeaderLinksMoleculeComponent {
    constructor(loginService, utilityService) {
        this.loginService = loginService;
        this.utilityService = utilityService;
        this.displayName = '';
        this.isLoggedIn = false;
        this.loginService.userContext.subscribe(context => {
            this.userContext = context;
        });
    }
    ngOnInit() {
        this.loginService.userContext.subscribe(context => {
            this.displayName = context.displayName;
            this.isLoggedIn = context.displayName ? true : false;
        });
    }
    onLogout(e) {
        e.preventDefault();
        this.userContext.displayUID = '';
        this.userContext.displayName = '';
        this.userContext.isAuthenticated = false;
        this.utilityService.removeCookie('isAuthenticated');
        this.utilityService.removeCookie('displayUid');
        this.utilityService.removeCookie('userName');
        this.loginService.changeUserContext(this.userContext);
    }
};
HeaderLinksMoleculeComponent.ctorParameters = () => [
    { type: _services_login_service__WEBPACK_IMPORTED_MODULE_2__["LoginService"] },
    { type: _services_common_utility_service__WEBPACK_IMPORTED_MODULE_3__["CommonUtilityService"] }
];
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])()
], HeaderLinksMoleculeComponent.prototype, "headerLinks", void 0);
HeaderLinksMoleculeComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-cx-header-links-molecule',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./header-links-molecule.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/molecules/header-links-molecule/header-links-molecule.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./header-links-molecule.component.scss */ "./src/app/shared/molecules/header-links-molecule/header-links-molecule.component.scss")).default]
    })
], HeaderLinksMoleculeComponent);



/***/ }),

/***/ "./src/app/shared/molecules/header-nav-molecule/header-nav-molecule.component.css":
/*!****************************************************************************************!*\
  !*** ./src/app/shared/molecules/header-nav-molecule/header-nav-molecule.component.css ***!
  \****************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".nav-container {\n    display: flex;\n    flex-direction: row;\n    font-family: 'Roboto-Regular';\n    text-transform: uppercase;\n    font-size: 0.85em;\n}\n.mobile-sidenav, .hamburger {\n    display: none;\n}\n.nav-container nav {\n    width: 70%;\n    display: flex;\n    align-items: center;\n}\n.nav-container nav ul {\n    list-style: none;\n    margin: 0;\n    padding: 0;\n    display: flex;\n}\n.nav-container nav ul li {\n    padding: 0 10px;\n}\n.nav-container nav ul li a {\n     color: white;\n     text-decoration: none;\n     border-bottom: 3px solid transparent;\n     padding-bottom: 5px;\n}\n.nav-container nav ul li a:hover {\n    border-bottom: 3px solid #A7F108;\n}\n.cart-container{\n    width: 10%;\n    display: flex;\n    justify-content: center;\n    align-items: center;\n}\n@media only screen and (max-width: 768px) {\n    .nav-container {\n        display: none;\n    }\n    .hamburger {\n        display: block;\n        width: 20%;\n    }\n    .mobile-nav-container {\n        display: flex;\n    }\n    .mobile-nav-container ul {\n        padding: 0;\n        list-style: none;\n    }\n    .mobile-logo-container {\n        display: flex;\n        justify-content: left;\n        width: 80%\n    }\n    .mobile-logo-container img {\n        width: 200px;\n    }\n    .mobile-sidenav {\n        display: block;\n        height: 100%;\n        width: 0;\n        position: fixed;\n        z-index: 9999;\n        top: 0;\n        left: 0;\n        background-color: #111;\n        overflow-x: hidden;\n        transition: 0.5s;\n        padding-top: 60px;\n      }\n      \n      .mobile-sidenav a {\n        padding: 8px 8px 8px 32px;\n        text-decoration: none;\n        font-size: 15px;\n        color: white;\n        display: block;\n        transition: 0.3s;\n      }\n      \n      .mobile-sidenav a:hover {\n        color: #f1f1f1;\n      }\n      \n      .mobile-sidenav .closebtn {\n        position: absolute;\n        top: 0;\n        right: 25px;\n        font-size: 36px;\n        margin-left: 50px;\n      }\n      \n}\n@media screen and (max-height: 450px) {\n    .mobile-sidenav {padding-top: 15px;}\n    .mobile-sidenav a {font-size: 18px;}\n  }\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvc2hhcmVkL21vbGVjdWxlcy9oZWFkZXItbmF2LW1vbGVjdWxlL2hlYWRlci1uYXYtbW9sZWN1bGUuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtJQUNJLGFBQWE7SUFDYixtQkFBbUI7SUFDbkIsNkJBQTZCO0lBQzdCLHlCQUF5QjtJQUN6QixpQkFBaUI7QUFDckI7QUFDQTtJQUNJLGFBQWE7QUFDakI7QUFFQTtJQUNJLFVBQVU7SUFDVixhQUFhO0lBQ2IsbUJBQW1CO0FBQ3ZCO0FBRUE7SUFDSSxnQkFBZ0I7SUFDaEIsU0FBUztJQUNULFVBQVU7SUFDVixhQUFhO0FBQ2pCO0FBRUE7SUFDSSxlQUFlO0FBQ25CO0FBQ0M7S0FDSSxZQUFZO0tBQ1oscUJBQXFCO0tBQ3JCLG9DQUFvQztLQUNwQyxtQkFBbUI7QUFDeEI7QUFFQTtJQUNJLGdDQUFnQztBQUNwQztBQUNBO0lBQ0ksVUFBVTtJQUNWLGFBQWE7SUFDYix1QkFBdUI7SUFDdkIsbUJBQW1CO0FBQ3ZCO0FBQ0E7SUFDSTtRQUNJLGFBQWE7SUFDakI7SUFDQTtRQUNJLGNBQWM7UUFDZCxVQUFVO0lBQ2Q7SUFDQTtRQUNJLGFBQWE7SUFDakI7SUFDQTtRQUNJLFVBQVU7UUFDVixnQkFBZ0I7SUFDcEI7SUFDQTtRQUNJLGFBQWE7UUFDYixxQkFBcUI7UUFDckI7SUFDSjtJQUNBO1FBQ0ksWUFBWTtJQUNoQjtJQUNBO1FBQ0ksY0FBYztRQUNkLFlBQVk7UUFDWixRQUFRO1FBQ1IsZUFBZTtRQUNmLGFBQWE7UUFDYixNQUFNO1FBQ04sT0FBTztRQUNQLHNCQUFzQjtRQUN0QixrQkFBa0I7UUFDbEIsZ0JBQWdCO1FBQ2hCLGlCQUFpQjtNQUNuQjs7TUFFQTtRQUNFLHlCQUF5QjtRQUN6QixxQkFBcUI7UUFDckIsZUFBZTtRQUNmLFlBQVk7UUFDWixjQUFjO1FBQ2QsZ0JBQWdCO01BQ2xCOztNQUVBO1FBQ0UsY0FBYztNQUNoQjs7TUFFQTtRQUNFLGtCQUFrQjtRQUNsQixNQUFNO1FBQ04sV0FBVztRQUNYLGVBQWU7UUFDZixpQkFBaUI7TUFDbkI7O0FBRU47QUFDRTtJQUNFLGlCQUFpQixpQkFBaUIsQ0FBQztJQUNuQyxtQkFBbUIsZUFBZSxDQUFDO0VBQ3JDIiwiZmlsZSI6InNyYy9hcHAvc2hhcmVkL21vbGVjdWxlcy9oZWFkZXItbmF2LW1vbGVjdWxlL2hlYWRlci1uYXYtbW9sZWN1bGUuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIi5uYXYtY29udGFpbmVyIHtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XG4gICAgZm9udC1mYW1pbHk6ICdSb2JvdG8tUmVndWxhcic7XG4gICAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcbiAgICBmb250LXNpemU6IDAuODVlbTtcbn1cbi5tb2JpbGUtc2lkZW5hdiwgLmhhbWJ1cmdlciB7XG4gICAgZGlzcGxheTogbm9uZTtcbn1cblxuLm5hdi1jb250YWluZXIgbmF2IHtcbiAgICB3aWR0aDogNzAlO1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbn1cblxuLm5hdi1jb250YWluZXIgbmF2IHVsIHtcbiAgICBsaXN0LXN0eWxlOiBub25lO1xuICAgIG1hcmdpbjogMDtcbiAgICBwYWRkaW5nOiAwO1xuICAgIGRpc3BsYXk6IGZsZXg7XG59XG5cbi5uYXYtY29udGFpbmVyIG5hdiB1bCBsaSB7XG4gICAgcGFkZGluZzogMCAxMHB4O1xufVxuIC5uYXYtY29udGFpbmVyIG5hdiB1bCBsaSBhIHtcbiAgICAgY29sb3I6IHdoaXRlO1xuICAgICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XG4gICAgIGJvcmRlci1ib3R0b206IDNweCBzb2xpZCB0cmFuc3BhcmVudDtcbiAgICAgcGFkZGluZy1ib3R0b206IDVweDtcbn1cblxuLm5hdi1jb250YWluZXIgbmF2IHVsIGxpIGE6aG92ZXIge1xuICAgIGJvcmRlci1ib3R0b206IDNweCBzb2xpZCAjQTdGMTA4O1xufVxuLmNhcnQtY29udGFpbmVye1xuICAgIHdpZHRoOiAxMCU7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xufVxuQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiA3NjhweCkge1xuICAgIC5uYXYtY29udGFpbmVyIHtcbiAgICAgICAgZGlzcGxheTogbm9uZTtcbiAgICB9XG4gICAgLmhhbWJ1cmdlciB7XG4gICAgICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgICAgICB3aWR0aDogMjAlO1xuICAgIH1cbiAgICAubW9iaWxlLW5hdi1jb250YWluZXIge1xuICAgICAgICBkaXNwbGF5OiBmbGV4O1xuICAgIH1cbiAgICAubW9iaWxlLW5hdi1jb250YWluZXIgdWwge1xuICAgICAgICBwYWRkaW5nOiAwO1xuICAgICAgICBsaXN0LXN0eWxlOiBub25lO1xuICAgIH1cbiAgICAubW9iaWxlLWxvZ28tY29udGFpbmVyIHtcbiAgICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBsZWZ0O1xuICAgICAgICB3aWR0aDogODAlXG4gICAgfVxuICAgIC5tb2JpbGUtbG9nby1jb250YWluZXIgaW1nIHtcbiAgICAgICAgd2lkdGg6IDIwMHB4O1xuICAgIH1cbiAgICAubW9iaWxlLXNpZGVuYXYge1xuICAgICAgICBkaXNwbGF5OiBibG9jaztcbiAgICAgICAgaGVpZ2h0OiAxMDAlO1xuICAgICAgICB3aWR0aDogMDtcbiAgICAgICAgcG9zaXRpb246IGZpeGVkO1xuICAgICAgICB6LWluZGV4OiA5OTk5O1xuICAgICAgICB0b3A6IDA7XG4gICAgICAgIGxlZnQ6IDA7XG4gICAgICAgIGJhY2tncm91bmQtY29sb3I6ICMxMTE7XG4gICAgICAgIG92ZXJmbG93LXg6IGhpZGRlbjtcbiAgICAgICAgdHJhbnNpdGlvbjogMC41cztcbiAgICAgICAgcGFkZGluZy10b3A6IDYwcHg7XG4gICAgICB9XG4gICAgICBcbiAgICAgIC5tb2JpbGUtc2lkZW5hdiBhIHtcbiAgICAgICAgcGFkZGluZzogOHB4IDhweCA4cHggMzJweDtcbiAgICAgICAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xuICAgICAgICBmb250LXNpemU6IDE1cHg7XG4gICAgICAgIGNvbG9yOiB3aGl0ZTtcbiAgICAgICAgZGlzcGxheTogYmxvY2s7XG4gICAgICAgIHRyYW5zaXRpb246IDAuM3M7XG4gICAgICB9XG4gICAgICBcbiAgICAgIC5tb2JpbGUtc2lkZW5hdiBhOmhvdmVyIHtcbiAgICAgICAgY29sb3I6ICNmMWYxZjE7XG4gICAgICB9XG4gICAgICBcbiAgICAgIC5tb2JpbGUtc2lkZW5hdiAuY2xvc2VidG4ge1xuICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgICAgIHRvcDogMDtcbiAgICAgICAgcmlnaHQ6IDI1cHg7XG4gICAgICAgIGZvbnQtc2l6ZTogMzZweDtcbiAgICAgICAgbWFyZ2luLWxlZnQ6IDUwcHg7XG4gICAgICB9XG4gICAgICBcbn1cbiAgQG1lZGlhIHNjcmVlbiBhbmQgKG1heC1oZWlnaHQ6IDQ1MHB4KSB7XG4gICAgLm1vYmlsZS1zaWRlbmF2IHtwYWRkaW5nLXRvcDogMTVweDt9XG4gICAgLm1vYmlsZS1zaWRlbmF2IGEge2ZvbnQtc2l6ZTogMThweDt9XG4gIH0iXX0= */");

/***/ }),

/***/ "./src/app/shared/molecules/header-nav-molecule/header-nav-molecule.component.ts":
/*!***************************************************************************************!*\
  !*** ./src/app/shared/molecules/header-nav-molecule/header-nav-molecule.component.ts ***!
  \***************************************************************************************/
/*! exports provided: HeaderNavMoleculeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HeaderNavMoleculeComponent", function() { return HeaderNavMoleculeComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let HeaderNavMoleculeComponent = class HeaderNavMoleculeComponent {
    constructor() {
        this.closeNav = () => {
            document.getElementById("mySidenav").style.width = "0";
        };
        this.openNav = () => {
            document.getElementById("mySidenav").style.width = "150px";
        };
    }
    ngOnInit() {
        console.log(this.headerNavLinks);
        console.log(this.logoUrl);
    }
};
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])()
], HeaderNavMoleculeComponent.prototype, "headerNavLinks", void 0);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])()
], HeaderNavMoleculeComponent.prototype, "logoUrl", void 0);
HeaderNavMoleculeComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'cx-header-nav-molecule',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./header-nav-molecule.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/molecules/header-nav-molecule/header-nav-molecule.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./header-nav-molecule.component.css */ "./src/app/shared/molecules/header-nav-molecule/header-nav-molecule.component.css")).default]
    })
], HeaderNavMoleculeComponent);



/***/ }),

/***/ "./src/app/shared/molecules/login-form-molecule/login-form-molecule.component.scss":
/*!*****************************************************************************************!*\
  !*** ./src/app/shared/molecules/login-form-molecule/login-form-molecule.component.scss ***!
  \*****************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("/********************Custom Variables **************************/\n.text-white {\n  color: #fff;\n}\n.secondary-bg {\n  background: #000;\n}\n.primary-color {\n  color: #a7f108;\n}\n.no-margin {\n  margin: 0;\n}\n.no-padding {\n  padding: 0;\n}\n.margin-auto {\n  margin: 0 auto;\n}\n.font-bold {\n  font-weight: 800;\n}\n.font-semi-bold {\n  font-weight: 600;\n}\n.mb-30 {\n  margin-bottom: 30px;\n}\n.mb-50 {\n  margin-bottom: 50px;\n}\n.pr-20 {\n  padding-right: 20px;\n}\n.bg-white {\n  background: #fff;\n}\n.title {\n  display: flex;\n  justify-content: center;\n  padding: 30px;\n  margin: 0;\n  background-color: var(--cx-color-background);\n}\n.login-us-form {\n  min-width: 150px;\n  max-width: 500px;\n  width: 100%;\n  display: flex;\n  flex-direction: column;\n  justify-content: center;\n  margin: 40px auto !important;\n  box-shadow: 0 3px 36px rgba(0, 0, 0, 0.09);\n  padding: 40px;\n  margin: 40px auto;\n}\n.email-field, .password-field {\n  width: 100%;\n}\n.login-container {\n  padding: 0;\n  box-shadow: none;\n}\n.submit-btn {\n  background-color: #a7f108 !important;\n  border-color: #a7f108 !important;\n  color: #ffffff !important;\n  font-size: 20px;\n  font-weight: 400;\n  height: 48px;\n  max-height: 48px;\n}\n.register-btn {\n  background-color: rgba(0, 0, 0, 0.12);\n  font-size: 20px;\n  font-weight: 400;\n  height: 48px;\n  max-height: 48px;\n}\n.forgot-pwd {\n  padding-bottom: 20px;\n}\n.forgot-pwd a {\n  color: #000000;\n  text-decoration: underline !important;\n}\n.forgot-pwd a:hover {\n  color: var(--cx-color-primary) !important;\n}\n.register-link {\n  margin-bottom: 10px;\n}\n.error-msg {\n  display: flex;\n  justify-content: center;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi92YXIvd3d3L2h0bWwvanMtc3RvcmVmcm9udC9zcGFydGFjdXNzdG9yZS9zcmMvYXNzZXRzL2Nzcy92YXJpYWJsZS5zY3NzIiwic3JjL2FwcC9zaGFyZWQvbW9sZWN1bGVzL2xvZ2luLWZvcm0tbW9sZWN1bGUvbG9naW4tZm9ybS1tb2xlY3VsZS5jb21wb25lbnQuc2NzcyIsIi92YXIvd3d3L2h0bWwvanMtc3RvcmVmcm9udC9zcGFydGFjdXNzdG9yZS9zcmMvYXBwL3NoYXJlZC9tb2xlY3VsZXMvbG9naW4tZm9ybS1tb2xlY3VsZS9sb2dpbi1mb3JtLW1vbGVjdWxlLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLGdFQUFBO0FBTUE7RUFDSSxXQUFBO0FDSko7QURNRTtFQUNFLGdCQUFBO0FDSEo7QURLRTtFQUNFLGNBQUE7QUNGSjtBRElFO0VBQ0UsU0FBQTtBQ0RKO0FER0U7RUFDRSxVQUFBO0FDQUo7QURFRTtFQUNFLGNBQUE7QUNDSjtBRENFO0VBQ0UsZ0JBQUE7QUNFSjtBREFFO0VBQ0UsZ0JBQUE7QUNHSjtBRERFO0VBQ0UsbUJBQUE7QUNJSjtBREZFO0VBQ0UsbUJBQUE7QUNLSjtBREhFO0VBQ0UsbUJBQUE7QUNNSjtBREpFO0VBQ0UsZ0JBQUE7QUNPSjtBQzlDQTtFQUNJLGFBQUE7RUFDQSx1QkFBQTtFQUNBLGFBQUE7RUFDQSxTQUFBO0VBQ0EsNENBQUE7QURpREo7QUN4QkE7RUFDSSxnQkFBQTtFQUNBLGdCQUFBO0VBQ0EsV0FBQTtFQUNBLGFBQUE7RUFDQSxzQkFBQTtFQUNBLHVCQUFBO0VBQ0EsNEJBQUE7RUFDQSwwQ0FBQTtFQUNBLGFBQUE7RUFDQSxpQkFBQTtBRDJCSjtBQ3pCRTtFQUNFLFdBQUE7QUQ0Qko7QUMxQkU7RUFDSSxVQUFBO0VBQ0EsZ0JBQUE7QUQ2Qk47QUMzQkU7RUFDRSxvQ0FBQTtFQUNBLGdDQUFBO0VBQ0EseUJBQUE7RUFDQSxlQUFBO0VBQ0EsZ0JBQUE7RUFDQSxZQUFBO0VBQ0EsZ0JBQUE7QUQ4Qko7QUMzQkU7RUFDRSxxQ0FBQTtFQUNBLGVBQUE7RUFDQSxnQkFBQTtFQUNBLFlBQUE7RUFDQSxnQkFBQTtBRDhCSjtBQzNCRTtFQUNJLG9CQUFBO0FEOEJOO0FDN0JNO0VBQ0ksY0ZyRUY7RUVzRUUscUNBQUE7QUQrQlY7QUM5QlU7RUFDSSx5Q0FBQTtBRGdDZDtBQzVCRTtFQUNJLG1CQUFBO0FEK0JOO0FDNUJFO0VBQ0UsYUFBQTtFQUNBLHVCQUFBO0FEK0JKIiwiZmlsZSI6InNyYy9hcHAvc2hhcmVkL21vbGVjdWxlcy9sb2dpbi1mb3JtLW1vbGVjdWxlL2xvZ2luLWZvcm0tbW9sZWN1bGUuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIvKioqKioqKioqKioqKioqKioqKipDdXN0b20gVmFyaWFibGVzICoqKioqKioqKioqKioqKioqKioqKioqKioqL1xuJHdoaXRlOiAjZmZmZmZmO1xuJGJsYWNrOiAjMDAwMDAwO1xuJHByaW1hcnktY29sb3I6ICNhN2YxMDg7XG4kYm9yZGVyLWNvbG9yOiAgcmdiYSgyNTUsIDI1NSwgMjU1LCAwLjYpO1xuJGZvcm0tYmc6ICNmN2Y3Zjc7XG4udGV4dC13aGl0ZSB7XG4gICAgY29sb3I6ICNmZmY7XG4gIH1cbiAgLnNlY29uZGFyeS1iZyB7XG4gICAgYmFja2dyb3VuZDogIzAwMDtcbiAgfVxuICAucHJpbWFyeS1jb2xvciB7XG4gICAgY29sb3I6ICNhN2YxMDg7XG4gIH1cbiAgLm5vLW1hcmdpbiB7XG4gICAgbWFyZ2luOiAwO1xuICB9XG4gIC5uby1wYWRkaW5nIHtcbiAgICBwYWRkaW5nOiAwO1xuICB9XG4gIC5tYXJnaW4tYXV0byB7XG4gICAgbWFyZ2luOiAwIGF1dG87XG4gIH1cbiAgLmZvbnQtYm9sZCB7XG4gICAgZm9udC13ZWlnaHQ6IDgwMDtcbiAgfVxuICAuZm9udC1zZW1pLWJvbGQge1xuICAgIGZvbnQtd2VpZ2h0OiA2MDA7XG4gIH1cbiAgLm1iLTMwIHtcbiAgICBtYXJnaW4tYm90dG9tOiAzMHB4O1xuICB9XG4gIC5tYi01MCB7XG4gICAgbWFyZ2luLWJvdHRvbTogNTBweDtcbiAgfVxuICAucHItMjAge1xuICAgIHBhZGRpbmctcmlnaHQ6IDIwcHg7XG4gIH1cbiAgLmJnLXdoaXRlIHtcbiAgICBiYWNrZ3JvdW5kOiAjZmZmO1xuICB9IiwiLyoqKioqKioqKioqKioqKioqKioqQ3VzdG9tIFZhcmlhYmxlcyAqKioqKioqKioqKioqKioqKioqKioqKioqKi9cbi50ZXh0LXdoaXRlIHtcbiAgY29sb3I6ICNmZmY7XG59XG5cbi5zZWNvbmRhcnktYmcge1xuICBiYWNrZ3JvdW5kOiAjMDAwO1xufVxuXG4ucHJpbWFyeS1jb2xvciB7XG4gIGNvbG9yOiAjYTdmMTA4O1xufVxuXG4ubm8tbWFyZ2luIHtcbiAgbWFyZ2luOiAwO1xufVxuXG4ubm8tcGFkZGluZyB7XG4gIHBhZGRpbmc6IDA7XG59XG5cbi5tYXJnaW4tYXV0byB7XG4gIG1hcmdpbjogMCBhdXRvO1xufVxuXG4uZm9udC1ib2xkIHtcbiAgZm9udC13ZWlnaHQ6IDgwMDtcbn1cblxuLmZvbnQtc2VtaS1ib2xkIHtcbiAgZm9udC13ZWlnaHQ6IDYwMDtcbn1cblxuLm1iLTMwIHtcbiAgbWFyZ2luLWJvdHRvbTogMzBweDtcbn1cblxuLm1iLTUwIHtcbiAgbWFyZ2luLWJvdHRvbTogNTBweDtcbn1cblxuLnByLTIwIHtcbiAgcGFkZGluZy1yaWdodDogMjBweDtcbn1cblxuLmJnLXdoaXRlIHtcbiAgYmFja2dyb3VuZDogI2ZmZjtcbn1cblxuLnRpdGxlIHtcbiAgZGlzcGxheTogZmxleDtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gIHBhZGRpbmc6IDMwcHg7XG4gIG1hcmdpbjogMDtcbiAgYmFja2dyb3VuZC1jb2xvcjogdmFyKC0tY3gtY29sb3ItYmFja2dyb3VuZCk7XG59XG5cbi5sb2dpbi11cy1mb3JtIHtcbiAgbWluLXdpZHRoOiAxNTBweDtcbiAgbWF4LXdpZHRoOiA1MDBweDtcbiAgd2lkdGg6IDEwMCU7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICBtYXJnaW46IDQwcHggYXV0byAhaW1wb3J0YW50O1xuICBib3gtc2hhZG93OiAwIDNweCAzNnB4IHJnYmEoMCwgMCwgMCwgMC4wOSk7XG4gIHBhZGRpbmc6IDQwcHg7XG4gIG1hcmdpbjogNDBweCBhdXRvO1xufVxuXG4uZW1haWwtZmllbGQsIC5wYXNzd29yZC1maWVsZCB7XG4gIHdpZHRoOiAxMDAlO1xufVxuXG4ubG9naW4tY29udGFpbmVyIHtcbiAgcGFkZGluZzogMDtcbiAgYm94LXNoYWRvdzogbm9uZTtcbn1cblxuLnN1Ym1pdC1idG4ge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjYTdmMTA4ICFpbXBvcnRhbnQ7XG4gIGJvcmRlci1jb2xvcjogI2E3ZjEwOCAhaW1wb3J0YW50O1xuICBjb2xvcjogI2ZmZmZmZiAhaW1wb3J0YW50O1xuICBmb250LXNpemU6IDIwcHg7XG4gIGZvbnQtd2VpZ2h0OiA0MDA7XG4gIGhlaWdodDogNDhweDtcbiAgbWF4LWhlaWdodDogNDhweDtcbn1cblxuLnJlZ2lzdGVyLWJ0biB7XG4gIGJhY2tncm91bmQtY29sb3I6IHJnYmEoMCwgMCwgMCwgMC4xMik7XG4gIGZvbnQtc2l6ZTogMjBweDtcbiAgZm9udC13ZWlnaHQ6IDQwMDtcbiAgaGVpZ2h0OiA0OHB4O1xuICBtYXgtaGVpZ2h0OiA0OHB4O1xufVxuXG4uZm9yZ290LXB3ZCB7XG4gIHBhZGRpbmctYm90dG9tOiAyMHB4O1xufVxuLmZvcmdvdC1wd2QgYSB7XG4gIGNvbG9yOiAjMDAwMDAwO1xuICB0ZXh0LWRlY29yYXRpb246IHVuZGVybGluZSAhaW1wb3J0YW50O1xufVxuLmZvcmdvdC1wd2QgYTpob3ZlciB7XG4gIGNvbG9yOiB2YXIoLS1jeC1jb2xvci1wcmltYXJ5KSAhaW1wb3J0YW50O1xufVxuXG4ucmVnaXN0ZXItbGluayB7XG4gIG1hcmdpbi1ib3R0b206IDEwcHg7XG59XG5cbi5lcnJvci1tc2cge1xuICBkaXNwbGF5OiBmbGV4O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbn0iLCJAaW1wb3J0IFwiLi4vLi4vLi4vLi4vYXNzZXRzL2Nzcy9jb25zdGFudHMuc2Nzc1wiO1xuLnRpdGxlIHtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICAgIHBhZGRpbmc6IDMwcHg7XG4gICAgbWFyZ2luOiAwO1xuICAgIGJhY2tncm91bmQtY29sb3I6IHZhcigtLWN4LWNvbG9yLWJhY2tncm91bmQpO1xufVxuXG4vLyBtYXQtZm9ybS1maWVsZCB7XG4vLyAgICAgZmxvYXQ6IG5vbmUgIWltcG9ydGFudDtcbi8vICAgICB3aWR0aDogMTAwJTtcbi8vICAgICBkaXNwbGF5OiBmbGV4O1xuLy8gICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuLy8gfVxuLy8gLmxvZ2luLXVzLWZvcm0ge1xuLy8gICAgIGRpc3BsYXk6IGZsZXg7XG4vLyAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbi8vICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbi8vICAgICB3aWR0aDogMTAwJTtcbi8vIH1cbi8vIC5tYXQtZm9ybS1maWVsZC13cmFwcGVyIHtcbi8vICAgICB3aWR0aDogMTAwJTtcbi8vIH1cbi8vIGlucHV0IHtcbi8vICAgICB3aWR0aDogMTAwJVxuLy8gfVxuLy8gLmVtYWlsLWZpZWxkLCAucGFzc3dvcmQtZmllbGQge1xuLy8gICAgIHdpZHRoOiAxMDAlO1xuLy8gfVxuXG4ubG9naW4tdXMtZm9ybSB7XG4gICAgbWluLXdpZHRoOiAxNTBweDtcbiAgICBtYXgtd2lkdGg6IDUwMHB4O1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgICBtYXJnaW46IDQwcHggYXV0byAhaW1wb3J0YW50O1xuICAgIGJveC1zaGFkb3c6IDAgM3B4IDM2cHggcmdiYSgwLCAwLCAwLCAuMDkpO1xuICAgIHBhZGRpbmc6IDQwcHg7XG4gICAgbWFyZ2luOiA0MHB4IGF1dG87XG4gIH1cbiAgLmVtYWlsLWZpZWxkLCAucGFzc3dvcmQtZmllbGQge1xuICAgIHdpZHRoOiAxMDAlO1xuICB9XG4gIC5sb2dpbi1jb250YWluZXIge1xuICAgICAgcGFkZGluZzogMDtcbiAgICAgIGJveC1zaGFkb3c6IG5vbmU7XG4gIH1cbiAgLnN1Ym1pdC1idG4ge1xuICAgIGJhY2tncm91bmQtY29sb3I6ICRwcmltYXJ5LWNvbG9yICFpbXBvcnRhbnQ7XG4gICAgYm9yZGVyLWNvbG9yOiAkcHJpbWFyeS1jb2xvciAhaW1wb3J0YW50O1xuICAgIGNvbG9yOiAkd2hpdGUgIWltcG9ydGFudDtcbiAgICBmb250LXNpemU6IDIwcHg7XG4gICAgZm9udC13ZWlnaHQ6IDQwMDtcbiAgICBoZWlnaHQ6IDQ4cHg7XG4gICAgbWF4LWhlaWdodDogNDhweDtcblxuICB9XG4gIC5yZWdpc3Rlci1idG4ge1xuICAgIGJhY2tncm91bmQtY29sb3I6IHJnYmEoMCwwLDAsLjEyKTtcbiAgICBmb250LXNpemU6IDIwcHg7XG4gICAgZm9udC13ZWlnaHQ6IDQwMDtcbiAgICBoZWlnaHQ6IDQ4cHg7XG4gICAgbWF4LWhlaWdodDogNDhweDtcblxuICB9XG4gIC5mb3Jnb3QtcHdkIHtcbiAgICAgIHBhZGRpbmctYm90dG9tOiAyMHB4O1xuICAgICAgYSB7XG4gICAgICAgICAgY29sb3I6ICRibGFjaztcbiAgICAgICAgICB0ZXh0LWRlY29yYXRpb246IHVuZGVybGluZSAhaW1wb3J0YW50O1xuICAgICAgICAgICY6aG92ZXIge1xuICAgICAgICAgICAgICBjb2xvcjogdmFyKC0tY3gtY29sb3ItcHJpbWFyeSkgIWltcG9ydGFudDtcbiAgICAgICAgICB9XG4gICAgICB9XG4gIH1cbiAgLnJlZ2lzdGVyLWxpbmsge1xuICAgICAgbWFyZ2luLWJvdHRvbTogMTBweDtcbiAgfVxuXG4gIC5lcnJvci1tc2cge1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gIH0iXX0= */");

/***/ }),

/***/ "./src/app/shared/molecules/login-form-molecule/login-form-molecule.component.ts":
/*!***************************************************************************************!*\
  !*** ./src/app/shared/molecules/login-form-molecule/login-form-molecule.component.ts ***!
  \***************************************************************************************/
/*! exports provided: LoginFormMoleculeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginFormMoleculeComponent", function() { return LoginFormMoleculeComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _services_login_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../services/login.service */ "./src/app/services/login.service.ts");
/* harmony import */ var _services_common_utility_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../services/common-utility-service */ "./src/app/services/common-utility-service.ts");






let LoginFormMoleculeComponent = class LoginFormMoleculeComponent {
    constructor(fb, loginService, router, utilityService) {
        this.fb = fb;
        this.loginService = loginService;
        this.router = router;
        this.utilityService = utilityService;
        this.loginError = false;
        this.validationMessages = {
            email: [
                { type: 'required', message: 'Email is required' },
                { type: 'email', message: 'Enter a valid email' },
            ],
            password: [{ type: 'required', message: 'Password is required' }],
            message: [{ type: 'required', message: 'Please enter some message' }],
        };
        this.loginService.userContext.subscribe(context => {
            this.userContext = context;
        });
    }
    ngOnInit() {
        // user details form validations
        this.userLoginForm = this.fb.group({
            email: [
                null,
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].email]),
            ],
            password: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required])],
        });
    }
    onSubmitLoginDetails(userData) {
        console.log('userValue', userData);
        if (!!userData && !!userData.email && !!userData.password) {
            this.loginService.userLogin(userData).subscribe((res) => {
                if (!!res && res.access_token) {
                    this.loginService
                        .fetchUserDetails(res.access_token, userData.email)
                        .subscribe((res) => {
                        if (res && !res.active) {
                            this.userContext.displayUID = res.displayUid;
                            this.userContext.displayName = res.firstName;
                            this.userContext.isAuthenticated = true;
                            this.loginService.changeUserContext(this.userContext);
                            this.utilityService.setCookie('userName', this.userContext.displayName);
                            this.utilityService.setCookie('displayUid', this.userContext.displayUID);
                            this.utilityService.setCookie('isAuthenticated', true);
                            this.router.navigate(['/']);
                        }
                    }, err => {
                        this.loginError = true;
                    });
                }
                else {
                    this.loginError = true;
                }
            }, err => {
                this.loginError = true;
            });
        }
    }
};
LoginFormMoleculeComponent.ctorParameters = () => [
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"] },
    { type: _services_login_service__WEBPACK_IMPORTED_MODULE_4__["LoginService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] },
    { type: _services_common_utility_service__WEBPACK_IMPORTED_MODULE_5__["CommonUtilityService"] }
];
LoginFormMoleculeComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-login-form-molecule',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./login-form-molecule.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/molecules/login-form-molecule/login-form-molecule.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./login-form-molecule.component.scss */ "./src/app/shared/molecules/login-form-molecule/login-form-molecule.component.scss")).default]
    })
], LoginFormMoleculeComponent);



/***/ }),

/***/ "./src/app/shared/molecules/logo-molecule/logo-molecule.component.scss":
/*!*****************************************************************************!*\
  !*** ./src/app/shared/molecules/logo-molecule/logo-molecule.component.scss ***!
  \*****************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".mobile-logo-container {\n  display: none;\n}\n\n:host {\n  width: 20%;\n  display: block;\n  padding: 0 10px;\n}\n\n.logo-container img {\n  max-width: 80%;\n}\n\n@media only screen and (max-width: 768px) {\n  :host {\n    display: flex;\n    justify-content: center;\n    width: 80%;\n  }\n\n  .mobile-logo-container {\n    display: block;\n  }\n  .mobile-logo-container img {\n    width: 200px;\n  }\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi92YXIvd3d3L2h0bWwvanMtc3RvcmVmcm9udC9zcGFydGFjdXNzdG9yZS9zcmMvYXBwL3NoYXJlZC9tb2xlY3VsZXMvbG9nby1tb2xlY3VsZS9sb2dvLW1vbGVjdWxlLmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC9zaGFyZWQvbW9sZWN1bGVzL2xvZ28tbW9sZWN1bGUvbG9nby1tb2xlY3VsZS5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLGFBQUE7QUNDSjs7QURDQTtFQUNJLFVBQUE7RUFDQSxjQUFBO0VBQ0EsZUFBQTtBQ0VKOztBRENBO0VBQ0ksY0FBQTtBQ0VKOztBREFBO0VBQ0k7SUFDSSxhQUFBO0lBQ0EsdUJBQUE7SUFDQSxVQUFBO0VDR047O0VEREU7SUFDSSxjQUFBO0VDSU47RURITTtJQUNJLFlBQUE7RUNLVjtBQUNGIiwiZmlsZSI6InNyYy9hcHAvc2hhcmVkL21vbGVjdWxlcy9sb2dvLW1vbGVjdWxlL2xvZ28tbW9sZWN1bGUuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIubW9iaWxlLWxvZ28tY29udGFpbmVyIHtcbiAgICBkaXNwbGF5OiBub25lO1xufVxuOmhvc3Qge1xuICAgIHdpZHRoOiAyMCU7XG4gICAgZGlzcGxheTogYmxvY2s7XG4gICAgcGFkZGluZzogMCAxMHB4O1xufVxuXG4ubG9nby1jb250YWluZXIgaW1nIHtcbiAgICBtYXgtd2lkdGg6IDgwJTtcbn1cbkBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1heC13aWR0aDogNzY4cHgpIHtcbiAgICA6aG9zdCB7XG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICAgICAgICB3aWR0aDogODAlXG4gICAgfVxuICAgIC5tb2JpbGUtbG9nby1jb250YWluZXIge1xuICAgICAgICBkaXNwbGF5OiBibG9jaztcbiAgICAgICAgaW1nIHtcbiAgICAgICAgICAgIHdpZHRoOiAyMDBweDtcbiAgICAgICAgfVxuICAgIH0gXG59IiwiLm1vYmlsZS1sb2dvLWNvbnRhaW5lciB7XG4gIGRpc3BsYXk6IG5vbmU7XG59XG5cbjpob3N0IHtcbiAgd2lkdGg6IDIwJTtcbiAgZGlzcGxheTogYmxvY2s7XG4gIHBhZGRpbmc6IDAgMTBweDtcbn1cblxuLmxvZ28tY29udGFpbmVyIGltZyB7XG4gIG1heC13aWR0aDogODAlO1xufVxuXG5AbWVkaWEgb25seSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDc2OHB4KSB7XG4gIDpob3N0IHtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICAgIHdpZHRoOiA4MCU7XG4gIH1cblxuICAubW9iaWxlLWxvZ28tY29udGFpbmVyIHtcbiAgICBkaXNwbGF5OiBibG9jaztcbiAgfVxuICAubW9iaWxlLWxvZ28tY29udGFpbmVyIGltZyB7XG4gICAgd2lkdGg6IDIwMHB4O1xuICB9XG59Il19 */");

/***/ }),

/***/ "./src/app/shared/molecules/logo-molecule/logo-molecule.component.ts":
/*!***************************************************************************!*\
  !*** ./src/app/shared/molecules/logo-molecule/logo-molecule.component.ts ***!
  \***************************************************************************/
/*! exports provided: LogoMoleculeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LogoMoleculeComponent", function() { return LogoMoleculeComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../environments/environment */ "./src/environments/environment.ts");



let LogoMoleculeComponent = class LogoMoleculeComponent {
    constructor() {
        this.hostName = _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].hostName;
    }
    ngOnInit() {
    }
};
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])()
], LogoMoleculeComponent.prototype, "displayType", void 0);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])()
], LogoMoleculeComponent.prototype, "logoUrl", void 0);
LogoMoleculeComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-logo-molecule',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./logo-molecule.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/molecules/logo-molecule/logo-molecule.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./logo-molecule.component.scss */ "./src/app/shared/molecules/logo-molecule/logo-molecule.component.scss")).default]
    })
], LogoMoleculeComponent);



/***/ }),

/***/ "./src/app/shared/molecules/molecules.module.ts":
/*!******************************************************!*\
  !*** ./src/app/shared/molecules/molecules.module.ts ***!
  \******************************************************/
/*! exports provided: MoleculesModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MoleculesModule", function() { return MoleculesModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm2015/platform-browser.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _material_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../material.module */ "./src/app/material.module.ts");
/* harmony import */ var _services_services_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../services/services.module */ "./src/app/services/services.module.ts");
/* harmony import */ var _directives_directives_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../directives/directives.module */ "./src/app/shared/directives/directives.module.ts");
/* harmony import */ var _pipes_pipes_module__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../pipes/pipes.module */ "./src/app/shared/pipes/pipes.module.ts");
/* harmony import */ var _header_links_molecule_header_links_molecule_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./header-links-molecule/header-links-molecule.component */ "./src/app/shared/molecules/header-links-molecule/header-links-molecule.component.ts");
/* harmony import */ var _header_nav_molecule_header_nav_molecule_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./header-nav-molecule/header-nav-molecule.component */ "./src/app/shared/molecules/header-nav-molecule/header-nav-molecule.component.ts");
/* harmony import */ var _carousal_molecule_carousal_molecule_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./carousal-molecule/carousal-molecule.component */ "./src/app/shared/molecules/carousal-molecule/carousal-molecule.component.ts");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm2015/ng-bootstrap.js");
/* harmony import */ var _logo_molecule_logo_molecule_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./logo-molecule/logo-molecule.component */ "./src/app/shared/molecules/logo-molecule/logo-molecule.component.ts");
/* harmony import */ var _contact_us_molecule_contact_us_molecule_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./contact-us-molecule/contact-us-molecule.component */ "./src/app/shared/molecules/contact-us-molecule/contact-us-molecule.component.ts");
/* harmony import */ var _client_logo_molecule_client_logo_component__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./client-logo-molecule/client-logo.component */ "./src/app/shared/molecules/client-logo-molecule/client-logo.component.ts");
/* harmony import */ var _saned_section_molecule_saned_section_molecule_component__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./saned-section-molecule/saned-section-molecule.component */ "./src/app/shared/molecules/saned-section-molecule/saned-section-molecule.component.ts");
/* harmony import */ var _register_molecule_register_molecule_component__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./register-molecule/register-molecule.component */ "./src/app/shared/molecules/register-molecule/register-molecule.component.ts");
/* harmony import */ var _sign_up_molecule_sign_up_component__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ./sign-up-molecule/sign-up.component */ "./src/app/shared/molecules/sign-up-molecule/sign-up.component.ts");
/* harmony import */ var _footer_molecule_footer_component__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ./footer-molecule/footer.component */ "./src/app/shared/molecules/footer-molecule/footer.component.ts");
/* harmony import */ var _login_form_molecule_login_form_molecule_component__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! ./login-form-molecule/login-form-molecule.component */ "./src/app/shared/molecules/login-form-molecule/login-form-molecule.component.ts");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! ../../app-routing.module */ "./src/app/app-routing.module.ts");
/* harmony import */ var _cart_molecule_cart_molecule_component__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! ./cart-molecule/cart-molecule.component */ "./src/app/shared/molecules/cart-molecule/cart-molecule.component.ts");
























const components = [
    _header_links_molecule_header_links_molecule_component__WEBPACK_IMPORTED_MODULE_10__["HeaderLinksMoleculeComponent"],
    _header_nav_molecule_header_nav_molecule_component__WEBPACK_IMPORTED_MODULE_11__["HeaderNavMoleculeComponent"],
    _carousal_molecule_carousal_molecule_component__WEBPACK_IMPORTED_MODULE_12__["CarousalMoleculeComponent"],
    _logo_molecule_logo_molecule_component__WEBPACK_IMPORTED_MODULE_14__["LogoMoleculeComponent"],
    _contact_us_molecule_contact_us_molecule_component__WEBPACK_IMPORTED_MODULE_15__["ContactUsMoleculeComponent"],
    _client_logo_molecule_client_logo_component__WEBPACK_IMPORTED_MODULE_16__["ClientLogoComponent"],
    _saned_section_molecule_saned_section_molecule_component__WEBPACK_IMPORTED_MODULE_17__["SanedSectionMoleculeComponent"],
    _register_molecule_register_molecule_component__WEBPACK_IMPORTED_MODULE_18__["RegisterMoleculeComponent"],
    _sign_up_molecule_sign_up_component__WEBPACK_IMPORTED_MODULE_19__["SignUpComponent"],
    _footer_molecule_footer_component__WEBPACK_IMPORTED_MODULE_20__["FooterComponent"],
    _login_form_molecule_login_form_molecule_component__WEBPACK_IMPORTED_MODULE_21__["LoginFormMoleculeComponent"],
    _cart_molecule_cart_molecule_component__WEBPACK_IMPORTED_MODULE_23__["CartMoleculeComponent"]
];
let MoleculesModule = class MoleculesModule {
};
MoleculesModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _services_services_module__WEBPACK_IMPORTED_MODULE_7__["ServicesModule"],
            _directives_directives_module__WEBPACK_IMPORTED_MODULE_8__["DirectivesModule"],
            _pipes_pipes_module__WEBPACK_IMPORTED_MODULE_9__["PipesModule"],
            _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_13__["NgbModule"],
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"],
            _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__["BrowserModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormsModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_5__["ReactiveFormsModule"],
            _material_module__WEBPACK_IMPORTED_MODULE_6__["MaterialModule"],
            _app_routing_module__WEBPACK_IMPORTED_MODULE_22__["AppRoutingModule"]
        ],
        exports: [...components],
        declarations: [...components],
        providers: [],
    })
], MoleculesModule);



/***/ }),

/***/ "./src/app/shared/molecules/register-molecule/register-molecule.component.scss":
/*!*************************************************************************************!*\
  !*** ./src/app/shared/molecules/register-molecule/register-molecule.component.scss ***!
  \*************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("/********************Custom Variables **************************/\n.text-white {\n  color: #fff;\n}\n.secondary-bg {\n  background: #000;\n}\n.primary-color {\n  color: #a7f108;\n}\n.no-margin {\n  margin: 0;\n}\n.no-padding {\n  padding: 0;\n}\n.margin-auto {\n  margin: 0 auto;\n}\n.font-bold {\n  font-weight: 800;\n}\n.font-semi-bold {\n  font-weight: 600;\n}\n.mb-30 {\n  margin-bottom: 30px;\n}\n.mb-50 {\n  margin-bottom: 50px;\n}\n.pr-20 {\n  padding-right: 20px;\n}\n.bg-white {\n  background: #fff;\n}\n.register-title {\n  display: flex;\n  flex-direction: column;\n  padding: 20px 0;\n  text-align: center;\n  color: currentcolor;\n  background-color: var(--cx-color-background);\n}\n.register-title a {\n  color: inherit;\n  padding: 5px;\n}\n.reg-form-wrapper {\n  box-shadow: 0 3px 36px rgba(0, 0, 0, 0.09);\n  padding: 40px;\n  margin: 40px auto;\n}\n.reg-form-wrapper form {\n  width: 300px;\n  margin: 0 auto;\n}\n.reg-form-wrapper form .mat-form-field {\n  width: 100%;\n}\n.reg-form-wrapper form .reg-btn {\n  background-color: #a7f108;\n  border-color: #a7f108;\n  width: 100%;\n  color: #ffffff;\n  font-size: 20px;\n  padding: 6px;\n}\n.reg-form-wrapper form .reg-btn:disabled {\n  opacity: 0.5;\n}\n.reg-form-wrapper form .btn-link {\n  font-weight: 400;\n  color: #a7f108;\n  font-size: 1.125rem;\n  color: #212738;\n  text-decoration: underline !important;\n  cursor: pointer;\n  display: inline-block;\n}\n@media only screen and (min-width: 600px) {\n  form {\n    width: 500px !important;\n    margin: 0 auto;\n  }\n\n  .reg-btn {\n    background-color: #a7f108;\n    border-color: #a7f108;\n    width: 100%;\n    color: #000000;\n    font-size: 20px;\n    padding: 6px;\n  }\n  .reg-btn:disabled {\n    opacity: 0.5;\n  }\n\n  .btn-link {\n    font-weight: 400;\n    color: #a7f108;\n    font-size: 1.125rem;\n    color: #212738;\n    text-decoration: underline !important;\n    cursor: pointer;\n    display: inline-block;\n  }\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi92YXIvd3d3L2h0bWwvanMtc3RvcmVmcm9udC9zcGFydGFjdXNzdG9yZS9zcmMvYXNzZXRzL2Nzcy92YXJpYWJsZS5zY3NzIiwic3JjL2FwcC9zaGFyZWQvbW9sZWN1bGVzL3JlZ2lzdGVyLW1vbGVjdWxlL3JlZ2lzdGVyLW1vbGVjdWxlLmNvbXBvbmVudC5zY3NzIiwiL3Zhci93d3cvaHRtbC9qcy1zdG9yZWZyb250L3NwYXJ0YWN1c3N0b3JlL3NyYy9hcHAvc2hhcmVkL21vbGVjdWxlcy9yZWdpc3Rlci1tb2xlY3VsZS9yZWdpc3Rlci1tb2xlY3VsZS5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxnRUFBQTtBQU1BO0VBQ0ksV0FBQTtBQ0pKO0FETUU7RUFDRSxnQkFBQTtBQ0hKO0FES0U7RUFDRSxjQUFBO0FDRko7QURJRTtFQUNFLFNBQUE7QUNESjtBREdFO0VBQ0UsVUFBQTtBQ0FKO0FERUU7RUFDRSxjQUFBO0FDQ0o7QURDRTtFQUNFLGdCQUFBO0FDRUo7QURBRTtFQUNFLGdCQUFBO0FDR0o7QURERTtFQUNFLG1CQUFBO0FDSUo7QURGRTtFQUNFLG1CQUFBO0FDS0o7QURIRTtFQUNFLG1CQUFBO0FDTUo7QURKRTtFQUNFLGdCQUFBO0FDT0o7QUM5Q0E7RUFDSSxhQUFBO0VBQ0Esc0JBQUE7RUFDQSxlQUFBO0VBQ0Esa0JBQUE7RUFDQSxtQkFBQTtFQUNBLDRDQUFBO0FEaURKO0FDaERJO0VBQ0ksY0FBQTtFQUNBLFlBQUE7QURrRFI7QUMvQ0E7RUFDUSwwQ0FBQTtFQUNBLGFBQUE7RUFDQSxpQkFBQTtBRGtEUjtBQ2pESTtFQUNJLFlBQUE7RUFDQSxjQUFBO0FEbURSO0FDbERRO0VBQ0ksV0FBQTtBRG9EWjtBQ2xETztFQUNDLHlCRnJCUTtFRXNCUixxQkZ0QlE7RUV1QlIsV0FBQTtFQUNBLGNGMUJBO0VFMkJBLGVBQUE7RUFDQSxZQUFBO0FEb0RSO0FDbkRRO0VBQ0ksWUFBQTtBRHFEWjtBQ2xEUTtFQUNJLGdCQUFBO0VBQ0EsY0ZqQ0k7RUVrQ0osbUJBQUE7RUFDQSxjQUFBO0VBQ0EscUNBQUE7RUFDQSxlQUFBO0VBQ0EscUJBQUE7QURvRFo7QUMvQ0E7RUFDSTtJQUNBLHVCQUFBO0lBQ0EsY0FBQTtFRGtERjs7RUNoREU7SUFDSSx5QkZqRFE7SUVrRFIscUJGbERRO0lFbURSLFdBQUE7SUFDQSxjRnJEQTtJRXNEQSxlQUFBO0lBQ0EsWUFBQTtFRG1ETjtFQ2xEVTtJQUNJLFlBQUE7RURvRGQ7O0VDakRFO0lBQ0ksZ0JBQUE7SUFDQSxjRjdEUTtJRThEUixtQkFBQTtJQUNBLGNBQUE7SUFDQSxxQ0FBQTtJQUNBLGVBQUE7SUFDQSxxQkFBQTtFRG9ETjtBQUNGIiwiZmlsZSI6InNyYy9hcHAvc2hhcmVkL21vbGVjdWxlcy9yZWdpc3Rlci1tb2xlY3VsZS9yZWdpc3Rlci1tb2xlY3VsZS5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi8qKioqKioqKioqKioqKioqKioqKkN1c3RvbSBWYXJpYWJsZXMgKioqKioqKioqKioqKioqKioqKioqKioqKiovXG4kd2hpdGU6ICNmZmZmZmY7XG4kYmxhY2s6ICMwMDAwMDA7XG4kcHJpbWFyeS1jb2xvcjogI2E3ZjEwODtcbiRib3JkZXItY29sb3I6ICByZ2JhKDI1NSwgMjU1LCAyNTUsIDAuNik7XG4kZm9ybS1iZzogI2Y3ZjdmNztcbi50ZXh0LXdoaXRlIHtcbiAgICBjb2xvcjogI2ZmZjtcbiAgfVxuICAuc2Vjb25kYXJ5LWJnIHtcbiAgICBiYWNrZ3JvdW5kOiAjMDAwO1xuICB9XG4gIC5wcmltYXJ5LWNvbG9yIHtcbiAgICBjb2xvcjogI2E3ZjEwODtcbiAgfVxuICAubm8tbWFyZ2luIHtcbiAgICBtYXJnaW46IDA7XG4gIH1cbiAgLm5vLXBhZGRpbmcge1xuICAgIHBhZGRpbmc6IDA7XG4gIH1cbiAgLm1hcmdpbi1hdXRvIHtcbiAgICBtYXJnaW46IDAgYXV0bztcbiAgfVxuICAuZm9udC1ib2xkIHtcbiAgICBmb250LXdlaWdodDogODAwO1xuICB9XG4gIC5mb250LXNlbWktYm9sZCB7XG4gICAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgfVxuICAubWItMzAge1xuICAgIG1hcmdpbi1ib3R0b206IDMwcHg7XG4gIH1cbiAgLm1iLTUwIHtcbiAgICBtYXJnaW4tYm90dG9tOiA1MHB4O1xuICB9XG4gIC5wci0yMCB7XG4gICAgcGFkZGluZy1yaWdodDogMjBweDtcbiAgfVxuICAuYmctd2hpdGUge1xuICAgIGJhY2tncm91bmQ6ICNmZmY7XG4gIH0iLCIvKioqKioqKioqKioqKioqKioqKipDdXN0b20gVmFyaWFibGVzICoqKioqKioqKioqKioqKioqKioqKioqKioqL1xuLnRleHQtd2hpdGUge1xuICBjb2xvcjogI2ZmZjtcbn1cblxuLnNlY29uZGFyeS1iZyB7XG4gIGJhY2tncm91bmQ6ICMwMDA7XG59XG5cbi5wcmltYXJ5LWNvbG9yIHtcbiAgY29sb3I6ICNhN2YxMDg7XG59XG5cbi5uby1tYXJnaW4ge1xuICBtYXJnaW46IDA7XG59XG5cbi5uby1wYWRkaW5nIHtcbiAgcGFkZGluZzogMDtcbn1cblxuLm1hcmdpbi1hdXRvIHtcbiAgbWFyZ2luOiAwIGF1dG87XG59XG5cbi5mb250LWJvbGQge1xuICBmb250LXdlaWdodDogODAwO1xufVxuXG4uZm9udC1zZW1pLWJvbGQge1xuICBmb250LXdlaWdodDogNjAwO1xufVxuXG4ubWItMzAge1xuICBtYXJnaW4tYm90dG9tOiAzMHB4O1xufVxuXG4ubWItNTAge1xuICBtYXJnaW4tYm90dG9tOiA1MHB4O1xufVxuXG4ucHItMjAge1xuICBwYWRkaW5nLXJpZ2h0OiAyMHB4O1xufVxuXG4uYmctd2hpdGUge1xuICBiYWNrZ3JvdW5kOiAjZmZmO1xufVxuXG4ucmVnaXN0ZXItdGl0bGUge1xuICBkaXNwbGF5OiBmbGV4O1xuICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICBwYWRkaW5nOiAyMHB4IDA7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgY29sb3I6IGN1cnJlbnRjb2xvcjtcbiAgYmFja2dyb3VuZC1jb2xvcjogdmFyKC0tY3gtY29sb3ItYmFja2dyb3VuZCk7XG59XG4ucmVnaXN0ZXItdGl0bGUgYSB7XG4gIGNvbG9yOiBpbmhlcml0O1xuICBwYWRkaW5nOiA1cHg7XG59XG5cbi5yZWctZm9ybS13cmFwcGVyIHtcbiAgYm94LXNoYWRvdzogMCAzcHggMzZweCByZ2JhKDAsIDAsIDAsIDAuMDkpO1xuICBwYWRkaW5nOiA0MHB4O1xuICBtYXJnaW46IDQwcHggYXV0bztcbn1cbi5yZWctZm9ybS13cmFwcGVyIGZvcm0ge1xuICB3aWR0aDogMzAwcHg7XG4gIG1hcmdpbjogMCBhdXRvO1xufVxuLnJlZy1mb3JtLXdyYXBwZXIgZm9ybSAubWF0LWZvcm0tZmllbGQge1xuICB3aWR0aDogMTAwJTtcbn1cbi5yZWctZm9ybS13cmFwcGVyIGZvcm0gLnJlZy1idG4ge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjYTdmMTA4O1xuICBib3JkZXItY29sb3I6ICNhN2YxMDg7XG4gIHdpZHRoOiAxMDAlO1xuICBjb2xvcjogI2ZmZmZmZjtcbiAgZm9udC1zaXplOiAyMHB4O1xuICBwYWRkaW5nOiA2cHg7XG59XG4ucmVnLWZvcm0td3JhcHBlciBmb3JtIC5yZWctYnRuOmRpc2FibGVkIHtcbiAgb3BhY2l0eTogMC41O1xufVxuLnJlZy1mb3JtLXdyYXBwZXIgZm9ybSAuYnRuLWxpbmsge1xuICBmb250LXdlaWdodDogNDAwO1xuICBjb2xvcjogI2E3ZjEwODtcbiAgZm9udC1zaXplOiAxLjEyNXJlbTtcbiAgY29sb3I6ICMyMTI3Mzg7XG4gIHRleHQtZGVjb3JhdGlvbjogdW5kZXJsaW5lICFpbXBvcnRhbnQ7XG4gIGN1cnNvcjogcG9pbnRlcjtcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xufVxuXG5AbWVkaWEgb25seSBzY3JlZW4gYW5kIChtaW4td2lkdGg6IDYwMHB4KSB7XG4gIGZvcm0ge1xuICAgIHdpZHRoOiA1MDBweCAhaW1wb3J0YW50O1xuICAgIG1hcmdpbjogMCBhdXRvO1xuICB9XG5cbiAgLnJlZy1idG4ge1xuICAgIGJhY2tncm91bmQtY29sb3I6ICNhN2YxMDg7XG4gICAgYm9yZGVyLWNvbG9yOiAjYTdmMTA4O1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIGNvbG9yOiAjMDAwMDAwO1xuICAgIGZvbnQtc2l6ZTogMjBweDtcbiAgICBwYWRkaW5nOiA2cHg7XG4gIH1cbiAgLnJlZy1idG46ZGlzYWJsZWQge1xuICAgIG9wYWNpdHk6IDAuNTtcbiAgfVxuXG4gIC5idG4tbGluayB7XG4gICAgZm9udC13ZWlnaHQ6IDQwMDtcbiAgICBjb2xvcjogI2E3ZjEwODtcbiAgICBmb250LXNpemU6IDEuMTI1cmVtO1xuICAgIGNvbG9yOiAjMjEyNzM4O1xuICAgIHRleHQtZGVjb3JhdGlvbjogdW5kZXJsaW5lICFpbXBvcnRhbnQ7XG4gICAgY3Vyc29yOiBwb2ludGVyO1xuICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgfVxufSIsIkBpbXBvcnQgXCIuLi8uLi8uLi8uLi9hc3NldHMvY3NzL2NvbnN0YW50cy5zY3NzXCI7XG4ucmVnaXN0ZXItdGl0bGV7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICAgIHBhZGRpbmc6IDIwcHggMDtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgY29sb3I6IGN1cnJlbnRjb2xvcjtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiB2YXIoLS1jeC1jb2xvci1iYWNrZ3JvdW5kKTtcbiAgICBhe1xuICAgICAgICBjb2xvcjogaW5oZXJpdDtcbiAgICAgICAgcGFkZGluZzogNXB4O1xuICAgIH1cbn1cbi5yZWctZm9ybS13cmFwcGVye1xuICAgICAgICBib3gtc2hhZG93OiAwIDNweCAzNnB4IHJnYmEoMCwgMCwgMCwgLjA5KTtcbiAgICAgICAgcGFkZGluZzogNDBweDtcbiAgICAgICAgbWFyZ2luOiA0MHB4IGF1dG87XG4gICAgZm9ybXtcbiAgICAgICAgd2lkdGg6IDMwMHB4O1xuICAgICAgICBtYXJnaW46IDAgYXV0bztcbiAgICAgICAgLm1hdC1mb3JtLWZpZWxke1xuICAgICAgICAgICAgd2lkdGg6IDEwMCU7XG4gICAgICAgIH1cbiAgICAgICAucmVnLWJ0bntcbiAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogJHByaW1hcnktY29sb3I7XG4gICAgICAgIGJvcmRlci1jb2xvcjogJHByaW1hcnktY29sb3I7XG4gICAgICAgIHdpZHRoOiAxMDAlO1xuICAgICAgICBjb2xvcjogJHdoaXRlO1xuICAgICAgICBmb250LXNpemU6IDIwcHg7XG4gICAgICAgIHBhZGRpbmc6IDZweDtcbiAgICAgICAgJjpkaXNhYmxlZHtcbiAgICAgICAgICAgIG9wYWNpdHk6IDAuNTtcbiAgICAgICAgfVxuICAgICAgICB9XG4gICAgICAgIC5idG4tbGlua3tcbiAgICAgICAgICAgIGZvbnQtd2VpZ2h0OiA0MDA7XG4gICAgICAgICAgICBjb2xvcjogJHByaW1hcnktY29sb3I7XG4gICAgICAgICAgICBmb250LXNpemU6IDEuMTI1cmVtO1xuICAgICAgICAgICAgY29sb3I6ICMyMTI3Mzg7XG4gICAgICAgICAgICB0ZXh0LWRlY29yYXRpb246IHVuZGVybGluZSAhaW1wb3J0YW50O1xuICAgICAgICAgICAgY3Vyc29yOiBwb2ludGVyO1xuICAgICAgICAgICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICAgIFxuICAgICAgICB9XG4gICAgfVxufVxuQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWluLXdpZHRoOiA2MDBweCkge1xuICAgIGZvcm17XG4gICAgd2lkdGg6IDUwMHB4ICFpbXBvcnRhbnQ7XG4gICAgbWFyZ2luOiAwIGF1dG87XG4gICAgfVxuICAgIC5yZWctYnRue1xuICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAkcHJpbWFyeS1jb2xvcjtcbiAgICAgICAgYm9yZGVyLWNvbG9yOiAkcHJpbWFyeS1jb2xvcjtcbiAgICAgICAgd2lkdGg6IDEwMCU7XG4gICAgICAgIGNvbG9yOiAkYmxhY2s7XG4gICAgICAgIGZvbnQtc2l6ZTogMjBweDtcbiAgICAgICAgcGFkZGluZzogNnB4O1xuICAgICAgICAgICAgJjpkaXNhYmxlZHtcbiAgICAgICAgICAgICAgICBvcGFjaXR5OiAwLjU7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAuYnRuLWxpbmt7XG4gICAgICAgIGZvbnQtd2VpZ2h0OiA0MDA7XG4gICAgICAgIGNvbG9yOiAkcHJpbWFyeS1jb2xvcjtcbiAgICAgICAgZm9udC1zaXplOiAxLjEyNXJlbTtcbiAgICAgICAgY29sb3I6ICMyMTI3Mzg7XG4gICAgICAgIHRleHQtZGVjb3JhdGlvbjogdW5kZXJsaW5lICFpbXBvcnRhbnQ7XG4gICAgICAgIGN1cnNvcjogcG9pbnRlcjtcbiAgICAgICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICAgIH1cbn0iXX0= */");

/***/ }),

/***/ "./src/app/shared/molecules/register-molecule/register-molecule.component.ts":
/*!***********************************************************************************!*\
  !*** ./src/app/shared/molecules/register-molecule/register-molecule.component.ts ***!
  \***********************************************************************************/
/*! exports provided: RegisterMoleculeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RegisterMoleculeComponent", function() { return RegisterMoleculeComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _services_register_user_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../services/register-user.service */ "./src/app/services/register-user.service.ts");
/* harmony import */ var _services_common_utility_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../services/common-utility-service */ "./src/app/services/common-utility-service.ts");
/* harmony import */ var _angular_material_snack_bar__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/material/snack-bar */ "./node_modules/@angular/material/esm2015/snack-bar.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");







let RegisterMoleculeComponent = class RegisterMoleculeComponent {
    constructor(fb, regUserService, router, matSnack, utilService) {
        this.fb = fb;
        this.regUserService = regUserService;
        this.router = router;
        this.matSnack = matSnack;
        this.utilService = utilService;
        this.validationMessages = {
            name: [{ type: 'required', message: 'Full name is required' }],
            email: [
                { type: 'required', message: 'Email is required' },
                { type: 'email', message: 'Enter a valid email' }
            ],
            mobile: [
                { type: 'required', message: 'Mobile number is required' },
                //  { type: 'minlength', message: 'Minimum 10 digits' },
                { type: 'pattern', message: 'Please enter a valid mobile number' }
            ],
            company: [{ type: 'required', message: 'Company name is required' }],
            size: [{ type: 'required', message: 'Please select any option' }],
            commercial: [{ type: 'required', message: 'Commercial registration is required' }],
        };
        this.errMsg = 'Sorry ! Please try again later';
        this.succMsg = 'User Registerd successfully !';
    }
    ngOnInit() {
        const MOBILE_PATTERN = '^((\\+91-?)|0)?[0-9]{10}$';
        this.registerForm = this.fb.group({
            name: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
            email: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([
                    _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required,
                    _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].email
                ])],
            mobile: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([
                    _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required,
                    _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].pattern(MOBILE_PATTERN)
                ])],
            company: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
            size: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
            commercial: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]
        });
    }
    registerUser(user) {
        const { name, email, mobile, company, size, commercial } = user;
        if (name && email && mobile && company && size && commercial) {
            this.utilService.getAuth().subscribe((res) => {
                if (res && res.access_token) {
                    this.regUserService.registerUserDetails(res.access_token, user).subscribe((result) => {
                        console.log('User registered successfully!', result);
                        this.matSnack.open(this.succMsg, 'close', {
                            duration: 5000,
                        });
                        this.router.navigate(['/']);
                    }, (err) => {
                        this.matSnack.open(this.errMsg, 'close', {
                            duration: 5000,
                        });
                    });
                }
            }, (err) => {
                console.log('O Auth API failed', err);
                this.matSnack.open(this.errMsg, 'close', {
                    duration: 5000,
                });
            });
        }
    }
};
RegisterMoleculeComponent.ctorParameters = () => [
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"] },
    { type: _services_register_user_service__WEBPACK_IMPORTED_MODULE_3__["RegisterUserService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_6__["Router"] },
    { type: _angular_material_snack_bar__WEBPACK_IMPORTED_MODULE_5__["MatSnackBar"] },
    { type: _services_common_utility_service__WEBPACK_IMPORTED_MODULE_4__["CommonUtilityService"] }
];
RegisterMoleculeComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-register-molecule',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./register-molecule.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/molecules/register-molecule/register-molecule.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./register-molecule.component.scss */ "./src/app/shared/molecules/register-molecule/register-molecule.component.scss")).default]
    })
], RegisterMoleculeComponent);



/***/ }),

/***/ "./src/app/shared/molecules/saned-section-molecule/saned-section-molecule.component.scss":
/*!***********************************************************************************************!*\
  !*** ./src/app/shared/molecules/saned-section-molecule/saned-section-molecule.component.scss ***!
  \***********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("/********************Custom Variables **************************/\n.text-white {\n  color: #fff;\n}\n.secondary-bg {\n  background: #000;\n}\n.primary-color {\n  color: #a7f108;\n}\n.no-margin {\n  margin: 0;\n}\n.no-padding {\n  padding: 0;\n}\n.margin-auto {\n  margin: 0 auto;\n}\n.font-bold {\n  font-weight: 800;\n}\n.font-semi-bold {\n  font-weight: 600;\n}\n.mb-30 {\n  margin-bottom: 30px;\n}\n.mb-50 {\n  margin-bottom: 50px;\n}\n.pr-20 {\n  padding-right: 20px;\n}\n.bg-white {\n  background: #fff;\n}\n.saned-section-wrapper {\n  background-color: #000000;\n  padding-bottom: 0;\n}\n.saned-section-wrapper .title {\n  position: relative;\n  font-size: 34px;\n  font-family: \"Roboto-Medium\";\n  margin: 0 auto;\n  color: #ffffff;\n}\n.saned-section-wrapper .title::after {\n  content: \"\";\n  display: block;\n  margin: 0 auto;\n  width: 8%;\n  padding-top: 15px;\n  border-bottom: 4px solid #a7f108;\n}\n.saned-section-wrapper .domains {\n  display: inline-block;\n  width: 100%;\n  padding: 0;\n}\n.saned-section-wrapper .domains li {\n  float: left;\n}\n.saned-section-wrapper .domains li a {\n  display: inline-block;\n}\n.saned-section-wrapper .domains li a img {\n  width: 100px;\n  height: 100px;\n}\n.saned-section-wrapper .domains li a img:hover {\n  -sand-transform: scale(1.1);\n  -ms-transform: scale(1.1);\n  -moz-transform: scale(1.1);\n  -o-transform: scale(1.1);\n  -webkit-transform: scale(1.1);\n  transition: 0.6s;\n}\n.saned-section-wrapper .domains li a p {\n  margin: 30px 0;\n  color: #ffffff;\n  font-size: 20px;\n  font-weight: 600;\n  letter-spacing: 0.5px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi92YXIvd3d3L2h0bWwvanMtc3RvcmVmcm9udC9zcGFydGFjdXNzdG9yZS9zcmMvYXNzZXRzL2Nzcy92YXJpYWJsZS5zY3NzIiwic3JjL2FwcC9zaGFyZWQvbW9sZWN1bGVzL3NhbmVkLXNlY3Rpb24tbW9sZWN1bGUvc2FuZWQtc2VjdGlvbi1tb2xlY3VsZS5jb21wb25lbnQuc2NzcyIsIi92YXIvd3d3L2h0bWwvanMtc3RvcmVmcm9udC9zcGFydGFjdXNzdG9yZS9zcmMvYXBwL3NoYXJlZC9tb2xlY3VsZXMvc2FuZWQtc2VjdGlvbi1tb2xlY3VsZS9zYW5lZC1zZWN0aW9uLW1vbGVjdWxlLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLGdFQUFBO0FBTUE7RUFDSSxXQUFBO0FDSko7QURNRTtFQUNFLGdCQUFBO0FDSEo7QURLRTtFQUNFLGNBQUE7QUNGSjtBRElFO0VBQ0UsU0FBQTtBQ0RKO0FER0U7RUFDRSxVQUFBO0FDQUo7QURFRTtFQUNFLGNBQUE7QUNDSjtBRENFO0VBQ0UsZ0JBQUE7QUNFSjtBREFFO0VBQ0UsZ0JBQUE7QUNHSjtBRERFO0VBQ0UsbUJBQUE7QUNJSjtBREZFO0VBQ0UsbUJBQUE7QUNLSjtBREhFO0VBQ0UsbUJBQUE7QUNNSjtBREpFO0VBQ0UsZ0JBQUE7QUNPSjtBQzlDQTtFQUNFLHlCQUFBO0VBQ0EsaUJBQUE7QURpREY7QUNoREU7RUFDRSxrQkFBQTtFQUNBLGVBQUE7RUFDQSw0QkFBQTtFQUNBLGNBQUE7RUFDQSxjRlJJO0FDMERSO0FDakRJO0VBQ0ksV0FBQTtFQUNBLGNBQUE7RUFDQSxjQUFBO0VBQ0EsU0FBQTtFQUNBLGlCQUFBO0VBQ0EsZ0NBQUE7QURtRFI7QUNoREU7RUFDRSxxQkFBQTtFQUNBLFdBQUE7RUFDQSxVQUFBO0FEa0RKO0FDakRRO0VBQ0ksV0FBQTtBRG1EWjtBQ2xEWTtFQUNJLHFCQUFBO0FEb0RoQjtBQ25EZ0I7RUFDSSxZQUFBO0VBQ0EsYUFBQTtBRHFEcEI7QUNwRG9CO0VBQ0ksMkJBQUE7RUFDQSx5QkFBQTtFQUNBLDBCQUFBO0VBQ0Esd0JBQUE7RUFDQSw2QkFBQTtFQUlBLGdCQUFBO0FEc0R4QjtBQ25EZ0I7RUFDSSxjQUFBO0VBQ0EsY0YzQ1o7RUU0Q1ksZUFBQTtFQUNBLGdCQUFBO0VBQ0EscUJBQUE7QURxRHBCIiwiZmlsZSI6InNyYy9hcHAvc2hhcmVkL21vbGVjdWxlcy9zYW5lZC1zZWN0aW9uLW1vbGVjdWxlL3NhbmVkLXNlY3Rpb24tbW9sZWN1bGUuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIvKioqKioqKioqKioqKioqKioqKipDdXN0b20gVmFyaWFibGVzICoqKioqKioqKioqKioqKioqKioqKioqKioqL1xuJHdoaXRlOiAjZmZmZmZmO1xuJGJsYWNrOiAjMDAwMDAwO1xuJHByaW1hcnktY29sb3I6ICNhN2YxMDg7XG4kYm9yZGVyLWNvbG9yOiAgcmdiYSgyNTUsIDI1NSwgMjU1LCAwLjYpO1xuJGZvcm0tYmc6ICNmN2Y3Zjc7XG4udGV4dC13aGl0ZSB7XG4gICAgY29sb3I6ICNmZmY7XG4gIH1cbiAgLnNlY29uZGFyeS1iZyB7XG4gICAgYmFja2dyb3VuZDogIzAwMDtcbiAgfVxuICAucHJpbWFyeS1jb2xvciB7XG4gICAgY29sb3I6ICNhN2YxMDg7XG4gIH1cbiAgLm5vLW1hcmdpbiB7XG4gICAgbWFyZ2luOiAwO1xuICB9XG4gIC5uby1wYWRkaW5nIHtcbiAgICBwYWRkaW5nOiAwO1xuICB9XG4gIC5tYXJnaW4tYXV0byB7XG4gICAgbWFyZ2luOiAwIGF1dG87XG4gIH1cbiAgLmZvbnQtYm9sZCB7XG4gICAgZm9udC13ZWlnaHQ6IDgwMDtcbiAgfVxuICAuZm9udC1zZW1pLWJvbGQge1xuICAgIGZvbnQtd2VpZ2h0OiA2MDA7XG4gIH1cbiAgLm1iLTMwIHtcbiAgICBtYXJnaW4tYm90dG9tOiAzMHB4O1xuICB9XG4gIC5tYi01MCB7XG4gICAgbWFyZ2luLWJvdHRvbTogNTBweDtcbiAgfVxuICAucHItMjAge1xuICAgIHBhZGRpbmctcmlnaHQ6IDIwcHg7XG4gIH1cbiAgLmJnLXdoaXRlIHtcbiAgICBiYWNrZ3JvdW5kOiAjZmZmO1xuICB9IiwiLyoqKioqKioqKioqKioqKioqKioqQ3VzdG9tIFZhcmlhYmxlcyAqKioqKioqKioqKioqKioqKioqKioqKioqKi9cbi50ZXh0LXdoaXRlIHtcbiAgY29sb3I6ICNmZmY7XG59XG5cbi5zZWNvbmRhcnktYmcge1xuICBiYWNrZ3JvdW5kOiAjMDAwO1xufVxuXG4ucHJpbWFyeS1jb2xvciB7XG4gIGNvbG9yOiAjYTdmMTA4O1xufVxuXG4ubm8tbWFyZ2luIHtcbiAgbWFyZ2luOiAwO1xufVxuXG4ubm8tcGFkZGluZyB7XG4gIHBhZGRpbmc6IDA7XG59XG5cbi5tYXJnaW4tYXV0byB7XG4gIG1hcmdpbjogMCBhdXRvO1xufVxuXG4uZm9udC1ib2xkIHtcbiAgZm9udC13ZWlnaHQ6IDgwMDtcbn1cblxuLmZvbnQtc2VtaS1ib2xkIHtcbiAgZm9udC13ZWlnaHQ6IDYwMDtcbn1cblxuLm1iLTMwIHtcbiAgbWFyZ2luLWJvdHRvbTogMzBweDtcbn1cblxuLm1iLTUwIHtcbiAgbWFyZ2luLWJvdHRvbTogNTBweDtcbn1cblxuLnByLTIwIHtcbiAgcGFkZGluZy1yaWdodDogMjBweDtcbn1cblxuLmJnLXdoaXRlIHtcbiAgYmFja2dyb3VuZDogI2ZmZjtcbn1cblxuLnNhbmVkLXNlY3Rpb24td3JhcHBlciB7XG4gIGJhY2tncm91bmQtY29sb3I6ICMwMDAwMDA7XG4gIHBhZGRpbmctYm90dG9tOiAwO1xufVxuLnNhbmVkLXNlY3Rpb24td3JhcHBlciAudGl0bGUge1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIGZvbnQtc2l6ZTogMzRweDtcbiAgZm9udC1mYW1pbHk6IFwiUm9ib3RvLU1lZGl1bVwiO1xuICBtYXJnaW46IDAgYXV0bztcbiAgY29sb3I6ICNmZmZmZmY7XG59XG4uc2FuZWQtc2VjdGlvbi13cmFwcGVyIC50aXRsZTo6YWZ0ZXIge1xuICBjb250ZW50OiBcIlwiO1xuICBkaXNwbGF5OiBibG9jaztcbiAgbWFyZ2luOiAwIGF1dG87XG4gIHdpZHRoOiA4JTtcbiAgcGFkZGluZy10b3A6IDE1cHg7XG4gIGJvcmRlci1ib3R0b206IDRweCBzb2xpZCAjYTdmMTA4O1xufVxuLnNhbmVkLXNlY3Rpb24td3JhcHBlciAuZG9tYWlucyB7XG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgd2lkdGg6IDEwMCU7XG4gIHBhZGRpbmc6IDA7XG59XG4uc2FuZWQtc2VjdGlvbi13cmFwcGVyIC5kb21haW5zIGxpIHtcbiAgZmxvYXQ6IGxlZnQ7XG59XG4uc2FuZWQtc2VjdGlvbi13cmFwcGVyIC5kb21haW5zIGxpIGEge1xuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG59XG4uc2FuZWQtc2VjdGlvbi13cmFwcGVyIC5kb21haW5zIGxpIGEgaW1nIHtcbiAgd2lkdGg6IDEwMHB4O1xuICBoZWlnaHQ6IDEwMHB4O1xufVxuLnNhbmVkLXNlY3Rpb24td3JhcHBlciAuZG9tYWlucyBsaSBhIGltZzpob3ZlciB7XG4gIC1zYW5kLXRyYW5zZm9ybTogc2NhbGUoMS4xKTtcbiAgLW1zLXRyYW5zZm9ybTogc2NhbGUoMS4xKTtcbiAgLW1vei10cmFuc2Zvcm06IHNjYWxlKDEuMSk7XG4gIC1vLXRyYW5zZm9ybTogc2NhbGUoMS4xKTtcbiAgLXdlYmtpdC10cmFuc2Zvcm06IHNjYWxlKDEuMSk7XG4gIC1tb3otdHJhbnNpdGlvbjogMC42cztcbiAgLW8tdHJhbnNpdGlvbjogMC42cztcbiAgLXdlYmtpdC10cmFuc2l0aW9uOiAwLjZzO1xuICB0cmFuc2l0aW9uOiAwLjZzO1xufVxuLnNhbmVkLXNlY3Rpb24td3JhcHBlciAuZG9tYWlucyBsaSBhIHAge1xuICBtYXJnaW46IDMwcHggMDtcbiAgY29sb3I6ICNmZmZmZmY7XG4gIGZvbnQtc2l6ZTogMjBweDtcbiAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgbGV0dGVyLXNwYWNpbmc6IDAuNXB4O1xufSIsIkBpbXBvcnQgXCIuLi8uLi8uLi8uLi9hc3NldHMvY3NzL2NvbnN0YW50cy5zY3NzXCI7XG4uc2FuZWQtc2VjdGlvbi13cmFwcGVye1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAkYmxhY2s7XG4gIHBhZGRpbmctYm90dG9tOiAwO1xuICAudGl0bGUge1xuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgICBmb250LXNpemU6IDM0cHg7XG4gICAgZm9udC1mYW1pbHk6ICdSb2JvdG8tTWVkaXVtJztcbiAgICBtYXJnaW46IDAgYXV0bztcbiAgICBjb2xvcjogJHdoaXRlO1xuICAgICY6OmFmdGVye1xuICAgICAgICBjb250ZW50OiBcIlwiO1xuICAgICAgICBkaXNwbGF5OiBibG9jaztcbiAgICAgICAgbWFyZ2luOiAwIGF1dG87XG4gICAgICAgIHdpZHRoOiA4JTtcbiAgICAgICAgcGFkZGluZy10b3A6IDE1cHg7XG4gICAgICAgIGJvcmRlci1ib3R0b206IDRweCBzb2xpZCAkcHJpbWFyeS1jb2xvcjtcbiAgICB9XG4gIH1cbiAgLmRvbWFpbnN7XG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICAgIHdpZHRoOjEwMCU7XG4gICAgcGFkZGluZzogMDtcbiAgICAgICAgbGl7XG4gICAgICAgICAgICBmbG9hdDpsZWZ0O1xuICAgICAgICAgICAgYSB7XG4gICAgICAgICAgICAgICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICAgICAgICAgICAgICAgIGltZ3tcbiAgICAgICAgICAgICAgICAgICAgd2lkdGg6MTAwcHg7XG4gICAgICAgICAgICAgICAgICAgIGhlaWdodDoxMDBweDtcbiAgICAgICAgICAgICAgICAgICAgJjpob3ZlcntcbiAgICAgICAgICAgICAgICAgICAgICAgIC1zYW5kLXRyYW5zZm9ybTogc2NhbGUoMS4xKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIC1tcy10cmFuc2Zvcm06IHNjYWxlKDEuMSk7XG4gICAgICAgICAgICAgICAgICAgICAgICAtbW96LXRyYW5zZm9ybTogc2NhbGUoMS4xKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIC1vLXRyYW5zZm9ybTogc2NhbGUoMS4xKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIC13ZWJraXQtdHJhbnNmb3JtOiBzY2FsZSgxLjEpO1xuICAgICAgICAgICAgICAgICAgICAgICAgLW1vei10cmFuc2l0aW9uOiAuNnM7XG4gICAgICAgICAgICAgICAgICAgICAgICAtby10cmFuc2l0aW9uOiAuNnM7XG4gICAgICAgICAgICAgICAgICAgICAgICAtd2Via2l0LXRyYW5zaXRpb246IC42cztcbiAgICAgICAgICAgICAgICAgICAgICAgIHRyYW5zaXRpb246IC42cztcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICBwe1xuICAgICAgICAgICAgICAgICAgICBtYXJnaW46MzBweCAwO1xuICAgICAgICAgICAgICAgICAgICBjb2xvcjogJHdoaXRlO1xuICAgICAgICAgICAgICAgICAgICBmb250LXNpemU6IDIwcHg7XG4gICAgICAgICAgICAgICAgICAgIGZvbnQtd2VpZ2h0OiA2MDA7XG4gICAgICAgICAgICAgICAgICAgIGxldHRlci1zcGFjaW5nOiAwLjVweDtcbiAgICAgICAgICAgICAgICB9fVxuICAgICAgICAgIFxuICAgICAgICB9XG4gICAgfVxufSJdfQ== */");

/***/ }),

/***/ "./src/app/shared/molecules/saned-section-molecule/saned-section-molecule.component.ts":
/*!*********************************************************************************************!*\
  !*** ./src/app/shared/molecules/saned-section-molecule/saned-section-molecule.component.ts ***!
  \*********************************************************************************************/
/*! exports provided: SanedSectionMoleculeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SanedSectionMoleculeComponent", function() { return SanedSectionMoleculeComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../environments/environment */ "./src/environments/environment.ts");




let SanedSectionMoleculeComponent = class SanedSectionMoleculeComponent {
    constructor(route) {
        this.route = route;
        this.hostName = _environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].hostName;
    }
    ngOnInit() {
        this.route.data.subscribe((data) => {
            const response = data.homeData;
            if (response &&
                response.contentSlots &&
                response.contentSlots.contentSlot) {
                for (const content of response.contentSlots.contentSlot) {
                    if (content.slotId === 'SanedSolutionSlot') {
                        this.images = content.components.component;
                    }
                }
            }
        });
    }
};
SanedSectionMoleculeComponent.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"] }
];
SanedSectionMoleculeComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-saned-section-molecule',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./saned-section-molecule.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/molecules/saned-section-molecule/saned-section-molecule.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./saned-section-molecule.component.scss */ "./src/app/shared/molecules/saned-section-molecule/saned-section-molecule.component.scss")).default]
    })
], SanedSectionMoleculeComponent);



/***/ }),

/***/ "./src/app/shared/molecules/sign-up-molecule/sign-up.component.scss":
/*!**************************************************************************!*\
  !*** ./src/app/shared/molecules/sign-up-molecule/sign-up.component.scss ***!
  \**************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("/********************Custom Variables **************************/\n.text-white {\n  color: #fff;\n}\n.secondary-bg {\n  background: #000;\n}\n.primary-color {\n  color: #a7f108;\n}\n.no-margin {\n  margin: 0;\n}\n.no-padding {\n  padding: 0;\n}\n.margin-auto {\n  margin: 0 auto;\n}\n.font-bold {\n  font-weight: 800;\n}\n.font-semi-bold {\n  font-weight: 600;\n}\n.mb-30 {\n  margin-bottom: 30px;\n}\n.mb-50 {\n  margin-bottom: 50px;\n}\n.pr-20 {\n  padding-right: 20px;\n}\n.bg-white {\n  background: #fff;\n}\n.news-letter-section {\n  background: url('slideimage.jpg');\n  width: 100%;\n  padding: 5% 10%;\n  text-align: center;\n}\n.news-letter-section .title {\n  font-size: 34px;\n  font-family: \"Roboto-Medium\";\n  margin: 0 auto;\n  color: #ffffff;\n}\n.news-letter-section .title::after {\n  content: \"\";\n  display: block;\n  margin: 0 auto;\n  width: 8%;\n  padding-top: 15px;\n  border-bottom: 4px solid #a7f108;\n}\n.sign-up-input-field input {\n  padding: 0.375rem 0.75rem;\n  border-radius: 5px 0px 0px 5px;\n  border: 2px solid #ffffff;\n  width: 50%;\n}\n.sign-up-input-field .signup-btn {\n  color: #000000;\n  padding: 0.375rem 0.75rem;\n  background: #a7f108;\n  border: 2px solid #a7f108;\n  border-radius: 0 5px 5px 0;\n  font-weight: bold;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi92YXIvd3d3L2h0bWwvanMtc3RvcmVmcm9udC9zcGFydGFjdXNzdG9yZS9zcmMvYXNzZXRzL2Nzcy92YXJpYWJsZS5zY3NzIiwic3JjL2FwcC9zaGFyZWQvbW9sZWN1bGVzL3NpZ24tdXAtbW9sZWN1bGUvc2lnbi11cC5jb21wb25lbnQuc2NzcyIsIi92YXIvd3d3L2h0bWwvanMtc3RvcmVmcm9udC9zcGFydGFjdXNzdG9yZS9zcmMvYXBwL3NoYXJlZC9tb2xlY3VsZXMvc2lnbi11cC1tb2xlY3VsZS9zaWduLXVwLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLGdFQUFBO0FBTUE7RUFDSSxXQUFBO0FDSko7QURNRTtFQUNFLGdCQUFBO0FDSEo7QURLRTtFQUNFLGNBQUE7QUNGSjtBRElFO0VBQ0UsU0FBQTtBQ0RKO0FER0U7RUFDRSxVQUFBO0FDQUo7QURFRTtFQUNFLGNBQUE7QUNDSjtBRENFO0VBQ0UsZ0JBQUE7QUNFSjtBREFFO0VBQ0UsZ0JBQUE7QUNHSjtBRERFO0VBQ0UsbUJBQUE7QUNJSjtBREZFO0VBQ0UsbUJBQUE7QUNLSjtBREhFO0VBQ0UsbUJBQUE7QUNNSjtBREpFO0VBQ0UsZ0JBQUE7QUNPSjtBQzlDQTtFQUNFLGlDQUFBO0VBQ0EsV0FBQTtFQUNBLGVBQUE7RUFDQSxrQkFBQTtBRGlERjtBQ2hERTtFQUNFLGVBQUE7RUFDQSw0QkFBQTtFQUNBLGNBQUE7RUFDQSxjRlRJO0FDMkRSO0FDakRJO0VBQ0UsV0FBQTtFQUNBLGNBQUE7RUFDQSxjQUFBO0VBQ0EsU0FBQTtFQUNBLGlCQUFBO0VBQ0EsZ0NBQUE7QURtRE47QUM3Q0U7RUFDRSx5QkFBQTtFQUNBLDhCQUFBO0VBQ0EseUJBQUE7RUFDQSxVQUFBO0FEZ0RKO0FDOUNFO0VBQ0UsY0Y1Qkk7RUU2QkoseUJBQUE7RUFDQSxtQkY3Qlk7RUU4QloseUJBQUE7RUFDQSwwQkFBQTtFQUNBLGlCQUFBO0FEZ0RKIiwiZmlsZSI6InNyYy9hcHAvc2hhcmVkL21vbGVjdWxlcy9zaWduLXVwLW1vbGVjdWxlL3NpZ24tdXAuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIvKioqKioqKioqKioqKioqKioqKipDdXN0b20gVmFyaWFibGVzICoqKioqKioqKioqKioqKioqKioqKioqKioqL1xuJHdoaXRlOiAjZmZmZmZmO1xuJGJsYWNrOiAjMDAwMDAwO1xuJHByaW1hcnktY29sb3I6ICNhN2YxMDg7XG4kYm9yZGVyLWNvbG9yOiAgcmdiYSgyNTUsIDI1NSwgMjU1LCAwLjYpO1xuJGZvcm0tYmc6ICNmN2Y3Zjc7XG4udGV4dC13aGl0ZSB7XG4gICAgY29sb3I6ICNmZmY7XG4gIH1cbiAgLnNlY29uZGFyeS1iZyB7XG4gICAgYmFja2dyb3VuZDogIzAwMDtcbiAgfVxuICAucHJpbWFyeS1jb2xvciB7XG4gICAgY29sb3I6ICNhN2YxMDg7XG4gIH1cbiAgLm5vLW1hcmdpbiB7XG4gICAgbWFyZ2luOiAwO1xuICB9XG4gIC5uby1wYWRkaW5nIHtcbiAgICBwYWRkaW5nOiAwO1xuICB9XG4gIC5tYXJnaW4tYXV0byB7XG4gICAgbWFyZ2luOiAwIGF1dG87XG4gIH1cbiAgLmZvbnQtYm9sZCB7XG4gICAgZm9udC13ZWlnaHQ6IDgwMDtcbiAgfVxuICAuZm9udC1zZW1pLWJvbGQge1xuICAgIGZvbnQtd2VpZ2h0OiA2MDA7XG4gIH1cbiAgLm1iLTMwIHtcbiAgICBtYXJnaW4tYm90dG9tOiAzMHB4O1xuICB9XG4gIC5tYi01MCB7XG4gICAgbWFyZ2luLWJvdHRvbTogNTBweDtcbiAgfVxuICAucHItMjAge1xuICAgIHBhZGRpbmctcmlnaHQ6IDIwcHg7XG4gIH1cbiAgLmJnLXdoaXRlIHtcbiAgICBiYWNrZ3JvdW5kOiAjZmZmO1xuICB9IiwiLyoqKioqKioqKioqKioqKioqKioqQ3VzdG9tIFZhcmlhYmxlcyAqKioqKioqKioqKioqKioqKioqKioqKioqKi9cbi50ZXh0LXdoaXRlIHtcbiAgY29sb3I6ICNmZmY7XG59XG5cbi5zZWNvbmRhcnktYmcge1xuICBiYWNrZ3JvdW5kOiAjMDAwO1xufVxuXG4ucHJpbWFyeS1jb2xvciB7XG4gIGNvbG9yOiAjYTdmMTA4O1xufVxuXG4ubm8tbWFyZ2luIHtcbiAgbWFyZ2luOiAwO1xufVxuXG4ubm8tcGFkZGluZyB7XG4gIHBhZGRpbmc6IDA7XG59XG5cbi5tYXJnaW4tYXV0byB7XG4gIG1hcmdpbjogMCBhdXRvO1xufVxuXG4uZm9udC1ib2xkIHtcbiAgZm9udC13ZWlnaHQ6IDgwMDtcbn1cblxuLmZvbnQtc2VtaS1ib2xkIHtcbiAgZm9udC13ZWlnaHQ6IDYwMDtcbn1cblxuLm1iLTMwIHtcbiAgbWFyZ2luLWJvdHRvbTogMzBweDtcbn1cblxuLm1iLTUwIHtcbiAgbWFyZ2luLWJvdHRvbTogNTBweDtcbn1cblxuLnByLTIwIHtcbiAgcGFkZGluZy1yaWdodDogMjBweDtcbn1cblxuLmJnLXdoaXRlIHtcbiAgYmFja2dyb3VuZDogI2ZmZjtcbn1cblxuLm5ld3MtbGV0dGVyLXNlY3Rpb24ge1xuICBiYWNrZ3JvdW5kOiB1cmwoXCIuLi8uLi8uLi8uLi9hc3NldHMvaW1hZ2VzL3NsaWRlaW1hZ2UuanBnXCIpO1xuICB3aWR0aDogMTAwJTtcbiAgcGFkZGluZzogNSUgMTAlO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG4ubmV3cy1sZXR0ZXItc2VjdGlvbiAudGl0bGUge1xuICBmb250LXNpemU6IDM0cHg7XG4gIGZvbnQtZmFtaWx5OiBcIlJvYm90by1NZWRpdW1cIjtcbiAgbWFyZ2luOiAwIGF1dG87XG4gIGNvbG9yOiAjZmZmZmZmO1xufVxuLm5ld3MtbGV0dGVyLXNlY3Rpb24gLnRpdGxlOjphZnRlciB7XG4gIGNvbnRlbnQ6IFwiXCI7XG4gIGRpc3BsYXk6IGJsb2NrO1xuICBtYXJnaW46IDAgYXV0bztcbiAgd2lkdGg6IDglO1xuICBwYWRkaW5nLXRvcDogMTVweDtcbiAgYm9yZGVyLWJvdHRvbTogNHB4IHNvbGlkICNhN2YxMDg7XG59XG5cbi5zaWduLXVwLWlucHV0LWZpZWxkIGlucHV0IHtcbiAgcGFkZGluZzogMC4zNzVyZW0gMC43NXJlbTtcbiAgYm9yZGVyLXJhZGl1czogNXB4IDBweCAwcHggNXB4O1xuICBib3JkZXI6IDJweCBzb2xpZCAjZmZmZmZmO1xuICB3aWR0aDogNTAlO1xufVxuLnNpZ24tdXAtaW5wdXQtZmllbGQgLnNpZ251cC1idG4ge1xuICBjb2xvcjogIzAwMDAwMDtcbiAgcGFkZGluZzogMC4zNzVyZW0gMC43NXJlbTtcbiAgYmFja2dyb3VuZDogI2E3ZjEwODtcbiAgYm9yZGVyOiAycHggc29saWQgI2E3ZjEwODtcbiAgYm9yZGVyLXJhZGl1czogMCA1cHggNXB4IDA7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xufSIsIkBpbXBvcnQgJy4uLy4uLy4uLy4uL2Fzc2V0cy9jc3MvY29uc3RhbnRzLnNjc3MnO1xuLm5ld3MtbGV0dGVyLXNlY3Rpb24ge1xuICBiYWNrZ3JvdW5kOiB1cmwoJy4uLy4uLy4uLy4uL2Fzc2V0cy9pbWFnZXMvc2xpZGVpbWFnZS5qcGcnKTtcbiAgd2lkdGg6IDEwMCU7XG4gIHBhZGRpbmc6IDUlIDEwJTtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAudGl0bGUge1xuICAgIGZvbnQtc2l6ZTogMzRweDtcbiAgICBmb250LWZhbWlseTogJ1JvYm90by1NZWRpdW0nO1xuICAgIG1hcmdpbjogMCBhdXRvO1xuICAgIGNvbG9yOiR3aGl0ZTtcbiAgICAmOjphZnRlciB7XG4gICAgICBjb250ZW50OiAnJztcbiAgICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgICAgbWFyZ2luOiAwIGF1dG87XG4gICAgICB3aWR0aDogOCU7XG4gICAgICBwYWRkaW5nLXRvcDogMTVweDtcbiAgICAgIGJvcmRlci1ib3R0b206IDRweCBzb2xpZCAkcHJpbWFyeS1jb2xvcjtcbiAgICB9XG4gIH1cbn1cblxuLnNpZ24tdXAtaW5wdXQtZmllbGR7XG4gIGlucHV0e1xuICAgIHBhZGRpbmc6IDAuMzc1cmVtIDAuNzVyZW07XG4gICAgYm9yZGVyLXJhZGl1czogNXB4IDBweCAwcHggNXB4O1xuICAgIGJvcmRlcjogMnB4IHNvbGlkICR3aGl0ZTtcbiAgICB3aWR0aDogNTAlO1xuICB9XG4gIC5zaWdudXAtYnRue1xuICAgIGNvbG9yOiAkYmxhY2s7XG4gICAgcGFkZGluZzogMC4zNzVyZW0gMC43NXJlbTtcbiAgICBiYWNrZ3JvdW5kOiAkcHJpbWFyeS1jb2xvcjtcbiAgICBib3JkZXI6IDJweCBzb2xpZCAkcHJpbWFyeS1jb2xvcjtcbiAgICBib3JkZXItcmFkaXVzOiAwIDVweCA1cHggMDtcbiAgICBmb250LXdlaWdodDogYm9sZDtcbiAgfVxufSJdfQ== */");

/***/ }),

/***/ "./src/app/shared/molecules/sign-up-molecule/sign-up.component.ts":
/*!************************************************************************!*\
  !*** ./src/app/shared/molecules/sign-up-molecule/sign-up.component.ts ***!
  \************************************************************************/
/*! exports provided: SignUpComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SignUpComponent", function() { return SignUpComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../environments/environment */ "./src/environments/environment.ts");




let SignUpComponent = class SignUpComponent {
    constructor(route) {
        this.route = route;
        this.hostName = _environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].hostName;
    }
    ngOnInit() {
    }
};
SignUpComponent.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"] }
];
SignUpComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-cx-sign-up',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./sign-up.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/molecules/sign-up-molecule/sign-up.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./sign-up.component.scss */ "./src/app/shared/molecules/sign-up-molecule/sign-up.component.scss")).default]
    })
], SignUpComponent);



/***/ }),

/***/ "./src/app/shared/organisms/footer-organism/footer-organism.component.scss":
/*!*********************************************************************************!*\
  !*** ./src/app/shared/organisms/footer-organism/footer-organism.component.scss ***!
  \*********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("/********************Custom Variables **************************/\n.text-white {\n  color: #fff;\n}\n.secondary-bg {\n  background: #000;\n}\n.primary-color {\n  color: #a7f108;\n}\n.no-margin {\n  margin: 0;\n}\n.no-padding {\n  padding: 0;\n}\n.margin-auto {\n  margin: 0 auto;\n}\n.font-bold {\n  font-weight: 800;\n}\n.font-semi-bold {\n  font-weight: 600;\n}\n.mb-30 {\n  margin-bottom: 30px;\n}\n.mb-50 {\n  margin-bottom: 50px;\n}\n.pr-20 {\n  padding-right: 20px;\n}\n.bg-white {\n  background: #fff;\n}\nheader {\n  background: #000000;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi92YXIvd3d3L2h0bWwvanMtc3RvcmVmcm9udC9zcGFydGFjdXNzdG9yZS9zcmMvYXNzZXRzL2Nzcy92YXJpYWJsZS5zY3NzIiwic3JjL2FwcC9zaGFyZWQvb3JnYW5pc21zL2Zvb3Rlci1vcmdhbmlzbS9mb290ZXItb3JnYW5pc20uY29tcG9uZW50LnNjc3MiLCIvdmFyL3d3dy9odG1sL2pzLXN0b3JlZnJvbnQvc3BhcnRhY3Vzc3RvcmUvc3JjL2FwcC9zaGFyZWQvb3JnYW5pc21zL2Zvb3Rlci1vcmdhbmlzbS9mb290ZXItb3JnYW5pc20uY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsZ0VBQUE7QUFNQTtFQUNJLFdBQUE7QUNKSjtBRE1FO0VBQ0UsZ0JBQUE7QUNISjtBREtFO0VBQ0UsY0FBQTtBQ0ZKO0FESUU7RUFDRSxTQUFBO0FDREo7QURHRTtFQUNFLFVBQUE7QUNBSjtBREVFO0VBQ0UsY0FBQTtBQ0NKO0FEQ0U7RUFDRSxnQkFBQTtBQ0VKO0FEQUU7RUFDRSxnQkFBQTtBQ0dKO0FEREU7RUFDRSxtQkFBQTtBQ0lKO0FERkU7RUFDRSxtQkFBQTtBQ0tKO0FESEU7RUFDRSxtQkFBQTtBQ01KO0FESkU7RUFDRSxnQkFBQTtBQ09KO0FDOUNBO0VBQ0ksbUJBQUE7QURpREoiLCJmaWxlIjoic3JjL2FwcC9zaGFyZWQvb3JnYW5pc21zL2Zvb3Rlci1vcmdhbmlzbS9mb290ZXItb3JnYW5pc20uY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIvKioqKioqKioqKioqKioqKioqKipDdXN0b20gVmFyaWFibGVzICoqKioqKioqKioqKioqKioqKioqKioqKioqL1xuJHdoaXRlOiAjZmZmZmZmO1xuJGJsYWNrOiAjMDAwMDAwO1xuJHByaW1hcnktY29sb3I6ICNhN2YxMDg7XG4kYm9yZGVyLWNvbG9yOiAgcmdiYSgyNTUsIDI1NSwgMjU1LCAwLjYpO1xuJGZvcm0tYmc6ICNmN2Y3Zjc7XG4udGV4dC13aGl0ZSB7XG4gICAgY29sb3I6ICNmZmY7XG4gIH1cbiAgLnNlY29uZGFyeS1iZyB7XG4gICAgYmFja2dyb3VuZDogIzAwMDtcbiAgfVxuICAucHJpbWFyeS1jb2xvciB7XG4gICAgY29sb3I6ICNhN2YxMDg7XG4gIH1cbiAgLm5vLW1hcmdpbiB7XG4gICAgbWFyZ2luOiAwO1xuICB9XG4gIC5uby1wYWRkaW5nIHtcbiAgICBwYWRkaW5nOiAwO1xuICB9XG4gIC5tYXJnaW4tYXV0byB7XG4gICAgbWFyZ2luOiAwIGF1dG87XG4gIH1cbiAgLmZvbnQtYm9sZCB7XG4gICAgZm9udC13ZWlnaHQ6IDgwMDtcbiAgfVxuICAuZm9udC1zZW1pLWJvbGQge1xuICAgIGZvbnQtd2VpZ2h0OiA2MDA7XG4gIH1cbiAgLm1iLTMwIHtcbiAgICBtYXJnaW4tYm90dG9tOiAzMHB4O1xuICB9XG4gIC5tYi01MCB7XG4gICAgbWFyZ2luLWJvdHRvbTogNTBweDtcbiAgfVxuICAucHItMjAge1xuICAgIHBhZGRpbmctcmlnaHQ6IDIwcHg7XG4gIH1cbiAgLmJnLXdoaXRlIHtcbiAgICBiYWNrZ3JvdW5kOiAjZmZmO1xuICB9IiwiLyoqKioqKioqKioqKioqKioqKioqQ3VzdG9tIFZhcmlhYmxlcyAqKioqKioqKioqKioqKioqKioqKioqKioqKi9cbi50ZXh0LXdoaXRlIHtcbiAgY29sb3I6ICNmZmY7XG59XG5cbi5zZWNvbmRhcnktYmcge1xuICBiYWNrZ3JvdW5kOiAjMDAwO1xufVxuXG4ucHJpbWFyeS1jb2xvciB7XG4gIGNvbG9yOiAjYTdmMTA4O1xufVxuXG4ubm8tbWFyZ2luIHtcbiAgbWFyZ2luOiAwO1xufVxuXG4ubm8tcGFkZGluZyB7XG4gIHBhZGRpbmc6IDA7XG59XG5cbi5tYXJnaW4tYXV0byB7XG4gIG1hcmdpbjogMCBhdXRvO1xufVxuXG4uZm9udC1ib2xkIHtcbiAgZm9udC13ZWlnaHQ6IDgwMDtcbn1cblxuLmZvbnQtc2VtaS1ib2xkIHtcbiAgZm9udC13ZWlnaHQ6IDYwMDtcbn1cblxuLm1iLTMwIHtcbiAgbWFyZ2luLWJvdHRvbTogMzBweDtcbn1cblxuLm1iLTUwIHtcbiAgbWFyZ2luLWJvdHRvbTogNTBweDtcbn1cblxuLnByLTIwIHtcbiAgcGFkZGluZy1yaWdodDogMjBweDtcbn1cblxuLmJnLXdoaXRlIHtcbiAgYmFja2dyb3VuZDogI2ZmZjtcbn1cblxuaGVhZGVyIHtcbiAgYmFja2dyb3VuZDogIzAwMDAwMDtcbn0iLCJAaW1wb3J0IFwiLi4vLi4vLi4vLi4vYXNzZXRzL2Nzcy9jb25zdGFudHMuc2Nzc1wiO1xuaGVhZGVyIHtcbiAgICBiYWNrZ3JvdW5kOiAkYmxhY2s7XG59Il19 */");

/***/ }),

/***/ "./src/app/shared/organisms/footer-organism/footer-organism.component.ts":
/*!*******************************************************************************!*\
  !*** ./src/app/shared/organisms/footer-organism/footer-organism.component.ts ***!
  \*******************************************************************************/
/*! exports provided: FooterOrganismComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FooterOrganismComponent", function() { return FooterOrganismComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _services_common_utility_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../services/common-utility-service */ "./src/app/services/common-utility-service.ts");
/* harmony import */ var src_environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/environments/environment */ "./src/environments/environment.ts");




let FooterOrganismComponent = class FooterOrganismComponent {
    constructor(utilityService) {
        this.utilityService = utilityService;
        this.footerColumnSlot = [];
        this.copyRightSlot = [];
    }
    ngOnInit() {
        const footerUrl = src_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].footerEndPoint;
        this.utilityService.getRequest(footerUrl, '').subscribe(data => {
            const response = JSON.parse(JSON.stringify(data));
            if (response &&
                response.contentSlots &&
                response.contentSlots.contentSlot) {
                for (const content of response.contentSlots.contentSlot) {
                    if (content.slotId === 'CopyRightsSlot') {
                        this.copyRightSlot = content.components.component[0];
                    }
                }
                this.footerColumnSlot = response.contentSlots.contentSlot.filter(each => each.slotId !== 'CopyRightsSlot');
            }
        });
    }
};
FooterOrganismComponent.ctorParameters = () => [
    { type: _services_common_utility_service__WEBPACK_IMPORTED_MODULE_2__["CommonUtilityService"] }
];
FooterOrganismComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-footer-organism',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./footer-organism.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/organisms/footer-organism/footer-organism.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./footer-organism.component.scss */ "./src/app/shared/organisms/footer-organism/footer-organism.component.scss")).default]
    })
], FooterOrganismComponent);



/***/ }),

/***/ "./src/app/shared/organisms/header-organism/header-organism.component.scss":
/*!*********************************************************************************!*\
  !*** ./src/app/shared/organisms/header-organism/header-organism.component.scss ***!
  \*********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("/********************Custom Variables **************************/\n.text-white {\n  color: #fff;\n}\n.secondary-bg {\n  background: #000;\n}\n.primary-color {\n  color: #a7f108;\n}\n.no-margin {\n  margin: 0;\n}\n.no-padding {\n  padding: 0;\n}\n.margin-auto {\n  margin: 0 auto;\n}\n.font-bold {\n  font-weight: 800;\n}\n.font-semi-bold {\n  font-weight: 600;\n}\n.mb-30 {\n  margin-bottom: 30px;\n}\n.mb-50 {\n  margin-bottom: 50px;\n}\n.pr-20 {\n  padding-right: 20px;\n}\n.bg-white {\n  background: #fff;\n}\nheader {\n  background: #000000;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi92YXIvd3d3L2h0bWwvanMtc3RvcmVmcm9udC9zcGFydGFjdXNzdG9yZS9zcmMvYXNzZXRzL2Nzcy92YXJpYWJsZS5zY3NzIiwic3JjL2FwcC9zaGFyZWQvb3JnYW5pc21zL2hlYWRlci1vcmdhbmlzbS9oZWFkZXItb3JnYW5pc20uY29tcG9uZW50LnNjc3MiLCIvdmFyL3d3dy9odG1sL2pzLXN0b3JlZnJvbnQvc3BhcnRhY3Vzc3RvcmUvc3JjL2FwcC9zaGFyZWQvb3JnYW5pc21zL2hlYWRlci1vcmdhbmlzbS9oZWFkZXItb3JnYW5pc20uY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsZ0VBQUE7QUFNQTtFQUNJLFdBQUE7QUNKSjtBRE1FO0VBQ0UsZ0JBQUE7QUNISjtBREtFO0VBQ0UsY0FBQTtBQ0ZKO0FESUU7RUFDRSxTQUFBO0FDREo7QURHRTtFQUNFLFVBQUE7QUNBSjtBREVFO0VBQ0UsY0FBQTtBQ0NKO0FEQ0U7RUFDRSxnQkFBQTtBQ0VKO0FEQUU7RUFDRSxnQkFBQTtBQ0dKO0FEREU7RUFDRSxtQkFBQTtBQ0lKO0FERkU7RUFDRSxtQkFBQTtBQ0tKO0FESEU7RUFDRSxtQkFBQTtBQ01KO0FESkU7RUFDRSxnQkFBQTtBQ09KO0FDOUNBO0VBQ0ksbUJBQUE7QURpREoiLCJmaWxlIjoic3JjL2FwcC9zaGFyZWQvb3JnYW5pc21zL2hlYWRlci1vcmdhbmlzbS9oZWFkZXItb3JnYW5pc20uY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIvKioqKioqKioqKioqKioqKioqKipDdXN0b20gVmFyaWFibGVzICoqKioqKioqKioqKioqKioqKioqKioqKioqL1xuJHdoaXRlOiAjZmZmZmZmO1xuJGJsYWNrOiAjMDAwMDAwO1xuJHByaW1hcnktY29sb3I6ICNhN2YxMDg7XG4kYm9yZGVyLWNvbG9yOiAgcmdiYSgyNTUsIDI1NSwgMjU1LCAwLjYpO1xuJGZvcm0tYmc6ICNmN2Y3Zjc7XG4udGV4dC13aGl0ZSB7XG4gICAgY29sb3I6ICNmZmY7XG4gIH1cbiAgLnNlY29uZGFyeS1iZyB7XG4gICAgYmFja2dyb3VuZDogIzAwMDtcbiAgfVxuICAucHJpbWFyeS1jb2xvciB7XG4gICAgY29sb3I6ICNhN2YxMDg7XG4gIH1cbiAgLm5vLW1hcmdpbiB7XG4gICAgbWFyZ2luOiAwO1xuICB9XG4gIC5uby1wYWRkaW5nIHtcbiAgICBwYWRkaW5nOiAwO1xuICB9XG4gIC5tYXJnaW4tYXV0byB7XG4gICAgbWFyZ2luOiAwIGF1dG87XG4gIH1cbiAgLmZvbnQtYm9sZCB7XG4gICAgZm9udC13ZWlnaHQ6IDgwMDtcbiAgfVxuICAuZm9udC1zZW1pLWJvbGQge1xuICAgIGZvbnQtd2VpZ2h0OiA2MDA7XG4gIH1cbiAgLm1iLTMwIHtcbiAgICBtYXJnaW4tYm90dG9tOiAzMHB4O1xuICB9XG4gIC5tYi01MCB7XG4gICAgbWFyZ2luLWJvdHRvbTogNTBweDtcbiAgfVxuICAucHItMjAge1xuICAgIHBhZGRpbmctcmlnaHQ6IDIwcHg7XG4gIH1cbiAgLmJnLXdoaXRlIHtcbiAgICBiYWNrZ3JvdW5kOiAjZmZmO1xuICB9IiwiLyoqKioqKioqKioqKioqKioqKioqQ3VzdG9tIFZhcmlhYmxlcyAqKioqKioqKioqKioqKioqKioqKioqKioqKi9cbi50ZXh0LXdoaXRlIHtcbiAgY29sb3I6ICNmZmY7XG59XG5cbi5zZWNvbmRhcnktYmcge1xuICBiYWNrZ3JvdW5kOiAjMDAwO1xufVxuXG4ucHJpbWFyeS1jb2xvciB7XG4gIGNvbG9yOiAjYTdmMTA4O1xufVxuXG4ubm8tbWFyZ2luIHtcbiAgbWFyZ2luOiAwO1xufVxuXG4ubm8tcGFkZGluZyB7XG4gIHBhZGRpbmc6IDA7XG59XG5cbi5tYXJnaW4tYXV0byB7XG4gIG1hcmdpbjogMCBhdXRvO1xufVxuXG4uZm9udC1ib2xkIHtcbiAgZm9udC13ZWlnaHQ6IDgwMDtcbn1cblxuLmZvbnQtc2VtaS1ib2xkIHtcbiAgZm9udC13ZWlnaHQ6IDYwMDtcbn1cblxuLm1iLTMwIHtcbiAgbWFyZ2luLWJvdHRvbTogMzBweDtcbn1cblxuLm1iLTUwIHtcbiAgbWFyZ2luLWJvdHRvbTogNTBweDtcbn1cblxuLnByLTIwIHtcbiAgcGFkZGluZy1yaWdodDogMjBweDtcbn1cblxuLmJnLXdoaXRlIHtcbiAgYmFja2dyb3VuZDogI2ZmZjtcbn1cblxuaGVhZGVyIHtcbiAgYmFja2dyb3VuZDogIzAwMDAwMDtcbn0iLCJAaW1wb3J0IFwiLi4vLi4vLi4vLi4vYXNzZXRzL2Nzcy9jb25zdGFudHMuc2Nzc1wiO1xuaGVhZGVyIHtcbiAgICBiYWNrZ3JvdW5kOiAkYmxhY2s7XG59Il19 */");

/***/ }),

/***/ "./src/app/shared/organisms/header-organism/header-organism.component.ts":
/*!*******************************************************************************!*\
  !*** ./src/app/shared/organisms/header-organism/header-organism.component.ts ***!
  \*******************************************************************************/
/*! exports provided: HeaderOrganismComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HeaderOrganismComponent", function() { return HeaderOrganismComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _services_common_utility_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../services/common-utility-service */ "./src/app/services/common-utility-service.ts");
/* harmony import */ var src_environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/environments/environment */ "./src/environments/environment.ts");




let HeaderOrganismComponent = class HeaderOrganismComponent {
    constructor(utilityService) {
        this.utilityService = utilityService;
    }
    ngOnInit() {
        const headerUrl = src_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].headerEndpoint;
        this.utilityService.getRequest(headerUrl, '').subscribe(data => {
            const response = JSON.parse(JSON.stringify(data));
            if (response &&
                response.contentSlots &&
                response.contentSlots.contentSlot) {
                for (const content of response.contentSlots.contentSlot) {
                    if (content.slotId === 'HeaderInfoSlot') {
                        this.headerLinks = content.components.component;
                    }
                    else if (content.slotId === 'SiteLogoSlot') {
                        this.logoUrl = content.components.component[0].media.url;
                    }
                    else if (content.slotId === 'NavigationBarSlot') {
                        this.headerNavLinks = content.components.component;
                    }
                }
            }
        });
    }
};
HeaderOrganismComponent.ctorParameters = () => [
    { type: _services_common_utility_service__WEBPACK_IMPORTED_MODULE_2__["CommonUtilityService"] }
];
HeaderOrganismComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-header-organism',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./header-organism.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/organisms/header-organism/header-organism.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./header-organism.component.scss */ "./src/app/shared/organisms/header-organism/header-organism.component.scss")).default]
    })
], HeaderOrganismComponent);



/***/ }),

/***/ "./src/app/shared/organisms/home-organism/home-organism.component.scss":
/*!*****************************************************************************!*\
  !*** ./src/app/shared/organisms/home-organism/home-organism.component.scss ***!
  \*****************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3NoYXJlZC9vcmdhbmlzbXMvaG9tZS1vcmdhbmlzbS9ob21lLW9yZ2FuaXNtLmNvbXBvbmVudC5zY3NzIn0= */");

/***/ }),

/***/ "./src/app/shared/organisms/home-organism/home-organism.component.ts":
/*!***************************************************************************!*\
  !*** ./src/app/shared/organisms/home-organism/home-organism.component.ts ***!
  \***************************************************************************/
/*! exports provided: HomeOrganismComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeOrganismComponent", function() { return HomeOrganismComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");



let HomeOrganismComponent = class HomeOrganismComponent {
    constructor(route) {
        this.route = route;
    }
    ngOnInit() {
        this.route.data.subscribe((data) => {
            const response = data.homeData;
            if (response &&
                response.contentSlots &&
                response.contentSlots.contentSlot) {
                for (const content of response.contentSlots.contentSlot) {
                    if (content.slotId === 'HomePageCarouselSlot') {
                        this.homeContent = content.components.component;
                    }
                }
            }
        });
    }
};
HomeOrganismComponent.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"] }
];
HomeOrganismComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-home-organism',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./home-organism.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/organisms/home-organism/home-organism.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./home-organism.component.scss */ "./src/app/shared/organisms/home-organism/home-organism.component.scss")).default]
    })
], HomeOrganismComponent);



/***/ }),

/***/ "./src/app/shared/organisms/login-organism/login-organism.component.scss":
/*!*******************************************************************************!*\
  !*** ./src/app/shared/organisms/login-organism/login-organism.component.scss ***!
  \*******************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3NoYXJlZC9vcmdhbmlzbXMvbG9naW4tb3JnYW5pc20vbG9naW4tb3JnYW5pc20uY29tcG9uZW50LnNjc3MifQ== */");

/***/ }),

/***/ "./src/app/shared/organisms/login-organism/login-organism.component.ts":
/*!*****************************************************************************!*\
  !*** ./src/app/shared/organisms/login-organism/login-organism.component.ts ***!
  \*****************************************************************************/
/*! exports provided: LoginOrganismComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginOrganismComponent", function() { return LoginOrganismComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let LoginOrganismComponent = class LoginOrganismComponent {
    constructor() { }
    ngOnInit() {
    }
};
LoginOrganismComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-login-organism',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./login-organism.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/organisms/login-organism/login-organism.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./login-organism.component.scss */ "./src/app/shared/organisms/login-organism/login-organism.component.scss")).default]
    })
], LoginOrganismComponent);



/***/ }),

/***/ "./src/app/shared/organisms/not-found-organism/not-found-organism.component.scss":
/*!***************************************************************************************!*\
  !*** ./src/app/shared/organisms/not-found-organism/not-found-organism.component.scss ***!
  \***************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3NoYXJlZC9vcmdhbmlzbXMvbm90LWZvdW5kLW9yZ2FuaXNtL25vdC1mb3VuZC1vcmdhbmlzbS5jb21wb25lbnQuc2NzcyJ9 */");

/***/ }),

/***/ "./src/app/shared/organisms/not-found-organism/not-found-organism.component.ts":
/*!*************************************************************************************!*\
  !*** ./src/app/shared/organisms/not-found-organism/not-found-organism.component.ts ***!
  \*************************************************************************************/
/*! exports provided: NotFoundOrganismComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NotFoundOrganismComponent", function() { return NotFoundOrganismComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let NotFoundOrganismComponent = class NotFoundOrganismComponent {
    constructor() { }
    ngOnInit() {
    }
};
NotFoundOrganismComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-not-found-organism',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./not-found-organism.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/organisms/not-found-organism/not-found-organism.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./not-found-organism.component.scss */ "./src/app/shared/organisms/not-found-organism/not-found-organism.component.scss")).default]
    })
], NotFoundOrganismComponent);



/***/ }),

/***/ "./src/app/shared/organisms/organisms.module.ts":
/*!******************************************************!*\
  !*** ./src/app/shared/organisms/organisms.module.ts ***!
  \******************************************************/
/*! exports provided: OrganismsModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OrganismsModule", function() { return OrganismsModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _services_services_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/services.module */ "./src/app/services/services.module.ts");
/* harmony import */ var _molecules_molecules_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../molecules/molecules.module */ "./src/app/shared/molecules/molecules.module.ts");
/* harmony import */ var _directives_directives_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../directives/directives.module */ "./src/app/shared/directives/directives.module.ts");
/* harmony import */ var _pipes_pipes_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../pipes/pipes.module */ "./src/app/shared/pipes/pipes.module.ts");
/* harmony import */ var _header_organism_header_organism_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./header-organism/header-organism.component */ "./src/app/shared/organisms/header-organism/header-organism.component.ts");
/* harmony import */ var _footer_organism_footer_organism_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./footer-organism/footer-organism.component */ "./src/app/shared/organisms/footer-organism/footer-organism.component.ts");
/* harmony import */ var _home_organism_home_organism_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./home-organism/home-organism.component */ "./src/app/shared/organisms/home-organism/home-organism.component.ts");
/* harmony import */ var _not_found_organism_not_found_organism_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./not-found-organism/not-found-organism.component */ "./src/app/shared/organisms/not-found-organism/not-found-organism.component.ts");
/* harmony import */ var _login_organism_login_organism_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./login-organism/login-organism.component */ "./src/app/shared/organisms/login-organism/login-organism.component.ts");











let OrganismsModule = class OrganismsModule {
};
OrganismsModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _services_services_module__WEBPACK_IMPORTED_MODULE_2__["ServicesModule"],
            _molecules_molecules_module__WEBPACK_IMPORTED_MODULE_3__["MoleculesModule"],
            _directives_directives_module__WEBPACK_IMPORTED_MODULE_4__["DirectivesModule"],
            _pipes_pipes_module__WEBPACK_IMPORTED_MODULE_5__["PipesModule"]
        ],
        exports: [
            _header_organism_header_organism_component__WEBPACK_IMPORTED_MODULE_6__["HeaderOrganismComponent"],
            _footer_organism_footer_organism_component__WEBPACK_IMPORTED_MODULE_7__["FooterOrganismComponent"],
            _home_organism_home_organism_component__WEBPACK_IMPORTED_MODULE_8__["HomeOrganismComponent"],
            _login_organism_login_organism_component__WEBPACK_IMPORTED_MODULE_10__["LoginOrganismComponent"],
            _not_found_organism_not_found_organism_component__WEBPACK_IMPORTED_MODULE_9__["NotFoundOrganismComponent"]
        ],
        declarations: [
            _header_organism_header_organism_component__WEBPACK_IMPORTED_MODULE_6__["HeaderOrganismComponent"],
            _footer_organism_footer_organism_component__WEBPACK_IMPORTED_MODULE_7__["FooterOrganismComponent"],
            _home_organism_home_organism_component__WEBPACK_IMPORTED_MODULE_8__["HomeOrganismComponent"],
            _not_found_organism_not_found_organism_component__WEBPACK_IMPORTED_MODULE_9__["NotFoundOrganismComponent"],
            _login_organism_login_organism_component__WEBPACK_IMPORTED_MODULE_10__["LoginOrganismComponent"]
        ],
        providers: [],
    })
], OrganismsModule);



/***/ }),

/***/ "./src/app/shared/pipes/pipes.module.ts":
/*!**********************************************!*\
  !*** ./src/app/shared/pipes/pipes.module.ts ***!
  \**********************************************/
/*! exports provided: PipesModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PipesModule", function() { return PipesModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");



let PipesModule = class PipesModule {
};
PipesModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"]
        ],
        exports: [],
        declarations: [],
        providers: [],
    })
], PipesModule);



/***/ }),

/***/ "./src/app/shared/shared.module.ts":
/*!*****************************************!*\
  !*** ./src/app/shared/shared.module.ts ***!
  \*****************************************/
/*! exports provided: SharedModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SharedModule", function() { return SharedModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _directives_directives_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./directives/directives.module */ "./src/app/shared/directives/directives.module.ts");
/* harmony import */ var _molecules_molecules_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./molecules/molecules.module */ "./src/app/shared/molecules/molecules.module.ts");
/* harmony import */ var _organisms_organisms_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./organisms/organisms.module */ "./src/app/shared/organisms/organisms.module.ts");
/* harmony import */ var _pipes_pipes_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./pipes/pipes.module */ "./src/app/shared/pipes/pipes.module.ts");







let SharedModule = class SharedModule {
};
SharedModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"]
        ],
        exports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _molecules_molecules_module__WEBPACK_IMPORTED_MODULE_4__["MoleculesModule"],
            _organisms_organisms_module__WEBPACK_IMPORTED_MODULE_5__["OrganismsModule"],
            _pipes_pipes_module__WEBPACK_IMPORTED_MODULE_6__["PipesModule"],
            _directives_directives_module__WEBPACK_IMPORTED_MODULE_3__["DirectivesModule"]
        ],
        declarations: []
    })
], SharedModule);



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

const environment = {
    production: false,
    hostName: 'https://api.c050ygx6-obeikanin2-d1-public.model-t.cc.commerce.ondemand.com/',
    headerEndpoint: 'osanedcommercewebservices/v2/osaned/cms/pages?pageType=ContentPage&pageLabelOrId=headerpage',
    logoUrl: 'medias/site-logo.png?context=bWFzdGVyfGltYWdlc3wxMTEyOXxpbWFnZS9wbmd8aDUyL2gxNi84Nzk2NzE3MjUyNjM4L3NpdGUtbG9nby5wbmd8MTJiMzZkYjBlZDNkMTVjMWMwOGMyMTU0NTlkZGI3NjE3NGU3MGFkZmVhNjg2M2VkY2E2ZGRkODA5MDZmNjMyOQ',
    homePageEndPoint: '/osanedcommercewebservices/v2/osaned/cms/pages?pageType=ContentPage&pageLabelOrId=homepage',
    oAuthAPI: 'authorizationserver/oauth/token',
    registerUserAPI: 'osanedcommercewebservices/v2/osaned/users',
    footerEndPoint: 'osanedcommercewebservices/v2/osaned/cms/pages?pageType=ContentPage&pageLabelOrId=footerPage',
    tokenEndpoint: 'authorizationserver/oauth/token',
    loginEndpoint: 'osanedcommercewebservices/v2/osaned/users/'
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm2015/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");





if (_environments_environment__WEBPACK_IMPORTED_MODULE_4__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["enableProdMode"])();
}
document.addEventListener('DOMContentLoaded', () => {
    Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_2__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_3__["AppModule"])
        .catch(err => console.error(err));
});


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /var/www/html/js-storefront/spartacusstore/src/main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main-es2015.js.map