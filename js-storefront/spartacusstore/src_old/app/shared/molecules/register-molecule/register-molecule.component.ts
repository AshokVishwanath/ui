import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { RegisterUserService } from '../../../services/register-user.service';
import { CommonUtilityService } from '../../../services/common-utility-service';
import {MatSnackBar} from '@angular/material/snack-bar';
import { Router } from '@angular/router';
@Component({
  selector: 'app-register-molecule',
  templateUrl: './register-molecule.component.html',
  styleUrls: ['./register-molecule.component.scss']
})
export class RegisterMoleculeComponent implements OnInit {
  registerForm: FormGroup;
  validationMessages = {
    name: [{ type: 'required', message: 'Full name is required' }],
    email: [
      { type: 'required', message: 'Email is required' },
      { type: 'email', message: 'Enter a valid email' }
    ],
    mobile: [
      { type: 'required', message: 'Mobile number is required' },
    //  { type: 'minlength', message: 'Minimum 10 digits' },
      { type: 'pattern', message: 'Please enter a valid mobile number' }
    ],
    company: [{ type: 'required', message: 'Company name is required' }],
    size: [{ type: 'required', message: 'Please select any option' }],
    commercial: [{ type: 'required', message: 'Commercial registration is required' }],
  };
  errMsg = 'Sorry ! Please try again later';
  succMsg = 'User Registerd successfully !';
  constructor(private fb: FormBuilder, private regUserService: RegisterUserService, private router: Router,
              private matSnack: MatSnackBar, private utilService: CommonUtilityService) { }

  ngOnInit() {
    const MOBILE_PATTERN = '^((\\+91-?)|0)?[0-9]{10}$';
    this.registerForm = this.fb.group({
      name: [null, Validators.required],
      email: [null, Validators.compose([
        Validators.required,
        Validators.email
      ])],
      mobile: [null, Validators.compose([
        Validators.required,
        Validators.pattern(MOBILE_PATTERN)
      ])],
      company: [null, Validators.required],
      size: [null, Validators.required],
      commercial: [null, Validators.required]
    });
  }
  registerUser(user) {
    const {name, email, mobile, company, size, commercial} =  user;
    if (name && email && mobile && company && size && commercial) {
      this.utilService.getAuth().subscribe((res: any) => {
        if (res && res.access_token) {
          this.regUserService.registerUserDetails(res.access_token, user).subscribe((result: any) => {
            console.log('User registered successfully!', result);
            this.matSnack.open(this.succMsg, 'close', {
              duration: 5000,
            });
            this.router.navigate(['/']);
          }, (err) => {
            this.matSnack.open(this.errMsg, 'close', {
              duration: 5000,
            });
          });
        }
      }, (err) => {
        console.log('O Auth API failed', err);
        this.matSnack.open(this.errMsg, 'close', {
          duration: 5000,
        });
      });
    }

  }
}
