import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { HomePageService } from '../../../services/home-page.service';

@Component({
  selector: 'app-contact-us-molecule',
  templateUrl: './contact-us-molecule.component.html',
  styleUrls: ['./contact-us-molecule.component.scss']
})
export class ContactUsMoleculeComponent implements OnInit {
  userDetailsForm: FormGroup;
  validationMessages = {
    name: [{ type: 'required', message: 'Full name is required' }],
    email: [
      { type: 'required', message: 'Email is required' },
      { type: 'email', message: 'Enter a valid email' }
    ],
    message: [{ type: 'required', message: 'Please enter some message' }],
  };

  constructor(private fb: FormBuilder, private homePageService: HomePageService) { }

  ngOnInit() {
    // user details form validations
    this.userDetailsForm = this.fb.group({
      name: [null, Validators.required],
      email: [null, Validators.compose([
        Validators.required,
        Validators.email
      ])],
      message: [null, Validators.required]
    });
  }
  onSubmitUserDetails(userMsg) {
    console.log('userValue', userMsg);
    if (userMsg) {
      this.homePageService.postUserMessage(userMsg).subscribe(() => { });
    }
  }

}
