import { Component, OnInit } from '@angular/core';
import { CommonUtilityService } from '../../../services/common-utility-service';
import { environment } from 'src/environments/environment';
@Component({
  selector: 'app-header-organism',
  templateUrl: './header-organism.component.html',
  styleUrls: ['./header-organism.component.scss'],
})
export class HeaderOrganismComponent implements OnInit {
  headerLinks: string[];
  headerNavLinks: string[];
  logoUrl: string;

  constructor(private utilityService: CommonUtilityService) {}

  ngOnInit() {
    const headerUrl = environment.headerEndpoint;
    this.utilityService.getRequest(headerUrl, '').subscribe(data => {
      const response = JSON.parse(JSON.stringify(data));
      if (
        response &&
        response.contentSlots &&
        response.contentSlots.contentSlot
      ) {
        for (const content of response.contentSlots.contentSlot) {
          if (content.slotId === 'HeaderInfoSlot') {
            this.headerLinks = content.components.component;
          } else if (content.slotId === 'SiteLogoSlot') {
            this.logoUrl = content.components.component[0].media.url;
          } else if (content.slotId === 'NavigationBarSlot') {
            this.headerNavLinks = content.components.component;
          }
        }
      }
    });
  }
}
