import { NgModule } from '@angular/core';
import {MatFormFieldModule, MatIconModule, MatDividerModule,
        MatButtonModule, MatInputModule, MatRippleModule,
        MatCardModule, MatSelectModule,   MatDatepickerModule,
        MatNativeDateModule, MatCheckboxModule, MatSnackBarModule} from '@angular/material';
const modules = [
    MatButtonModule,
    MatInputModule,
    MatRippleModule,
    MatIconModule,
    MatDividerModule,
    MatCardModule,
    MatSelectModule,
    MatFormFieldModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatCheckboxModule,
    MatSnackBarModule
];

@NgModule({
imports: [...modules],
exports: [...modules]
})
export class MaterialModule {}

