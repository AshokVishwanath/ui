import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ServicesModule } from '../../services/services.module';
import { DirectivesModule } from '../directives/directives.module';
import { PipesModule } from '../pipes/pipes.module';
import { HeaderLinksMoleculeComponent } from './header-links-molecule/header-links-molecule.component';
import { HeaderNavMoleculeComponent } from './header-nav-molecule/header-nav-molecule.component';
import { CarousalMoleculeComponent } from './carousal-molecule/carousal-molecule.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

@NgModule({
    imports: [
        ServicesModule,
        DirectivesModule,
        PipesModule,
        NgbModule,
        CommonModule,
        FormsModule

    ],
    exports: [
        HeaderLinksMoleculeComponent,
        HeaderNavMoleculeComponent,
        CarousalMoleculeComponent
    ],
    declarations: [
        HeaderLinksMoleculeComponent,
        HeaderNavMoleculeComponent,
        CarousalMoleculeComponent
    ],
    providers: [],
})
export class MoleculesModule { }
